package com.sunxin.demo.controller;

import org.apache.commons.io.FileUtils;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sunxin02
 */
@RestController
@RequestMapping("/api")
public class PingController {

    @RequestMapping("/ping")
    public String ping() throws Exception {
        return "pong";
    }

    public static void main(String[] args) throws Exception {

        RestTemplate restTemplate = new RestTemplate();
        List<Map<String, Object>> request = new ArrayList<>();
        List<String> sessionIds = FileUtils.readLines(new File("C:\\Users\\sunxin02\\Desktop\\session_10.txt"));
        for (String sessionId : sessionIds){
            System.out.println(sessionId);
            HashMap<String,Object> objectObjectHashMap = new HashMap<>();
            objectObjectHashMap.put("sessionId",sessionId.trim());
            objectObjectHashMap.put("isIdempotent",true);
            request.add(objectObjectHashMap);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Token","AA1CB3D9A117855F5E4B99DE5EE3E322");
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> entity = new HttpEntity<>(request, headers);
        ResponseEntity<String> exchange = restTemplate.exchange("http://antifraud-risk-admin.rrdbg.com/api/block/engine/event/gatewayRetry", HttpMethod.POST, entity, String.class);
        String body = exchange.getBody();
        System.out.println(body);
    }
}
