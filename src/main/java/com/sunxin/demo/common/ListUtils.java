package com.sunxin.demo.common;

/**
 * @author sunxin02
 */
public class ListUtils {

    public static ObjectNode generateList(){
        ObjectNode head = new ObjectNode(0);
        ObjectNode cur = new ObjectNode(1);
        head.next = cur;
        for (int i = 2; i < 10; i ++){
            cur.next = new ObjectNode(i);
            cur = cur.next;
        }
        return head;
    }

    public static void printList(Node objectNode){
        if (objectNode == null){
            System.err.println("node is null");
            return;
        }
        System.out.print("[");
        while (objectNode != null){
            System.out.print(objectNode.value);
            if (objectNode.next != null){
                System.out.print(",");
            }
            objectNode = objectNode.next;
        }
        System.out.println("]");
    }

    public static void printList(ObjectNode objectNode){
        if (objectNode == null){
            System.err.println("node is null");
            return;
        }
        System.out.print("[");
        while (objectNode != null){
            System.out.print(objectNode.value);
            if (objectNode.next != null){
                System.out.print(",");
            }
            objectNode = objectNode.next;
        }
        System.out.println("]");
    }

    public static void printList(DoubleNode node){
        if (node == null){
            System.err.println("node is null");
            return;
        }
        System.out.print("[");
        while (node != null){
            System.out.print(node.value);
            if (node.next != null){
                System.out.print(",");
            }
            node = node.next;
        }
        System.out.println("]");
    }

    public static void main(String[] args) {
        ObjectNode objectNode = generateList();
        printList(objectNode);
    }
}
