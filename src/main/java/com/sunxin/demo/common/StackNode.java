package com.sunxin.demo.common;

/**
 * @author q
 */
public class StackNode {

    public int value;
    public StackNode pre;
    public StackNode next;
    public StackNode last;


    public StackNode(int t) {
        this.value = t;
    }

}
