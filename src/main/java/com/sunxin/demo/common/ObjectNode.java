package com.sunxin.demo.common;

/**
 * @author sunxin02
 */
public class ObjectNode<T> {
    public T value;
    public ObjectNode<T> pre;
    public ObjectNode<T> next;

    public ObjectNode(T t) {
        this.value = t;
    }

}
