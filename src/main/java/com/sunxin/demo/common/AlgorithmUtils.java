package com.sunxin.demo.common;

import java.util.Random;

/**
 * @author sunxin02
 */
public class AlgorithmUtils {

    private static final Random random = new Random();


    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static TreeNode<Integer> generateBinaryTree(){
        TreeNode<Integer> root = new TreeNode<>(random.nextInt(100));
        createLeft(root,0);
        createRight(root,0);
        return root;
    }

    private static void createLeft(TreeNode<Integer> treeNode,Integer i){
        if (i > 5){
            return;
        }
        i++;
        treeNode.left = new TreeNode<>(random.nextInt(100));
        treeNode.right = new TreeNode<>(random.nextInt(100));
        createLeft(treeNode.left,i);
        createRight(treeNode.right,i);
    }

    private static void createRight(TreeNode<Integer> treeNode,Integer i){
        if (i > 5){
            return;
        }
        i++;
        treeNode.left = new TreeNode<>(random.nextInt(100));
        treeNode.right = new TreeNode<>(random.nextInt(100));
        createLeft(treeNode.left,i);
        createRight(treeNode.right,i);
    }

}
