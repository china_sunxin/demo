package com.sunxin.demo.common;

/**
 * @author sunxin02
 */
public class RandomNode {

    int val;
    ObjectNode next;
    ObjectNode random;

    public RandomNode(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
