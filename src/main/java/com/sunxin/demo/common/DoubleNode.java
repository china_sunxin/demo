package com.sunxin.demo.common;

/**
 * @author sunxin
 */
public class DoubleNode {

    public int value;
    public DoubleNode pre;
    public DoubleNode next;

    public DoubleNode(int t) {
        this.value = t;
    }

}
