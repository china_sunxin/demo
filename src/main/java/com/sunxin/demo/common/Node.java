package com.sunxin.demo.common;

/**
 * @author sunxin
 */
public class Node {

    public int value;
    public Node pre;
    public Node next;

    public Node(int t) {
        this.value = t;
    }

}
