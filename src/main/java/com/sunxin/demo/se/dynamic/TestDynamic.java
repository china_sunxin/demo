package com.sunxin.demo.se.dynamic;

import org.hibernate.validator.internal.util.privilegedactions.NewProxyInstance;

import java.lang.reflect.Proxy;

/**
 * @author q
 */
public class TestDynamic {

    public static void main(String[] args) {

        UserInterface user = new UserImpl();

        UserHandler userHandler = new UserHandler(user);

//        UserInterface o = (UserInterface)Proxy.newProxyInstance(user.getClass().getClassLoader(), new Class[]{UserInterface.class}, userHandler);
        UserInterface o = (UserInterface)Proxy.newProxyInstance(user.getClass().getClassLoader(), user.getClass().getInterfaces(), userHandler);
//        System.out.println(o);
        o.sayHello();
    }


}
