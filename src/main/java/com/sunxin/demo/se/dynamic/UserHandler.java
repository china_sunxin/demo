package com.sunxin.demo.se.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author q
 */
public class UserHandler implements InvocationHandler {
    private Object object;

    public UserHandler(Object object) {
        this.object = object;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("proxy before");
        method.invoke(object,args);
        System.out.println("proxy after");

        return null;
    }
}
