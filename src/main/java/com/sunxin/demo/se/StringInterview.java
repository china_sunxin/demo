package com.sunxin.demo.se;

/**
 * @author q
 */
public class StringInterview {

    public static void main(String[] args) {
//        String a = "a";
        final String a = "a";
        String b = "b";
        String d = "a";
        String c = a + b;

        System.out.println(a + b == "ab");
        System.out.println(a + b == c);
        System.out.println(a == d);
    }

}
