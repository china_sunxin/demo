package com.sunxin.demo.se.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class TestReflect {

    public static void main(String[] args) throws Exception {

        Class<?> clazz = Class.forName("com.sunxin.demo.se.reflect.User");
        Object o = clazz.newInstance();

        Field name = clazz.getDeclaredField("name");
        name.set(o,"zhangsan");

        Method[] methods = clazz.getMethods();
        for (Method method:methods) {
            if (method.getName().equals("eat")) {
                method.invoke(o, "shjuiguo");
            }
        }

    }

}
