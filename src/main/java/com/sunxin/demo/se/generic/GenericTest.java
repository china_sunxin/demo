package com.sunxin.demo.se.generic;

public class GenericTest {

    public static void main(String[] args) {
        Plate<? super Fruit> p=new Plate<Fruit>(new Fruit());

//存入元素正常
//        p.set(new Fruit());
//        p.set(new Apple());
//        p.set(new Food());

//读取出来的东西只能存放在Object类里。
//        Apple newFruit3=p.get();
//        Fruit newFruit1=p.get();
//        Food newFruit2=p.get();
    }
}
class Plate<T>{
    private T item;
    public Plate(T t){item=t;}
    public void set(T t){item=t;}
    public T get(){return item;}
}
class Food{}

//Lev 2
class Fruit extends Food{}
class Meat extends Food{}

//Lev 3
class Apple extends Fruit{}
class Banana extends Fruit{}
class Pork extends Meat{}
class Beef extends Meat{}

//Lev 4
class RedApple extends Apple{}
class GreenApple extends Apple{}
