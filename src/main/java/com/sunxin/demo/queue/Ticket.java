package com.sunxin.demo.queue;

import lombok.Data;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

@Data
public class Ticket implements Delayed {

    private String name;

    private long time;

    public Ticket() {

    }

    public Ticket(String name,long time) {
        this.name = name;
        this.time = System.currentTimeMillis() + time;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return time - System.currentTimeMillis();
    }

    @Override
    public int compareTo(Delayed o) {
        long diff = this.time - ((Ticket) o).time;
        if(diff <= 0){
            return -1;
        }else {
            return 1;
        }
    }
}
