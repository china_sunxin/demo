package com.sunxin.demo.queue;

import java.util.concurrent.DelayQueue;

public class DelayQueueTest {
    public static void main(String[] args) throws Exception {
        Ticket ticket2 = new Ticket("2",2000);
        Ticket ticket1 = new Ticket("1",5000);
        Ticket ticket3 = new Ticket("3",3000);

        DelayQueue<Ticket> tickets = new DelayQueue<>();
        tickets.put(ticket1);
        tickets.put(ticket2);
        tickets.put(ticket3);

    }
}
