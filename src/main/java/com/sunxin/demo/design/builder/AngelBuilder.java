package com.sunxin.demo.design.builder;

/**
 * @author sunxin02
 */
public class AngelBuilder extends ActorBuilder{
    @Override
    public void buildType() {
        actor.setType("angle type");
    }

    @Override
    public void buildSex() {
        actor.setSex("angle sex");
    }
}
