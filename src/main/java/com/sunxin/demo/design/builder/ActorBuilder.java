package com.sunxin.demo.design.builder;

/**
 * @author sunxin02
 */
public abstract class ActorBuilder {
    Actor actor;

    public abstract void buildType();
    public abstract void buildSex();

    public Actor constract(){
        actor = new Actor();
        this.buildSex();
        this.buildType();
        return actor;
    }
}
