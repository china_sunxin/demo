package com.sunxin.demo.design.builder;

import lombok.Data;

/**
 * @author sunxin02
 */
@Data
public class Actor {

    private String type;
    private String sex;
    private String face;
    private String costume;
    private String hairstyle;

}
