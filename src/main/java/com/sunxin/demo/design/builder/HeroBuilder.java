package com.sunxin.demo.design.builder;

/**
 * @author sunxin02
 */
public class HeroBuilder extends ActorBuilder{

    @Override
    public void buildType() {
        actor.setType("hero");
    }

    @Override
    public void buildSex() {
        actor.setSex("nan");
    }
}
