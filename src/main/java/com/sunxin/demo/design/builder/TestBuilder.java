package com.sunxin.demo.design.builder;

/**
 * @author sunxin02
 */
public class TestBuilder {

    public static void main(String[] args) {
        HeroBuilder heroBuilder = new HeroBuilder();
        Actor actor1 = heroBuilder.constract();

        AngelBuilder angelBuilder = new AngelBuilder();
        Actor actor2 = angelBuilder.constract();

        System.out.println(actor1);
        System.out.println(actor2);
        System.out.println(actor1);
    }
}
