package com.sunxin.demo.design.observer;

/**
 * @author sunxin
 * @date 2021/2/14 11:33
 **/
public class ObserverExe {
    public static void main(String[] args) {

        Subject subject = new Subject();
        WorkObserver workObserver = new WorkObserver();
        subject.addObserver(workObserver);
        subject.happen(EventEnum.work);

    }
}
