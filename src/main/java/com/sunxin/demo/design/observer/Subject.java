package com.sunxin.demo.design.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sunxin
 * @date 2021/2/14 11:22
 **/
public class Subject {
    private List<Observer> observers = new ArrayList<>();

    public void addObserver(Observer observer){
        observers.add(observer);
    }

    public void deleteObserver(Observer observer){
        observers.remove(observer);
    }

    public void happen(EventEnum event){
        for (Observer observer : observers){
            observer.observe(event);
        }
    }
}
