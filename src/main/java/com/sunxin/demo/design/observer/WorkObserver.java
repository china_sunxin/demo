package com.sunxin.demo.design.observer;

/**
 * @author sunxin
 * @date 2021/2/14 11:32
 **/
public class WorkObserver extends Observer{

    @Override
    public void observe(EventEnum event) {
        if (event == EventEnum.work){
            System.out.println("work");;
        }
    }
}
