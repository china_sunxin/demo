package com.sunxin.demo.design.observer;

/**
 * @author sunxin
 * @date 2021/2/14 11:28
 **/
public enum EventEnum {
    /**
     * 工作
     */
    work,
    rest,
    swim
}
