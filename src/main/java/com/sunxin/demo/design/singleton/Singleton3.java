package com.sunxin.demo.design.singleton;

/**
 * @author sunxin
 */
public class Singleton3 {

    private Singleton3(){

    }

    /**
     * 一个类在加载的时候，它的静态内部类是不会被加载的，只有调用getInstance的时候才会被加载
     * 解决了饿汉式单里的缺点。
     */
    private static class SingletonHolder{
        private final static Singleton3 instance = new Singleton3();
    }

    public Singleton3 getInstance(){
        return SingletonHolder.instance;
    }


}
