package com.sunxin.demo.design.singleton;

/**
 * @author sunxin
 * 枚举单例，枚举类是没有构造方法的，不能通过反射来创建，是最严谨的方法。
 */

public enum Singleton4 {
    INSTANCE;
}
