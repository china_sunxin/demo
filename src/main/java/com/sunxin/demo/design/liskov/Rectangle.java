package com.sunxin.demo.design.liskov;

public class Rectangle {

    protected double width;
    protected double height;

    public double area(){
        return this.width*this.height;
    }
}
