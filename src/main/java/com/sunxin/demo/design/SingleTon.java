package com.sunxin.demo.design;


/**
 * @author sunxin
 * DCL double check lock
 */
public class SingleTon {


    /**
     * 对象初始化的过程可以分为三步：
     * 1、在堆上new一个对象
     * 2、初始化init对象
     * 3、将栈上变量与对象建立关联
     *
     * 其中2与3是可以发生重排序的，重排序后，栈上的变量instance指向了一个半初始化的对象，该对象是不为null的
     *
     * 这种情况会导致多个线程拿到的对象不是同一个值，因此在写单例的时候要将对象用bolatile修饰。
     * eg:线程A第一次进来的时候刚好发生了重排序，instance 为一个半初始化的对象，此时线程B进来发现instance != null，于是直接return拿去使用了；
     * 然后线程A将对象初始化并返回。虽然两者使用的时同一个对象引用，但是两者内部的变量值并不一致。
     */
    private volatile SingleTon instance;


    private SingleTon(){

    }

    public SingleTon getInstance(){
        if (instance == null) {
            synchronized (this) {
                if(instance == null){
                    instance = new SingleTon();
                }
            }
        }
        return instance;
    }


}
