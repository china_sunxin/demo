package com.sunxin.demo.design.interceptor;

/**
 * @author sunxin
 * @date 2021/2/28 13:26
 * 主任
 **/
public class Director extends Approver {

    public Director(String name) {
        super(name);
    }

    @Override
    public void setSuccessor(Approver successor) {
        this.successor = successor;
    }

    @Override
    public void processRequest(PurchaseRequest request) {
        if (request.getAmount() < 50){
            System.out.println("主任审批了");
        }else {
            this.successor.processRequest(request);
        }
    }
}
