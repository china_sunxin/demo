package com.sunxin.demo.design.interceptor;

/**
 * @author sunxin
 * @date 2021/2/28 13:24
 * 审批者
 **/
public abstract class Approver {

    protected Approver successor;

    protected String name;

    public Approver(String name) {
        this.name = name;
    }

    public void setSuccessor(Approver successor) {
        this.successor = successor;
    }

    public abstract void processRequest(PurchaseRequest request);
}
