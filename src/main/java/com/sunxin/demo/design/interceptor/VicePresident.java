package com.sunxin.demo.design.interceptor;

/**
 * @author sunxin
 * @date 2021/2/28 13:30
 * 副董事长
 **/
public class VicePresident extends Approver{

    public VicePresident(String name) {
        super(name);
    }

    @Override
    public void setSuccessor(Approver successor) {
        this.successor = successor;
    }

    @Override
    public void processRequest(PurchaseRequest request) {
        if (request.getAmount() < 100){
            System.out.println("副董事长审批了");
        }else {
            this.successor.processRequest(request);
        }
    }

}
