package com.sunxin.demo.design.interceptor;

/**
 * @author sunxin
 * @date 2021/2/28 13:32
 **/
public class InterceptorExec {

    public static void main(String[] args) {
        Approver director = new Director("director");
        Approver vicePresident = new VicePresident("vicePresident");
        Approver president = new President("president");

        director.setSuccessor(vicePresident);
        vicePresident.setSuccessor(president);

        PurchaseRequest purchaseRequest = new PurchaseRequest(1000,10001,"买办公用品");
        director.processRequest(purchaseRequest);
    }

}
