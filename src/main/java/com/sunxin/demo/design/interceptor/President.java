package com.sunxin.demo.design.interceptor;

/**
 * @author sunxin
 * @date 2021/2/28 13:30
 * 董事长
 **/
public class President extends Approver{

    public President(String name) {
        super(name);
    }

    @Override
    public void setSuccessor(Approver successor) {
        this.successor = successor;
    }


    @Override
    public void processRequest(PurchaseRequest request) {
        System.out.println("董事长审批了");
    }

}
