package com.sunxin.demo.design.visitor;

/**
 * @author q
 */
public interface Element {
    public void accept(Visitor visitor);
}
