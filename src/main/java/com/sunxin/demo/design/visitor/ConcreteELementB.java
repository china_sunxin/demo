package com.sunxin.demo.design.visitor;

/**
 * @author q
 */
public class ConcreteELementB implements Element{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
