package com.sunxin.demo.design.visitor;

/**
 * @author q
 */
public class ConcreteVisitor extends Visitor {
    @Override
    public void visit(ConcreteELementA eLementA) {
        System.out.println("operate elementA");
    }

    @Override
    public void visit(ConcreteELementB eLementB) {
        System.out.println("operate elementB");
    }
}
