package com.sunxin.demo.design.visitor;

import com.sun.deploy.security.BadCertificateDialog;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author q
 */
public class ObjectStructure {

    private ArrayList<Element> list = new ArrayList<>();

    public void accept(Visitor visitor){
        Iterator<Element> iterator = list.iterator();
        while (iterator.hasNext()){
            iterator.next().accept(visitor);
        }
    }

    public void addElement(Element element){
        list.add(element);
    }

    public void removeElememt(Element element){
        list.remove(element);
    }

    public static void main(String[] args) {
        ObjectStructure objectStructure = new ObjectStructure();
        objectStructure.addElement(new ConcreteELementA());
        objectStructure.addElement(new ConcreteELementA());
        objectStructure.addElement(new ConcreteELementB());

        ConcreteVisitor concreteVisitor = new ConcreteVisitor();
        objectStructure.accept(concreteVisitor);
    }
}
