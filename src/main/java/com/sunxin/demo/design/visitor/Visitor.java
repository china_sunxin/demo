package com.sunxin.demo.design.visitor;

/**
 * @author q
 */
public abstract class Visitor {

    public abstract void visit(ConcreteELementA eLementA);
    public abstract void visit(ConcreteELementB eLementB);

}
