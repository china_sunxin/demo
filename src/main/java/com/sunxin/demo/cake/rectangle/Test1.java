package com.sunxin.demo.cake.rectangle;

public class Test1 {

    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle();

        boolean interSection = isInterSection(rectangle1, rectangle2);
        if (!interSection){
            System.out.println("两个矩形没有相交");
            return;
        }


    }

    private static boolean isInterSection(Rectangle rectangle1,Rectangle rectangle2) {
        if(
                in(rectangle1.getLeftX(),rectangle2.getLeftX(),rectangle2.getRightX())||
                in(rectangle1.getRightX(),rectangle2.getLeftX(),rectangle2.getRightX())||
                in(rectangle1.getBottomY(),rectangle2.getBottomY(),rectangle2.getTopY())||
                in(rectangle1.getTopY(),rectangle2.getBottomY(),rectangle2.getTopY())
        ){
            return true;
        }
        return false;
    }

    private static boolean in(int a,int small,int large){
        if(a > small && a < large ){
            return true;
        }
        return false;
    }
}
