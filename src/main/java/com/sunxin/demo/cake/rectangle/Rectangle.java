package com.sunxin.demo.cake.rectangle;

import lombok.Data;

@Data
public class Rectangle {

    private int leftX;

    private int bottomY;

    private int width;
    private int heigth;

    public Rectangle(int leftX, int bottomY, int width, int heigth) {
        this.leftX = leftX;
        this.bottomY = bottomY;
        this.width = width;
        this.heigth = heigth;
    }

    public Rectangle() {
    }

    public int getRightX(){
        return this.leftX + width;
    }

    public int getTopY(){
        return this.bottomY + heigth;
    }
}
