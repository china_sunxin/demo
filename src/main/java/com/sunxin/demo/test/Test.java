package com.sunxin.demo.test;

import org.springframework.stereotype.Service;

import java.util.Enumeration;
import java.util.ResourceBundle;

@Service
public class Test {


    public static void main(String[] args) {
        ResourceBundle test = ResourceBundle.getBundle("test");
        Enumeration<String> keys = test.getKeys();
        while (keys.hasMoreElements()){
            String s = keys.nextElement();
            String string = test.getString(s);
            System.out.println(string);
        }
    }

}
