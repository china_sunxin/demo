package com.sunxin.demo.leetcode.zuo;

/**
 * @author sunxin
 * @date 2021/3/13 16:12
 * 给定一个有序数组，从左到右依次表示X轴上从左往右的位置，给定一个正整数K，
 * 返回如果有一根长度为K的绳子，最多能盖住几个点。绳子的边缘点碰到X轴上的点，也算盖住。
 **/
public class Question01 {

    public static void main(String[] args) {
        int[] arr = {1,3,4,5,7,8,9};
        int k = 8;
        int num = getNum2(arr, k);
        System.out.println(num);
    }


    private static int getNum2(int[] arr, int L) {
        int left = 0;
        int right = 0;
        int N = arr.length;
        int max = 0;

        while (left < N){
            while (right < N && arr[right] - arr[left] <= L){
                right++;
            }
            max = Math.max(max,right - left);
            left++;
        }
        return max;
    }

    /**
     * 暴力
     * @param arr
     * @param k
     */
    private static int getNum1(int[] arr, int k) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(findNum(i, arr, k) - i + 1,max);
        }
        return max;



    }

    private static int findNum(int l, int[] arr, int k) {
        int r = arr.length - 1;
        int index = l;
        while (l <= r){
            int mid = l + ((r - l) >> 1);
            if (arr[mid] > k){
                r = mid - 1;
            }else {
                index = mid;
                l = mid + 1;
            }
        }
        return index;




    }

}
