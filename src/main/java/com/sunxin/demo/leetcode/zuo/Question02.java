package com.sunxin.demo.leetcode.zuo;

/**
 * @author sunxin
 * @date 2021/3/13 17:48
 * 括号有效配对：
 * 任何一个左括号都能找到和其正确配对的右括号
 * 任何一个右括号都能找到和其正确配对的左括号
 * 判断一个括号字符串有效
 * 如果一个括号字符串无效，返回至少填几个字符串让其整体有效
 **/
public class Question02 {

    public static void main(String[] args) {
        String str = ")))(()(";
        boolean valid = isValid(str);
        System.out.println(valid);
        int num = getNum(str);
        System.out.println(num);
    }

    private static int getNum(String str) {
        int count = 0;
        int need = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '('){
                count ++;
            }else {
                if (count == 0){
                    need++;
                }else {
                    count--;
                }
            }
        }
        return need + count;
    }

    private static boolean isValid(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (count < 0){
                return false;
            }
            if (str.charAt(i) == '('){
                count ++;
            }else {
                count--;
            }
        }
        return count == 0;
    }
}
