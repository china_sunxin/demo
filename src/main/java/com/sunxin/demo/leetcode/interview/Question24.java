package com.sunxin.demo.leetcode.interview;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author sunxin02
 * 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。
 */
public class Question24 {

    public static void main(String[] args) {
        Question24 question = new Question24();
        int[] arr = {1,2,3};
        ListNode listNode = question.swapPairs(generateListNode(arr));
        printListNode(listNode);
    }

    public ListNode swapPairs(ListNode head) {
        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;
        ListNode temp = dummyHead;
        while (temp != null && temp.next != null) {
            ListNode node1 = temp.next;
            ListNode node2 = temp.next.next;
            temp.next = node2;
            node1.next = node2.next;
            node2.next = node1;
            temp = node1;


            temp = temp.next.next;
        }
        return dummyHead.next;
    }

    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    static ListNode generateListNode(int[] arr){
        ListNode head = new ListNode(arr[0]);
        ListNode cur = head;
        for (int i = 1; i < arr.length; i++) {
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }
        return head;
    }

    static void printListNode(ListNode head){
        while (head != null){
            System.out.println(head.val);
            head = head.next;
        }
    }


}
