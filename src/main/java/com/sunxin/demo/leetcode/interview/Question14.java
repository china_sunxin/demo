package com.sunxin.demo.leetcode.interview;

/**
 * @author sunxin
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 * 如果不存在公共前缀，返回空字符串 ""。
 * 输入：strs = ["flower","flow","flight"]
 * 输出："fl"
 */
public class Question14 {


    public static void main(String[] args) {
//        String[] strs = {"flower","flow","flight"};
        String[] strs = {};
        Question14 question14 = new Question14();
        String s = question14.longestCommonPrefix(strs);
        System.out.println(s);
    }

    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0){
            return "";
        }
        int index = 0;
        StringBuilder res = new StringBuilder();
        while (true){
            if (strs[0].length() <= index){
                return res.toString();
            }
            char c = strs[0].charAt(index);
            for (int i = 1; i < strs.length; i++) {
                if (strs[i].length() <= index){
                    return res.toString();
                }
                if (c != strs[i].charAt(index)){
                    return res.toString();
                }
            }
            index++;
            res.append(c);
        }
    }
}
