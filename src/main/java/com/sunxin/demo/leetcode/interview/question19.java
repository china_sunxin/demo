package com.sunxin.demo.leetcode.interview;

/**
 * 给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
 *
 * 进阶：你能尝试使用一趟扫描实现吗？
 */

public class question19 {

    public static void main(String[] args) {
        question19 question = new question19();
        int[] arr = {1,2,3,4,5};
        ListNode listNode = generateListNode(arr);
        ListNode listNode1 = question.removeNthFromEnd(listNode, 4);
        printListNode(listNode1);
    }



    public ListNode removeNthFromEnd(ListNode head, int n) {
        if (head.next == null){
            return null;
        }
        ListNode fast = head;
        ListNode slow = head;
        while (n-- > 0){
            fast = fast.next;
        }
        if (fast == null){
            ListNode cur = head.next;
            head.next = null;
            return cur;
        }
        while (fast.next != null){
            fast = fast.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return head;
    }



    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    static ListNode generateListNode(int[] arr){
        ListNode head = new ListNode(arr[0]);
        ListNode cur = head;
        for (int i = 1; i < arr.length; i++) {
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }
        return head;
    }

    static void printListNode(ListNode head){
        while (head != null){
            System.out.println(head.val);
            head = head.next;
        }
    }
}
