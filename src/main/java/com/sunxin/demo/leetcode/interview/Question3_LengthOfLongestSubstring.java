package com.sunxin.demo.leetcode.interview;

import lombok.val;

import java.util.*;

/**
 * 给定一个字符串，请你找出其中不含有重复字符的 最长子串的长度。
 * @author sunxin
 */
public class Question3_LengthOfLongestSubstring {

    public static void main(String[] args) {
        Question3_LengthOfLongestSubstring exe = new Question3_LengthOfLongestSubstring();
        int i = exe.lengthOfLongestSubstring("abba");
    }

    public int lengthOfLongestSubstring2(String s) {
        if (s.length()==0) {
            return 0;
        };
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        int max = 0;
        int left = 0;
        for(int i = 0; i < s.length(); i ++){
            if(map.containsKey(s.charAt(i))){
                left = Math.max(map.get(s.charAt(i)) + 1,left);
            }
            map.put(s.charAt(i),i);
            max = Math.max(max,i-left+1);
        }
        return max;
    }

    public int lengthOfLongestSubstring(String s) {
        Set<Character> hashSet = new HashSet<Character>();
        int n = s.length();
        int right = -1;
        int max = 0;
        for (int i = 0; i < n; ++i) {
            if (i != 0) {
                hashSet.remove(s.charAt(i - 1));
            }
            while (right + 1 < n && !hashSet.contains(s.charAt(right + 1))) {
                hashSet.add(s.charAt(right + 1));
                ++right;
            }
            max = Math.max(max, right - i + 1);
        }
        return max;
    }

}
