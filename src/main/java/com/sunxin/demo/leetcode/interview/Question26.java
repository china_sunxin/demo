package com.sunxin.demo.leetcode.interview;

import java.util.Arrays;

/**
 * @author sunxin02
 * 给定一个排序数组，你需要在 原地 删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 */
public class Question26 {

    public static void main(String[] args) {
        int[] arr = {1,1,2,3,3,4,5,5,6,7};
        Question26 question = new Question26();
        int i = question.removeDuplicates(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(i);
    }



    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0){
            return 0;
        }
        int slow = 0;
        int fast = 1;
        int tem = nums[0];
        while (fast < nums.length){
            if(nums[fast] != tem){
                tem = nums[fast];
                nums[++slow] = tem;
            }
            fast++;
        }
        return slow + 1;
    }

    public int removeDuplicates1(int[] nums) {
        if (nums == null || nums.length == 0){
            return 0;
        }
        int slow = 0;
        int fast = 1;
        int tem = nums[0];
        while (fast < nums.length){
            if(nums[fast] != nums[slow]){
                nums[++slow] = nums[fast];
            }
            fast++;
        }
        return slow + 1;
    }

}
