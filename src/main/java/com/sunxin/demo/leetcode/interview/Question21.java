package com.sunxin.demo.leetcode.interview;

/**
 * @author sunxin02
 */
public class Question21 {

    public static void main(String[] args) {
        Question21 question = new Question21();
        int[] arr1 = {1,2,4};
        int[] arr2 = {1,3,4};
        ListNode listNode1 = generateListNode(arr1);
        ListNode listNode2 = generateListNode(arr2);
        ListNode mergedListNode = question.mergeTwoLists(listNode1, listNode2);
        printListNode(mergedListNode);
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode prehead = new ListNode(-1);
        ListNode prev = prehead;

        while (l1 != null && l2 != null){
            if (l1.val <= l2.val){
                prev.next = l1;
                l1 = l1.next;
            }else {
                prev.next = l2;
                l2 = l2.next;
            }
            prev = prev.next;
        }
        prev.next = l1 == null ? l2 : l1;
        return prehead.next;
    }


    public ListNode mergeTwoLists1(ListNode l1, ListNode l2) {
        if (l1 == null){
            return l2;
        }
        if (l2 == null){
            return l1;
        }
        //头节点小的链表
        ListNode small = l1.val <= l2.val ? l1 : l2;
        ListNode big = l1.val > l2.val ? l1 : l2;
        ListNode head = small;
        while (small.next != null){
            if(small.next.val >= big.val) {
                ListNode tem = small.next;
                small.next = big;
                big = tem;
            }
            small = small.next;
        }
        small.next = big;
        return head;
    }


    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    static ListNode generateListNode(int[] arr){
        ListNode head = new ListNode(arr[0]);
        ListNode cur = head;
        for (int i = 1; i < arr.length; i++) {
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }
        return head;
    }

    static void printListNode(ListNode head){
        while (head != null){
            System.out.println(head.val);
            head = head.next;
        }
    }

}
