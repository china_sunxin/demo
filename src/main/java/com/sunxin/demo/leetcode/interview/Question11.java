package com.sunxin.demo.leetcode.interview;

/**
 * @author sunxin02
 */
public class Question11 {

    public static void main(String[] args) {
        Question11 question11 = new Question11();
        int[] height = {1,8,6,2,5,4,8,3,7};
        int res = question11.maxArea(height);
        System.out.println(res);
    }


    public int maxArea1(int[] height) {
        int lenght = height.length;
        int maxArea = 0;

        for (int i = 0; i < lenght - 1; i++) {
            for (int j = i + 1; j < lenght; j++) {
                maxArea = Math.max(maxArea,(j - i) * Math.min(height[i],height[j]));
            }
        }
        return maxArea;
    }

    /**
     * 暴力解法
     * @param height
     * @return
     */
    public int maxArea(int[] height) {
        int maxArea = 0;
        int left = 0;
        int right = height.length - 1;
        while (left < right){
            if (height[right] > height[left]){
                maxArea = Math.max((right - left) * (height[left++]),maxArea);
            }else {
                maxArea = Math.max((right - left) * (height[right--]),maxArea);
            }
        }
        return maxArea;
    }
}
