package com.sunxin.demo.leetcode.interview;

/**
 * @author sunxin02
 * 实现 strStr() 函数。
 *
 * 给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。如果不存在，则返回  -1。
 */
public class Question28 {

    public static void main(String[] args) {
        Question28 question28 = new Question28();
        int i = question28.strStr("hello", "ll");
        System.out.println(i);
    }


    public int strStr(String haystack, String needle) {
        if (haystack.length() < needle.length()){
            return -1;
        }
        for1:
        for (int i = 0; i < haystack.length() - needle.length() + 1; i++) {
            for (int j = 0; j < needle.length(); j++) {
                if (haystack.charAt(i + j) != needle.charAt(j)){
                    i += j - 1;
                    continue for1;
                }
            }
            return i;
        }
        return -1;
    }

}
