package com.sunxin.demo.leetcode.interview;


import javafx.scene.control.TableColumnBase;

/**
 * @author sunxin02
 */
public class Question5_LongestPalindrome {
    public static void main(String[] args) {
        Question5_LongestPalindrome exe = new Question5_LongestPalindrome();
        String s = exe.longestPalindrome("abdcdba");
        System.out.println(s);
    }

    /**
     * 动态规划
     * @param s
     * @return
     */
    public String longestPalindrome(String s) {
        char[] chars = s.toCharArray();
        int lenght = chars.length;
        boolean[][] dp = new boolean[lenght][lenght];
        String res = "";
        for (int i = lenght - 1; i >= 0; i--) {
            for (int j = 0; j < lenght; j++) {
                if (i > j){
                    dp[i][j] = false;
                    continue;
                }
                if (i == j){
                    dp[i][j] = true;
                }else if (Math.abs(i - j) == 1){
                    dp[i][j] = chars[i] == chars[j];
                }else {
                    dp[i][j] = dp[i + 1][j - 1] && chars[i] == chars[j];
                }
                if (dp[i][j]) {
                    res = res.length() > j - i + 1 ? res : s.substring(i,j + 1);
                }
//                System.out.println(dp[i][j]);
//                System.out.println(s.substring(i,j + 1));
            }
        }
        return res;
    }
}
