package com.sunxin.demo.leetcode.interview;

/**
 * @author sunxin02
 * 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 *
 * 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-element
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Question27 {

    public static void main(String[] args) {
        int[] arr = {3,2,2,3};
        int value = 3;
        int i1 = removeElement1(arr, value);
        for (int i = 0; i < i1; i++) {
            System.out.println(arr[i]);
        }
    }


    public static int removeElement(int[] nums, int val) {
        int left = -1;
        int index = 0;
        int right = nums.length - 1;
        while (left < right){
            if (nums[index] == val){
                nums[index] = nums[index] ^ nums[right];
                nums[right] = nums[index] ^ nums[right];
                nums[index] = nums[index] ^ nums[right];
                right--;
            }else {
                index++;
                left++;
            }
        }
        return left + 1;
    }

    public static int removeElement1(int[] nums, int val) {
        int j = nums.length - 1;
        for (int i = 0; i <= j; i++) {
            if (nums[i] == val) {
                swap(nums, i--, j--);
            }
        }
        return j + 1;
    }
    static void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

}
