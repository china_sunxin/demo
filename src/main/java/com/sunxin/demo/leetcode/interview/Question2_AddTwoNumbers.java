package com.sunxin.demo.leetcode.interview;

/**
 * @author sunxin02
 */
public class Question2_AddTwoNumbers {

    public static void main(String[] args) {

        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(9);
        ListNode listNode3 = new ListNode(9);
        ListNode listNode4 = new ListNode(9);
        ListNode listNode5 = new ListNode(9);
        ListNode listNode6 = new ListNode(9);
        ListNode listNode7 = new ListNode(9);
        ListNode listNode8 = new ListNode(9);
        ListNode listNode9 = new ListNode(9);

        ListNode listNode0 = new ListNode(9);

        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        listNode6.next = listNode7;
        listNode7.next = listNode8;
        listNode8.next = listNode9;

        ListNode listNode = addTwoNumbers(listNode1, listNode0);
        ListNode currNode = listNode;
        while (currNode != null){
            System.out.println(currNode.val);
            currNode = currNode.next;
        }

    }


    private static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        ListNode cur = head;
        int up = 0;
        while (l1 != null && l2 != null){
            cur.next = new ListNode((l1.val + l2.val + up) % 10);
            cur = cur.next;
            up = (l1.val + l2.val + up) / 10;
            l1 = l1.next;
            l2 = l2.next;
        }
        while (l1 != null){
            cur.next = new ListNode((l1.val + up) % 10);
            cur = cur.next;
            up = (l1.val + up) / 10;
            l1 = l1.next;
        }
        while (l2 != null){
            cur.next = new ListNode((l2.val + up) % 10);
            cur = cur.next;
            up = (l2.val + up) / 10;
            l2 = l2.next;
        }
        cur.next = (up == 0 ? null : new ListNode(up));
        return head.next;
    }

    private static ListNode addTwoNumbers2(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        ListNode cur = head;
        int up = 0;
        while (l1 != null && l2 != null){
            up += l1.val + l2.val;
            cur.next = new ListNode(up % 10);
            cur = cur.next;
            up = up / 10;
            l1 = l1.next;
            l2 = l2.next;
        }
        while (l1 != null){
            up += l1.val;
            cur.next = new ListNode(up % 10);
            cur = cur.next;
            up = up/ 10;
            l1 = l1.next;
        }
        while (l2 != null){
            up += l2.val;
            cur.next = new ListNode(up % 10);
            cur = cur.next;
            up = up/ 10;
            l2 = l2.next;
        }
        if(up != 0){
            cur.next = new ListNode(up);
        }
        return head.next;
    }


    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }
}



