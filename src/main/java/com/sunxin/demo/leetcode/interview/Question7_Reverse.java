package com.sunxin.demo.leetcode.interview;

/**
 * @author sunxin02
 */
public class Question7_Reverse {

    public static void main(String[] args) {
        Question7_Reverse exe = new Question7_Reverse();
        int reverse = exe.reverse(1234);
        System.out.println(reverse);
    }

    //1234
    public int reverse(int x) {
        int result = 0;
        while(x != 0) {
            int tmp = result;
            result = (result * 10) + (x % 10);
            x /= 10;
            if (result / 10 != tmp) {
                return 0;
            }
        }
        return result;
    }

    public int reverse2(int x) {
        int rev = 0;
        while (x != 0) {
            int pop = x % 10;
            x /= 10;
            if (rev > Integer.MAX_VALUE/10 || (rev == Integer.MAX_VALUE / 10 && pop > 7)) {
                return 0;
            }
            if (rev < Integer.MIN_VALUE/10 || (rev == Integer.MIN_VALUE / 10 && pop < -8)) {
                return 0;
            }
            rev = rev * 10 + pop;
        }
        return rev;
    }

}
