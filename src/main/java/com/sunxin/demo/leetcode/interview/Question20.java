package com.sunxin.demo.leetcode.interview;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author sunxin02
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
 *
 * 有效字符串需满足：
 *
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 */
public class Question20 {


    public static void main(String[] args) {
        Question20 question = new Question20();
//        boolean valid = question.isValid("([)]");
        boolean valid = question.isValid("([{}])");
        System.out.println(valid);
        //40
        int a = '(';
        //41
        int b = ')';
        //91
        int c = '[';
        //93
        int d = ']';
        //123
        int e = '{';
        //125
        int f = '}';
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
    }



    public boolean isValid(String s) {
        //40
        int a = '(';
        //41
        int b = ')';
        //91
        int c = '[';
        //93
        int d = ']';
        //123
        int e = '{';
        //125
        int f = '}';
        if (s.length() == 0){
            return true;
        }
        if (s.length() % 2 == 1){
            return false;
        }
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char aChar = s.charAt(i);
            if (!stack.isEmpty() && (aChar - stack.peek() > 0) && (aChar - stack.peek() < 5) ) {
                stack.pop();
            } else {
                stack.push(aChar);
            }
        }
        return stack.isEmpty();
    }
}
