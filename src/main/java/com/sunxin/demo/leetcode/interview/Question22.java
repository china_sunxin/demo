package com.sunxin.demo.leetcode.interview;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author sunxin02
 * 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。
 */
public class Question22 {

    public static void main(String[] args) {
        Question22 question = new Question22();
        List<String> strings = question.generateParenthesis(1);
        System.out.println(JSON.toJSONString(strings));
    }

    public List<String> generateParenthesis(int n) {
        ArrayList<String> result = new ArrayList<>();
        return generateParenthesis1(new char[2 * n],0,result);
    }

    public List<String> generateParenthesis1(char[] chars,int index,List<String> result) {
        if (index == chars.length){
            String s = new String(chars);
            if (isValid(s)) {
                result.add(s);
            }
        }else {
            chars[index] = ')';
            generateParenthesis1(chars, index + 1, result);
            chars[index] = '(';
            generateParenthesis1(chars, index + 1, result);
        }
        return result;
    }

    public boolean isValid(String s) {
        if (s.length() == 0){
            return true;
        }
        if (s.length() % 2 == 1){
            return false;
        }
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char aChar = s.charAt(i);
            if (!stack.isEmpty() && (aChar - stack.peek() > 0) && (aChar - stack.peek() < 5) ) {
                stack.pop();
            } else {
                stack.push(aChar);
            }
        }
        return stack.isEmpty();
    }

}
