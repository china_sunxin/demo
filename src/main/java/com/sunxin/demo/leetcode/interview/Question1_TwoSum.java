package com.sunxin.demo.leetcode.interview;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sunxin
 */
public class Question1_TwoSum {

    public static void main(String[] args) {
        int[] arr = {2,7,11,15};
        int[] ints = new Question1_TwoSum().twoSum(arr, 9);
        System.out.println(Arrays.toString(ints));
    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0;i < nums.length; i++){
            if(map.containsKey(target - nums[i])){
                return new int[]{map.get(target - nums[i]),i};
            }
            map.put(nums[i],i);
        }
        return new int[]{-1,-1};
    }

}
