package com.sunxin.demo.leetcode.interview;

import com.alibaba.fastjson.JSON;

import java.util.Arrays;
import java.util.List;

/**
 * @author sunxin02
 */
public class Question16 {

    public static void main(String[] args) {
        Question16 question16 = new Question16();
        int[] arr = {-1,2,1,-4};
        int res = question16.threeSumClosest(arr, 1);
        System.out.println(res);
    }

    public int threeSumClosest(int[] nums, int target) {
        int res = nums[0] + nums[1] + nums[2];
        int length = nums.length;
        Arrays.sort(nums);
        for (int i = 0; i < length; i++) {
            if(i > 0 && nums[i] == nums[i-1]){
                continue;
            }
            int l = i + 1;
            int r = length - 1;
            while (l < r){
                int sum = nums[i] + nums[l] + nums[r];
                if(Math.abs(target - sum) < Math.abs(target - res)) {
                    res = sum;
                }
                if (sum == target){
                    return target;
                }else if (sum < target){
                    l++;
                }else {
                    r--;
                }
            }
        }
        return res;
    }

}
