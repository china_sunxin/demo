package com.sunxin.demo.encrypt;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.HmacUtils;

public class DemoTest {

    public static void main(String[] args) {
        String s = Base64.encodeBase64String(HmacUtils.hmacSha1("xEPl8XXgCMLlL0N2/iksonYHiq3QfFK1", "requestRefId=4211573c-5edc-4c3b-b887-1ad6f8b0913c&secretId=RRRD001"));
        System.out.println(s);
    }
}
