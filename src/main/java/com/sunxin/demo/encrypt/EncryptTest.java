package com.sunxin.demo.encrypt;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.security.Key;

import static com.sunxin.demo.encrypt.DESedeCoder.CIPHER_ALGORITHM;
import static com.sunxin.demo.encrypt.DESedeCoder.KEY_ALGORITHM;

public class EncryptTest {

    public static void main(String[] args) throws Exception {
        byte[] a = encrypt("hello".getBytes(), "5m2FvFHxQAT9FjiAHFRt5P3ap0Am72TH".getBytes());
        System.out.println(Base64.encodeBase64String(a));

        byte[] b = decrypt(a,"5m2FvFHxQAT9FjiAHFRt5P3ap0Am72TH".getBytes());
        System.out.println(new String(b));

    }

    public static byte[] encrypt(byte[] data,byte[] key) throws Exception{
        //实例化Des密钥
        DESedeKeySpec dks=new DESedeKeySpec(key);
        //实例化密钥工厂
        SecretKeyFactory keyFactory=SecretKeyFactory.getInstance(KEY_ALGORITHM);
        //生成密钥
        Key k=keyFactory.generateSecret(dks);
        //实例化
        Cipher cipher=Cipher.getInstance(CIPHER_ALGORITHM);
        //初始化，设置为加密模式
        cipher.init(Cipher.ENCRYPT_MODE, k);
        //执行操作
        return cipher.doFinal(data);
    }
    /**
     * 解密数据
     * @param data 待解密数据
     * @param key 密钥
     * @return byte[] 解密后的数据
     * */
    public static byte[] decrypt(byte[] data,byte[] key) throws Exception{
        //实例化Des密钥
        DESedeKeySpec dks=new DESedeKeySpec(key);
        //实例化密钥工厂
        SecretKeyFactory keyFactory=SecretKeyFactory.getInstance(KEY_ALGORITHM);
        //生成密钥
        Key k=keyFactory.generateSecret(dks);
        //实例化
        Cipher cipher=Cipher.getInstance(CIPHER_ALGORITHM);
        //初始化，设置为解密模式
        cipher.init(Cipher.DECRYPT_MODE, k);
        //执行操作
        return cipher.doFinal(data);
    }

}
