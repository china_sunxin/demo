package com.sunxin.demo.encrypt;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import org.apache.commons.codec.binary.Base64;
/**
 * DESede对称加密算法演示
 * @author kongqz
 * */
public class DESedeCoder {
    /**
     * 密钥算法
     * */
    public static final String KEY_ALGORITHM="DESede";

    /**
     * 加密/解密算法/工作模式/填充方式
     * */
    public static final String CIPHER_ALGORITHM="DESede/ECB/PKCS5Padding";

    /**
     * 加密数据
     * @param data 待加密数据
     * @param key 密钥
     * @return byte[] 加密后的数据
     * */
    public static byte[] encrypt(byte[] data,byte[] key) throws Exception{
        //实例化Des密钥
        DESedeKeySpec dks=new DESedeKeySpec(key);
        //实例化密钥工厂
        SecretKeyFactory keyFactory=SecretKeyFactory.getInstance(KEY_ALGORITHM);
        //生成密钥
        Key k=keyFactory.generateSecret(dks);
        //实例化
        Cipher cipher=Cipher.getInstance(CIPHER_ALGORITHM);
        //初始化，设置为加密模式
        cipher.init(Cipher.ENCRYPT_MODE, k);
        //执行操作
        return cipher.doFinal(data);
    }
    /**
     * 解密数据
     * @param data 待解密数据
     * @param key 密钥
     * @return byte[] 解密后的数据
     * */
    public static byte[] decrypt(byte[] data,byte[] key) throws Exception{
        //实例化Des密钥
        DESedeKeySpec dks=new DESedeKeySpec(key);
        //实例化密钥工厂
        SecretKeyFactory keyFactory=SecretKeyFactory.getInstance(KEY_ALGORITHM);
        //生成密钥
        Key k=keyFactory.generateSecret(dks);
        //实例化
        Cipher cipher=Cipher.getInstance(CIPHER_ALGORITHM);
        //初始化，设置为解密模式
        cipher.init(Cipher.DECRYPT_MODE, k);
        //执行操作
        return cipher.doFinal(data);
    }
    /**
     * 进行加解密的测试
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        String str="hello";
        System.out.println("原文："+str);
        //初始化密钥
        byte[] key="5m2FvFHxQAT9FjiAHFRt5P3ap0Am72TH".getBytes();
        //加密数据
        byte[] data=DESedeCoder.encrypt(str.getBytes(), key);
        System.out.println("加密后：/t"+Base64.encodeBase64String(data));
        //解密数据
        data=DESedeCoder.decrypt(data, key);
        System.out.println("解密后："+new String(data));
    }
}