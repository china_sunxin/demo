package com.sunxin.demo.encrypt;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.Key;

public class DesTest {

    // 二进制转换字符串
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    // 加密
    public byte[] encode(byte[] source) throws Exception {
        DESKeySpec deSedeKeySpec = new DESKeySpec("5m2FvFHxQAT9FjiAHFRt5P3ap0Am72TH".getBytes());
        SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
        Key convertSecretKey = factory.generateSecret(deSedeKeySpec);

        KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
        keyGenerator.init(56);
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.ENCRYPT_MODE, convertSecretKey);
        byte[] encodeSource = cipher.doFinal(source);
        return encodeSource;
    }
    // 解密
    public String decode(byte[] encodeSource) throws Exception {
        DESKeySpec deSedeKeySpec = new DESKeySpec("5m2FvFHxQAT9FjiAHFRt5P3ap0Am72TH".getBytes());
        SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
        Key convertSecretKey = factory.generateSecret(deSedeKeySpec);
        KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
        keyGenerator.init(56);
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.DECRYPT_MODE, convertSecretKey);
        byte[] decodeRes = cipher.doFinal(encodeSource);
        return new String(decodeRes, "UTF-8");
    }
    public static void main(String[] args) throws Exception {
        DesTest dt = new DesTest();
        String obj = "就先测试这句吧";
        System.out.println("加密前:" + obj);
        byte[] source = dt.encode(obj.getBytes("UTF-8"));
        System.out.println("加密后:" + bytesToHexString(source));
        String res = dt.decode(source);
        System.out.println("解密后:" + res);
    }
}
