package com.sunxin.demo.encrypt;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.security.Key;

public class EncryptTest1 {

    public String encrypt3DES(String str, String key) {
        try {
            //实例化Des密钥
            DESedeKeySpec dks = new DESedeKeySpec(key.getBytes());
            //实例化密钥工厂
            SecretKeyFactory keyFactory=SecretKeyFactory.getInstance("DESede");
            //生成密钥
            Key k=keyFactory.generateSecret(dks);
            //实例化
            Cipher cipher=Cipher.getInstance("DESede/ECB/PKCS5Padding");
            //初始化，设置为加密模式
            cipher.init(Cipher.ENCRYPT_MODE, k);
            //执行操作
            byte[] bytes = cipher.doFinal(str.getBytes());
            return Base64.encodeBase64String(bytes);
        } catch (Exception e) {
//            logger.error("百行特别关注名单新版接口请求报文加密异常",e);
            return null;
        }
    }

    private String decrypt(String decryptKey, String data) {
        if (StringUtils.isBlank(decryptKey) || StringUtils.isBlank(data)) {
            return null;
        }
        try {
            //实例化Des密钥
            DESedeKeySpec dks=new DESedeKeySpec(decryptKey.getBytes());
            //实例化密钥工厂
            SecretKeyFactory keyFactory=SecretKeyFactory.getInstance("DESede");
            //生成密钥
            Key k=keyFactory.generateSecret(dks);
            //实例化
            Cipher cipher=Cipher.getInstance("DES/ECB/PKCS5Padding");
            //初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, k);
            //执行操作
            byte[] bytes = cipher.doFinal(data.getBytes());
            return new String(bytes);
        } catch (Exception e) {
//            logger.error("解密时出错，原文：{}" + data, e);
            return null;
        }
    }
}
