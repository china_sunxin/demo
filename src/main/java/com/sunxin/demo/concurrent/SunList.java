package com.sunxin.demo.concurrent;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author sunxin
 */
public class SunList {
    public static void main(String[] args) throws InterruptedException {
        final ReentrantLock lock = new ReentrantLock();
        CopyOnWriteArrayList<Integer> arr = new CopyOnWriteArrayList<>();

        new Thread(() -> {
            try {
                lock.lock();
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }).start();
        Thread.sleep(100);
        new Thread(() -> {
            try {
                lock.lock();
                System.out.println("removed");
            }finally {
                lock.unlock();
            }
        }).start();

    }
}
