package com.sunxin.demo.concurrent.interrupt;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author sunxin
 */
public class TestInterrupt {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() ->{

        });
        Thread.sleep(1000);
        System.out.println(thread.isInterrupted());
        thread.interrupt();
        try {
            thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(thread.isInterrupted());
        Thread.sleep(1000);
    }

    public static void futureCancle(){
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<String> result = executorService.submit(() -> {
            System.out.println("任务执行了");
            return "";
        });
        executorService.shutdownNow();
        boolean cancel = result.cancel(true);
    }


}
