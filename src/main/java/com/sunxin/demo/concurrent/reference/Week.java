package com.sunxin.demo.concurrent.reference;

import java.lang.ref.WeakReference;

/**
 * @author sunxin
 */
public class Week {

    public static void main(String[] args) {
        WeakReference<T> reference = new WeakReference<>(new T());
        System.out.println(reference.get());
        System.gc();
        System.out.println(reference.get());

        ThreadLocal<T> threadLocal = new ThreadLocal<>();
        ThreadLocal<T> threadLocal2 = new ThreadLocal<>();


        threadLocal.set(new T());
        threadLocal.remove();
    }


}
