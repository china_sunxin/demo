package com.sunxin.demo.concurrent.reference;

import java.lang.ref.SoftReference;

/**
 * @author sunxin
 */
public class Soft {
    public static void main(String[] args) throws InterruptedException {
        SoftReference<byte[]> softReference = new SoftReference<>(new byte[1024*1024*10*122]);
        System.out.println(softReference.get());
        System.gc();
        Thread.sleep(1000);
        System.out.println(softReference.get());
        byte[] bytes = new byte[1024 * 1024 * 15];
        System.out.println(softReference.get());
    }

    @Override
    public void finalize(){
        System.out.println("gc 执行了");
    }
}
