package com.sunxin.demo.concurrent.reference;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author sunxin
 */
public class Phantom {
    private static final LinkedList<byte[]> list = new LinkedList<byte[]>();
    private static final ReferenceQueue<T> queue = new ReferenceQueue<>();
    public static void main(String[] args) {
        PhantomReference<T> reference = new PhantomReference<>(new T(), queue);
        new Thread(() -> {
            while (true){
                list.add(new byte[1024 * 1024]);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(reference.get());
            }
        }).start();

        new Thread(() -> {
            while (true){
                Reference<? extends T> poll = queue.poll();
                if(poll != null){
                    System.out.println("虚引用对象被jvm对象回收了:" + poll);
                }
            }
        }).start();
    }


}
