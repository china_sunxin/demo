package com.sunxin.demo.concurrent;

/**
 * @author sunxin02
 */
public class Main2 {

    public static void main(String[] args) {

        Object object = new Object();

        new Thread(() -> {
            synchronized (object){
                try {
                    System.out.println("a");
                    Thread.sleep(100000000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();



        new Thread(() -> {
            synchronized (object){
                System.out.println("b");
            }
        }).start();
    }

}
