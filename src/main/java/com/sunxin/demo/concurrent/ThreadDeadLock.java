package com.sunxin.demo.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author sunxin
 */
public class ThreadDeadLock {



    public static void main(String[] args) throws Exception {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        RenderPageTask task = new RenderPageTask(executor);

        Future<String> submit = executor.submit(task);
        submit.get();

    }
}
