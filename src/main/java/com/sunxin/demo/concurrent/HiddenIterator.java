package com.sunxin.demo.concurrent;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author sunxin
 */
public class HiddenIterator {
    private final static Set<Integer> set = new HashSet<>();
    private static Random r = new Random();

    public static synchronized void add(Integer i){
        set.add(i);
    }

    public synchronized void remove(Integer i){
        set.remove(i);
    }

    public static void addTenThings(){
        for (int i = 0; i < 10; i++) {
            add(r.nextInt());
        }
        System.out.println(set);
    }

    public static void main(String[] args) throws InterruptedException {
        HiddenIterator call = new HiddenIterator();
        new Thread(() -> {
            for (;;) {
                add(r.nextInt());
            }
        }).start();
        Thread.sleep(1000);
        //这里tostring方法相当于遍历，会出现并发修改异常。equals和hashCode也是如此
        System.out.println(set);


    }


}
