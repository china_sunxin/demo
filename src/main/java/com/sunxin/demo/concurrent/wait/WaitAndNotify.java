package com.sunxin.demo.concurrent.wait;

import lombok.SneakyThrows;

public class WaitAndNotify {

    private static final Object object = new Object();
    private static volatile boolean flag = true;

    public static void main(String[] args) throws InterruptedException {
        new WaitThread().start();
        Thread.sleep(1000);
        new NotifyThread().start();
    }

    static class WaitThread extends Thread {

        @SneakyThrows
        @Override
        public void run() {
            synchronized (object){
                while (flag){
                    object.wait();
                }
                System.out.println("aaaaaa");
            }
        }
    }
    static class NotifyThread extends Thread{

        @Override
        public void run(){
            synchronized (object){
                flag = false;
                object.notify();
                System.out.println("bbbbb");

            }
        }
    }

}

