package com.sunxin.demo.concurrent;

/**
 * @author sunxin02
 */
public class Main1 {

    public static void main(String[] args) {

        char [] char1 = {'a','b','c','d','e'};
        char [] char2 = {'1','2','3','4','5'};

        Object object = new Object();

        Thread thread1 = new Thread(() -> {
            for (char c : char1) {
                synchronized (object){
                    System.out.println(c);
                    try {
                        object.notify();
                        object.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        Thread thread2 = new Thread(() -> {
            for (char c : char2) {
                synchronized (object){
                    System.out.println(c);
                    try {
                        object.notify();
                        object.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread1.start();
        thread2.start();

    }
}
