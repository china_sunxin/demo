package com.sunxin.demo.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * @author sunxin
 */
public class RenderPageTask implements Callable<String> {

    ExecutorService executor;
    public RenderPageTask(ExecutorService executor) {
        this.executor = executor;
    }
    @Override
    public String call() throws Exception {
        Future<String> header,footer;

        header = executor.submit(() ->{
            fab(45);
            System.out.println("11111111111");
            return "hello ";
        });
        footer = executor.submit(() ->{
            fab(46);
            System.out.println("2222222222222");
            return "world";
        });
        return header.get() + footer.get();
    }

    private static int fab(int i){
        if(i <= 2){
            return 1;
        }
        return fab(i -1 ) + fab(i - 2);
    }
}
