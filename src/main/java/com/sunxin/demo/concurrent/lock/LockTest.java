package com.sunxin.demo.concurrent.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author sunxin
 */
public class LockTest {

    static int num = 0;
    static SunLock lock = new SunLock();
//    static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        SunLock lock = new SunLock();
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                increase();
            }).start();
        }
        Thread.sleep(1000);
        System.out.println(num);

    }

    private static void increase(){
        lock.lock();
        num++;
        lock.unlock();
    }


}
