package com.sunxin.demo.concurrent.lock;

public class Producer implements Runnable{
    private DataBuffer buffer;

    public Producer(DataBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        buffer.put();
    }
}
