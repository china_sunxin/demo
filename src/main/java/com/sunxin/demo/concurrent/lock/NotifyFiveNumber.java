package com.sunxin.demo.concurrent.lock;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author q
 */
public class NotifyFiveNumber {

    private static final LinkedList<String> list = new LinkedList<>();

    private static final int max = 10;

    private int count = 0;

    private static Lock lock = new ReentrantLock();
    private static Condition consumer = lock.newCondition();
    private static Condition producer = lock.newCondition();

    public static void main(String[] args) throws InterruptedException {

        NotifyFiveNumber notifyFiveNumber = new NotifyFiveNumber();
        notifyFiveNumber.put();

    }

    public void get(){

    }

    public void put1(String s){
        try {
            lock.lock();
            while (list.size() == max){
                producer.await();
            }
            list.add(s);
            consumer.notifyAll();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void put() {
        try {
            lock.lock();
            while (list.size() == max) {
                producer.await();
            }
            list.add("a");
            consumer.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }


}
