package com.sunxin.demo.concurrent.lock;

/**
 * @author sunxin02
 */
public class LoggingWidget {

    public static void main(String[] args) {
        LoggingWidget loggingWidget1 = new LoggingWidget();

        new Thread(()->{ loggingWidget1.m1();}).start();
        new Thread(()->{ loggingWidget1.m1();}).start();

    }


    private static synchronized void m1() {
        System.out.println("m1");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("m1 end!");
    }

    private synchronized void m2() {
        System.out.println("m2");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("m2 end!");
    }
}
