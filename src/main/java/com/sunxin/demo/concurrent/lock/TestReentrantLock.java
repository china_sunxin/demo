package com.sunxin.demo.concurrent.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author sunxin
 * @date 2021/3/11 23:04
 **/
public class TestReentrantLock {

    private static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            try {
                lock.lock();
                System.out.println("thread1 get lock");
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        },"t1").start();

        Thread.sleep(10000);
        new Thread(() -> {
            try {
                lock.lock();
                System.out.println("thread2 get lock");
            } finally {
                lock.unlock();
            }


        },"t2").start();

        Thread.sleep(1000000000);
    }


}
