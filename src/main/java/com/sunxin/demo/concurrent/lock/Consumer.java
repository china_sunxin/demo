package com.sunxin.demo.concurrent.lock;

public class Consumer implements Runnable{
    private DataBuffer buffer;

    public Consumer(DataBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        buffer.get();
    }
}
