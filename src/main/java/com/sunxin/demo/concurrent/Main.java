package com.sunxin.demo.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] args) {
        Future<?> submit = Executors.newScheduledThreadPool(1).submit(() -> {
            return 1/0;
        });

        try {
            Object o = submit.get();
        } catch (InterruptedException e) {
//            e.printStackTrace();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof  ArithmeticException){
                System.out.println("ArithmeticException");
            }
//            e.printStackTrace();
        }
    }
}
