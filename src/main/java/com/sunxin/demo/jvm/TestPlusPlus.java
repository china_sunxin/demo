package com.sunxin.demo.jvm;

/**
 * @author q
 */
public class TestPlusPlus {

    public static void main(String[] args) {
        TestPlusPlus testPlusPlus = new TestPlusPlus();
        int i = testPlusPlus.m(3);
    }

    public int m(int n){
        if(n == 1){
            return 1;
        }else {
            return n * m(n - 1);
        }
    }
}
