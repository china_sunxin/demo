package com.sunxin.demo.jvm;

public class VolatileTest {

    private static int x ,y = 0;
    private static int a ,b = 0;
    public static void main(String[] args) throws InterruptedException {
        int i = 0;
        for (;;){
            i ++;
            x = 0;y = 0;
            a = 0;b = 0;
            Thread one = new Thread(() -> {
               a = 1; x = b;
            });
            Thread other = new Thread(() -> {
                b = 1; y = a;
            });

            one.start(); other.start();
            one.join(); other.join();
            String result = "第" + i + "次执行结果：x = " + x + ",y = " + y;
            if(x == 0 && y == 0){
                System.err.println(result);
                break;
            }
        }
    }
}
