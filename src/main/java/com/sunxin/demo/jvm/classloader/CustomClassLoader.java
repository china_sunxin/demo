package com.sunxin.demo.jvm.classloader;

import java.io.*;

/**
 * @author sunxin02
 */
public class CustomClassLoader extends ClassLoader{
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        FileInputStream inputStream = null;
        ByteArrayOutputStream outputStream = null;
        try {
            File file = new File("E:\\software\\workspace\\workspace_idea\\demo\\src\\main\\resources\\ClassToBeLoad.class");
            inputStream = new FileInputStream(file);
            outputStream = new ByteArrayOutputStream();
            int b;
            while ((b=inputStream.read()) != -1){
                outputStream.write(b);
            }
            byte[] bytes = outputStream.toByteArray();
            return defineClass(name,bytes,0,bytes.length);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return super.findClass(name);
    }

    public static void main(String[] args) throws Exception {
        CustomClassLoader loader = new CustomClassLoader();
        Class<?> classToBeLoad = loader.loadClass("com.sunxin.demo.jvm.classloader.ClassToBeLoad");
        ClassToBeLoad o = (ClassToBeLoad)classToBeLoad.newInstance();
        o.print();
    }
}
