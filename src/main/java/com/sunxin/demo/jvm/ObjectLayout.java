package com.sunxin.demo.jvm;

import org.openjdk.jol.info.ClassLayout;

/**
 * @author sunxin
 */
public class ObjectLayout {
    public static void main(String[] args) {
        Object object = new Object();
        System.out.println(ClassLayout.parseInstance(object).toPrintable());
        synchronized (object) {
            System.out.println(ClassLayout.parseInstance(object).toPrintable());
        }
    }

    private void add(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("hello").append("world");
    }

    private String lockCoarsening(String str){
        int i = 0;
        StringBuffer buffer = new StringBuffer();
        while (i <100) {
            buffer.append("hello ");
        }
        return str;
    }
}
