package com.sunxin.demo.zhaoyun.day02;


/**
 * @author sunxin02
 */
public class SunArray {
    private int size;//data 的长度
    private int[] data;
    private int index;//当前的已存储的数据的大小

    public SunArray(int size) {
        data = new int[size];
        this.size = size;
        index = 0;
    }

    public void insert(int loc,int n){
        if(index ++ < size){
            for(int i = size - 1; i > loc; i-- ){
                data[i] = data[i -1];
            }
            data[loc] = n;
        }else{
           //扩容
        }
    }

    public void delete(int loc){
        for(int i = loc; i < size; i++ ){
            if(i != size -1){
                data[i] = data[i+1];
            }else{
                data[i] = 0;
            }
        }
        index--;
    }

    public void update(int loc,int n){
        if(loc >= 0 && loc <= size -1){
            data[loc] = n;
        }
    }
}
