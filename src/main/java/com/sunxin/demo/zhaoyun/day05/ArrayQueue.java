package com.sunxin.demo.zhaoyun.day05;


/**
 * @author q
 * 数组前面的空间浪费：
 * 1、每次pop都移动数组，时间复杂度为O(n)
 * 2、在push的时候，发现空间不够时进行清理。大部分时候时间复杂度都是O(1)，只有满了的时候时间复杂度才是O(n)。
 */
public class ArrayQueue<T> {

    private Object data[];
    private int head;
    private int tail;
    private int index;

    public ArrayQueue(int capacity) {
        data = new Object[capacity];
        this.head = 0;
        this.tail = 0;
        this.index = capacity;
    }

    public void push(Object t){
        if(tail == index ){
            throw new RuntimeException("队列已满");
        }
        data[tail] = t;
        tail ++;
    }

    public T pop(){
        if(head == tail){
            throw new RuntimeException("队列已空");
        }
        head++;
        return (T) data[head - 1];
    }

    public boolean isEmpty(){
        return head == tail;
    }
}
