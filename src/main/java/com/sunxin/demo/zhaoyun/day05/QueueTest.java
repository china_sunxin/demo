package com.sunxin.demo.zhaoyun.day05;


/**
 * @author q
 */
public class QueueTest {

    public static void main(String[] args) {
        ArrayQueue<Integer> queue = new ArrayQueue<Integer>(10);
        queue.push(1);
        queue.push(2);
        queue.push(3);
        queue.push(4);
        queue.push(5);
        queue.push(6);

        Integer num;
        while (true) {
            System.out.println(queue.pop());
        }
    }
}
