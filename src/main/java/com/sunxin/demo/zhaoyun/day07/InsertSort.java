package com.sunxin.demo.zhaoyun.day07;

import java.util.Arrays;

/**
 * 插入排序
 * 1、将数组分成已排序和未排序段，初始化时已排序段只有一个元素
 * 2、到未排序段中去元素插入到已排序段，并保证插入后仍然有序
 * 3、重复执行上述操作，知道未排序段元素全部加载完
 * 4、插入排序是稳定排序
 * 插入排序是稳定的吗？稳定
 * 希尔排序呢？不稳定的
 * 归并排序呢？稳定
 * @author sunxin
 */
public class InsertSort {
    public static void main(String[] args) {
        int a[] = {9,8,7,6,5,4,3,2,1};

        int n = a.length;
        //i从1开始，第一个元素不需要排序，0 ~ i的元素认为是已经排好序的。
        for(int i = 1; i < n; i ++){
            //用data与数组中0-j中排好序的数字倒着对比，
            //倒着比，这样数组移动比较简单
            //如果data比排好序的数组中的元素小，然拍好序的数组的最后一个后移，以此类推
            //如果data比排好序的数组中的元素大，跳出循环，因为下一次比较肯定还是比它大，直接赋值，a[j + 1] = data
            int data = a[i];
            int j = i - 1;
            for(; j >= 0; j--){
                //这里> 为正序， < 为倒序
                if(a[j] > data){
                    a[j +1] = a[j];
                }else{
                    break;
                }
            }
            a[j + 1] = data;
        }
        System.out.println(Arrays.toString(a));
    }

}
