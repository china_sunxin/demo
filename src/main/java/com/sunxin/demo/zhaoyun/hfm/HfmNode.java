package com.sunxin.demo.zhaoyun.hfm;

/**
 * @author sunxin02
 */
public class HfmNode implements Comparable<HfmNode>{
    String chars;
    HfmNode parent;
    HfmNode left;
    HfmNode right;
    int frq;

    @Override
    public int compareTo(HfmNode o) {
        return this.frq - o.frq;
    }
}
