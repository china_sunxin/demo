package com.sunxin.demo.zhaoyun.hfm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * @author sunxin02
 */
public class HfmTree {

    /***根节点*/
    HfmNode root;

    /**树的叶子接节点*/
    List<HfmNode> leafs;

    /**叶子节点的权重*/
    Map<Character,Integer> weights;

    public HfmTree(Map<Character,Integer> weights ){
        this.weights = weights;
        leafs = new ArrayList<HfmNode>();
    }

    public void createTree(){
        Character[] keys = weights.keySet().toArray(new Character[0]);
        PriorityQueue<HfmNode> priorityQueue = new PriorityQueue<>();
        for (Character c : keys) {
            HfmNode hfmNode = new HfmNode();
            hfmNode.chars = c.toString();
            hfmNode.frq = weights.get(c);
            priorityQueue.add(hfmNode);
            leafs.add(hfmNode);
        }

        int len = priorityQueue.size();
        for (int i = 1; i < len; i++) {
            HfmNode n1 = priorityQueue.poll();
            HfmNode n2 = priorityQueue.poll();
            HfmNode newNode = new HfmNode();
            newNode.frq = n1.frq + n2.frq;
            newNode.left = n1;
            newNode.right = n2;
            n1.parent = newNode;
            n2.parent = newNode;
            priorityQueue.add(newNode);
        }
        root = priorityQueue.poll();
        System.out.println("构建完成");
    }

    public static void main(String[] args) {
        HashMap<Character, Integer> map = new HashMap<>();
        map.put('a',3);
        map.put('b',24);
        map.put('c',6);
        map.put('d',20);
        map.put('e',34);
        map.put('f',4);
        map.put('g',12);
        HfmTree hfmTree = new HfmTree(map);
        hfmTree.createTree();
        hfmTree.code();
    }

    private Map<Character,String> code() {

        HashMap<Character, String> map = new HashMap<>();

        leafs.forEach(leaf -> {
            String code = "";
            Character c = leaf.chars.charAt(0);
            HfmNode current = leaf;
            do{
                if (current.parent != null && current == current.parent.left ){
                    code = 0 + code;
                }else{
                    code = 1 + code;
                }
                current = current.parent;
            }while (current.parent != null);
            map.putIfAbsent(c,code);
            System.out.println(c + ":" + code);
        });
        return map;
    }
}
