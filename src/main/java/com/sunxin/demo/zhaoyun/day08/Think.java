package com.sunxin.demo.zhaoyun.day08;

import org.springframework.util.StringUtils;

import java.io.*;
import java.util.Random;

/**
 * @author sunxin02
 * 对200W数据进行排序
 * 每个数值取值范围为0-999，两位小数
 * 大数据量的排序，用归并的时候，切忌每次新开一个临时数组，而是开一个全局的临时数组，否则耗时很高。
 */
public class Think {

    private static String fileName = "E:\\sunxin\\a.txt";

    private static int num = 2000000;

    private static double [] data = new double[num];

    private static int [] data2 = new int[num];

    public static void main(String[] args) throws Exception {

//        generate();
//        sort1();
        sort2();
//        System.out.println(99899/100.0);
    }

    private static void sort2() throws Exception {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(new File(fileName))));
        String s = "";
        int i = 0;
        while((s = reader.readLine()) != null){
            data2[i++] = (int)(Double.parseDouble(s) * 100);
        }

        int max = 100000;

        int []count = new int[max];

        for (int j = 0;j < data.length;j ++){
            count[data2[j]] ++;
        }

        File file = new File(fileName);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File("E:\\sunxin\\b.txt"))));

        for (int j = 0;j < max; j ++){
            if(count[j] >0){
                for (int k = 0; k < count[j] ; k++) {
                    writer.write(  j / 100.0 + "\r\n");
                }
            }
        }
        writer.flush();
        writer.close();

    }

    private static void generate() throws Exception {
        File file = new File(fileName);
        OutputStreamWriter writer = new OutputStreamWriter(
                new FileOutputStream(file));
        Random random = new Random();
        for (int i = 0; i < num; i++) {
            String s = random.nextDouble() * 999 + "";
            writer.write(  s.substring(0,s.indexOf(".") + 3) + "\r\n");
        }
    }

    private static void sort1() throws Exception {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(new File(fileName))));
        String s = "";
        int i = 0;
        while((s = reader.readLine()) != null){
            data[i++] = Double.valueOf(s);
        }
        quickSort(data,0,data.length - 1);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File("E:\\sunxin\\b.txt"))));
        for (int j = 0; j < data.length; j++) {
            writer.write(data[j] + "\r\n");
        }
        writer.flush();
        writer.close();
    }

    private static void quickSort(double [] data,int left,int right){
        double base = data[left];
        int l = left;
        int r = right;
        while (l < r){
            while(l < r && data[r] >= base){
                r--;
            }
            if(l < r){
                double tem = data[l];
                data[l] = data[r];
                data[r] = tem;
                l++;
            }

            while(l < r && data[l] <= base){
                l++;
            }
            if(l < r){
                double tem = data[l];
                data[l] = data[r];
                data[r] = tem;
                r--;
            }


        }
        if(left < l){
            quickSort(data,left,l - 1);
        }
        if(right > r){
            quickSort(data,r + 1,right);
        }

    }




}
