package com.sunxin.demo.zhaoyun.day08;

import sun.security.util.Length;

import java.util.Arrays;

/**
 * @author sunxin02
 * 快速排序：
 * 时间复杂度：nlogn，最坏O(n^2)
 * 空间复杂度：O(n)
 * 稳定性：不稳定
 * 和归并排序的区别：
 * 归并排序：分完才排，先递再归，由上到下，先处理子问题，然后合并
 * 快速排序：边排边分，从上到下排序，没有归的过程。先分区在处理子问题，不用合并
 * 优化：优化基准数
 */
public class QuickSort {

    /**
     * 基准数：一般就是取排序序列的第一个
     * 1、从后往前找比基准数小的进行对换
     * 2、从前往后找比基准数大的进行对换
     * 3、直到基准数左边的都比它小，右边的都比它大。
     * 4、递归执行基准数的左右两边的部分
     *
     * 二分的时候，如果每次基准数取的都是最大的或最小的，二分的次数为n次。
     * 取中间值的话分的次数为logn次
     * 因此基准数的选取很重要
     */

    private static int[] data = {9,8,7,6,5,4,3,2,1};
    public static void main(String[] args) {
        int base = 0;
        quickSort(data,base,data.length - 1);
        System.out.println(Arrays.toString(data));

    }

    private static void quickSort(int[] data,int left,int right) {
        //基准数
        int base = data[left];
        int ll = left;
        int rr = right;

        while (ll < rr){
            //从后往前找比基准数小的数，并交换
            while (ll < rr && data[rr] >= base){
                rr --;
            }
            //防止找不到情况下进行交换
            if(ll < rr){
                int tem = data[rr];
                data[rr] = data[ll];
                data[ll] = tem;
                ll++;
            }

            //从前往后找比基准数大的数，并交换
            while (ll < rr && data[ll] <= base){
                ll ++;
            }
            if(ll < rr){
                int tem = data[rr];
                data[rr] = data[ll];
                data[ll] = tem;
                rr--;
            }
        }
        if(left < ll){
            quickSort(data,left,ll - 1);
        }
        if(right > rr){
            quickSort(data,rr + 1,right );
        }
    }
}
