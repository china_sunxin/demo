package com.sunxin.demo.zhaoyun.graphtheory;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author sunxin02
 */
public class Bfs {

    /**标记走过的位置*/
    boolean [][] mark;

    int m;
    int n;

    /**邻接矩阵*/
    boolean [][] data = new boolean [m][n];

    int x;

    int y;

    int dx;

    int dy;

    private ArrayBlockingQueue<Point> queue = new ArrayBlockingQueue<>(m * n);

    public void find(){
        Point point = new Point(x, y);
        mark[x][y] = true;
        queue.add(point);
        while (!queue.isEmpty()){
            Point poll = queue.poll();
        }
    }

}

class Point{
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int x;
    int y;
}
