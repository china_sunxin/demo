package com.sunxin.demo.zhaoyun.day04;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ArrayStack<T> implements SunStack<T> {

    private T[] data = (T[])new Object[1];

    private int index;

    public ArrayStack(int capacity) {
        data = (T[])new Object[capacity];
    }

    @Override
    public void push(T item) {
        if(index >= data.length){
            resize(data.length *2);
        }
        if(data == null){
            data = (T[])new Object[16];
        }
        data[index++] = item;
    }


    private void resize(int size){
        T [] tem = (T[])new Object[size];
        for (int i = 0; i < index; i++) {
            tem[i] = data[i];
        }
        data = tem;
    }

    @Override
    public T pop() {
        if(isEmpty()){
            return null;
        }
        if(index >= 16 && data.length >= index * 2){
            resize(index);
        }
        T t = data[--index];
        data[index] = null;
        return t;
    }

    @Override
    public int size() {
        return index;
    }

    @Override
    public boolean isEmpty() {
        return index == 0 ;
    }
}
