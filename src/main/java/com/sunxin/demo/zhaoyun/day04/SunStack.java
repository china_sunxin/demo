package com.sunxin.demo.zhaoyun.day04;

public interface SunStack<T> {

    void push(T item);

    T pop();

    int size();

    boolean isEmpty();
}
