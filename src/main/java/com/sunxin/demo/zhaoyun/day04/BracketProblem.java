package com.sunxin.demo.zhaoyun.day04;

/**
 * @author q
 */
public class BracketProblem {

    public static void main(String[] args) {
        boolean a = isOk("<{}{}()>{}()[]{{}[]<{}[]{}{{}{}{{<<}>>}}>}");
        System.out.println(a);
    }

    private static boolean isOk(String s) {
        if (s == null){
            return false;
        }

        ArrayStack<Character> stack = new ArrayStack<>(16);
        char[] chars = s.toCharArray();
        for (Character c : chars){
            switch (c) {
                case '{':
                case '<':
                case '(':
                case '[':
                    stack.push(c);
                    break;
                case '}':
                    if(stack.pop() != '{'){
                        return false;
                    }
                    break;
                case '>':
                    if(stack.pop() != '<'){
                        return false;
                    }
                    break;
                case ')':
                    if(stack.pop() != '('){
                        return false;
                    }
                    break;
                case ']':
                    if(stack.pop() != '['){
                        return false;
                    }
                    break;
                default:
            }
        }
        if (stack.isEmpty()){
            return true;
        }
        return false;
    }

}
