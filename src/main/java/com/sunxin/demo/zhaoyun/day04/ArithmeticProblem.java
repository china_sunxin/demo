package com.sunxin.demo.zhaoyun.day04;

import java.util.Calendar;

/**
 * @author q
 */
public class ArithmeticProblem {


    public static void main(String[] args) {
        int [] arr = {1,'+',2,'*',3,'-',4,'/',2,'+',3};
        ArrayStack<Integer> numberStack = new ArrayStack<>(16);
        ArrayStack<Integer> symbolStack = new ArrayStack<>(16);
        for(int c : arr){
            if(c == '*' || c =='/' || c == '+' || c == '-' ){
                if(symbolStack.isEmpty()){
                    symbolStack.push(c);
                    continue;
                }
                Integer symbol;
                while ((symbol = symbolStack.pop()) != null && !judge(c, symbol)) {
                    Integer number1 = numberStack.pop();
                    Integer number2 = numberStack.pop();
                    int num = 0;
                    if (symbol == '+') {
                        num = number1 + number2;
                    } else if (symbol == '-') {
                        num = number2 - number1;
                    } else if (symbol == '*') {
                        num = number1 * number2;
                    } else if (symbol == '/') {
                        num = number2 / number1;
                    }
                    numberStack.push(num);
                }
                if(symbol != null){
                    symbolStack.push(symbol);
                }
                symbolStack.push(c);

            }else{
                numberStack.push(c);
            }
        }

        System.out.println(numberStack.pop() + "-" + symbolStack.pop() + "-" + numberStack.pop() + "-" + symbolStack.pop() + "-" + numberStack.pop());

//        System.out.println(pop == '+' ? "+": pop == '-' ? "-" : pop == '*' ? "*" : pop == '/' ? "/" : "other");
    }


    public static boolean judge(Integer a, Integer b){
        if(isTake(a) && isPlus(b)){
            return true;
        }
        return false;
    }

    public static boolean isPlus(Integer character){
        if (character == '+' || character == '-'){
            return true;
        }
        return false;
    }

    public static boolean isTake(Integer character){
        if (character == '*' || character == '/'){
            return true;
        }
        return false;
    }




}