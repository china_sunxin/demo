package com.sunxin.demo.zhaoyun.day01;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Random;

public class Day1 {
    public static void main(String[] args) throws Exception {
        String str = null;
        String fileName = "E:\\software\\workspace\\workspace_idea\\demo\\src\\main\\doc\\age1.txt";
        InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName),"UTF-8");

        long start = System.currentTimeMillis();
        BufferedReader br = new BufferedReader(isr);
        int tot = 0 ;	//21亿
        int data [] = new int[200];
        while((str = br.readLine()) != null){		//一行一行的读 O(n)
            int age = Integer.valueOf(str);
            data[age] ++ ;
            tot ++ ;
        }
        //O(n) 14亿. 100万/秒 *1000 = 10亿 100~1000s之间 => 500s以下 60*8=480s
        System.out.println("总共的数据大小: " + tot);

        for(int i = 0 ; i < 200 ; i ++){//下标从0开始的
            System.out.println(i + ":" + data[i]);
        }
        //144239ms => 144s
        System.out.println("计算花费的时间为:" + (System.currentTimeMillis() - start) + "ms");
    }


    private void generateData() throws Exception{
        final String fileName = "E:\\software\\workspace\\workspace_idea\\demo\\src\\main\\doc\\age1.txt";
        final Random random = new Random();
        BufferedWriter objWriter = null;
        objWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
        for (int i = 0; i < 1400000000; i++) {
            int age = Math.abs(random.nextInt()) % 180;
            objWriter.write(age + "\r\n");
        }
        objWriter.flush();
        objWriter.close();
    }
}
