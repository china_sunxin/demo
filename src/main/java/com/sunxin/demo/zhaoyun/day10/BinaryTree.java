package com.sunxin.demo.zhaoyun.day10;

import com.sunxin.demo.zhaoyun.day05.ArrayQueue;
import lombok.Data;

/**
 * @author sunxin02
 * 遍历的时间复杂度为O(2n) -> O(n)
 * 二叉树
 */
public class BinaryTree {

    /**
     * 前序遍历:根左右
     * @param root
     */
    private void pre(TreeNode root){
        print(root);
        if(root.getLeft() != null){
            pre(root.getLeft());
        }
        if(root.getRight() != null){
            pre(root.getRight());
        }
    }


    /**
     * 中序遍历:左根右
     * @param root
     */
    private void mid(TreeNode root){
        if(root.getLeft() != null){
            mid(root.getLeft());
        }
        print(root);
        if(root.getRight() != null){
            mid(root.getRight());
        }
    }


    /**
     * 后序遍历:左右根
     * @param root
     */
    private void post(TreeNode root){
        if(root.getLeft() != null){
            post(root.getLeft());
        }
        if(root.getRight() != null){
            post(root.getRight());
        }
        print(root);
    }


    ArrayQueue<TreeNode> queue = new ArrayQueue<>(50);
    /**
     * 层次遍历
     * @param node
     */
    private void level(TreeNode node){
        queue.push(node);

        while (!queue.isEmpty()){
            node = queue.pop();
            print(node);
            if(node.getLeft() != null){
                queue.push(node.getLeft());
            }
            if(node.getRight() != null){
                queue.push(node.getRight());
            }
        }
    }

    public static void main(String[] args) {
        BinaryTree c = new BinaryTree();
        TreeNode node = c.generateTree();
        c.level(node);
    }

    private TreeNode generateTree(){
        TreeNode A = new TreeNode('A');
        TreeNode B = new TreeNode('B');
        TreeNode C = new TreeNode('C');
        TreeNode D = new TreeNode('D');
        TreeNode E = new TreeNode('E');
        TreeNode F = new TreeNode('F');
        TreeNode G = new TreeNode('G');
        TreeNode H = new TreeNode('H');
        TreeNode K = new TreeNode('K');

        C.setLeft(D);
        B.setRight(C);
        A.setLeft(B);
        G.setLeft(H);
        G.setRight(K);
        F.setLeft(G);
        E.setRight(F);
        A.setRight(E);
        return A;
    }

    /**
     * 打印
     * @param node
     */
    private void print(TreeNode node){
        System.out.println(node.getData());
    }

}


@Data
class TreeNode{
    private TreeNode left;
    private TreeNode right;
    private char data;

    public TreeNode(char data) {
        this.data = data;
    }
}
