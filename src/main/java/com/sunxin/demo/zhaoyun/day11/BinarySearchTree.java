package com.sunxin.demo.zhaoyun.day11;

import lombok.Data;

import java.util.List;

/**
 * @author sunxin02
 * 二叉搜索树
 */
@Data
public class BinarySearchTree {

    int data;
    private BinarySearchTree left;
    private BinarySearchTree right;


    public BinarySearchTree(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    private void insert(BinarySearchTree root,int data){
        if (root.data >= data){
            if(root.left == null){
                root.left = new BinarySearchTree(data);
            }else {
                insert(root.left, data);
            }
        }else{
            if(root.right == null){
                root.right = new BinarySearchTree(data);
            }else {
                insert(root.right, data);
            }
        }
    }

    private void find(BinarySearchTree root,int data){
        if(root != null){
            if(root.data == data){
                System.out.println(root.data);
                return;
            }
            if(root.data > data){
                find(root.left,data);
            }
            if(root.data < data){
                find(root.right,data);
            }
        }

    }


    public List<List<Integer>> BSTSequences(TreeNode root) {


        return null;
    }
}

    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }
