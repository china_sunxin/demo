package com.sunxin.demo.zhaoyun.day06;

/**
 * @author q
 */
public class Fibonacci {
    public static void main(String[] args) {
        for (int i = 0; i < 45; i++) {
            long start = System.currentTimeMillis();
            System.out.println(i + ":" + fab(i) + "花费时间为：" + (System.currentTimeMillis() - start));
            //701408733
            //701408733
        }
    }


    /**
     * 斐波那契-递归
     * 时间复杂度 2^n
     * 空间复杂度 2^n
     * @param i
     * @return
     */
    private static int fab(int i){
        if(i <= 2){
            return 1;
        }
        return fab(i -1 ) + fab(i - 2);
    }


    /**
     * 非递归方式
     * @param n
     * @return
     */
    private static int fab2(int n){
        if (n <= 2){
            return 1;
        }
        int a = 1;
        int b = 1;
        int c = 0;
        for (int i = 3; i <= n; i++){
            c = a + b;
            a = b;
            b = c;
        }
        return  c;
    }


    /**
     * 斐波那契-数组缓存方式
     * 初始化一个数组,int类型的初始值都是0
     * 在归的时候将前一个值付给数组
     * 时间复杂度和空间复杂度都为O(n)
     * @param n
     * @return
     */
    private static int[] data = new int[100];
    private static int fabByArray(int n){
        if(n <= 2){
            return 1;
        }
        if (data[n] > 0){
            return data[n];
        }
        data[n] = fabByArray(n - 1) + fabByArray(n - 2);
        return data[n];
    }


    /**
     * 尾递归：调用函数一定出现在末尾，就没有任何其他操作了。
     * 编译器在编译代码时，如果发现函数末尾已经没有其他操作了就不会创建新的栈，而是覆盖到前面去。
     * fab方法在末尾进行了加运算，会把值放到栈里面去。
     * 1,1,2,4,6,10
     * res 上次运算的结果
     * pre 上上次运算的结果
     * 时间复杂度O(n)
     * @return
     */
    private static int tailFab(int res,int pre, int n){
        if(n <= 2){
            return res;
        }
        return tailFab(pre + res,res,n - 1);
    }



}
