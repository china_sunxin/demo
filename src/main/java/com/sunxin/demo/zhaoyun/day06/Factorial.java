package com.sunxin.demo.zhaoyun.day06;

/**
 * @author q
 */
public class Factorial {

    public static void main(String[] args) {
//        long factorial = factorial(10);
//        System.out.println(factorial);

//        int i = tailFactorial(5, 1);
//        System.out.println(i);
    }


    /**
     * 计算n的阶乘-递归
     * 时间复杂度：
     * 空间复杂度：
     * @param n
     * @return
     */
    private static long factorial(long n){
        if(n == 1){
            return 1;
        }
        return n * factorial(n -1);
    }


    /**
     * 计算n的阶乘-尾递归
     * 时间复杂度：
     * 空间复杂度：
     * @param n
     * @return
     */
    private static int tailFactorial(int n,int res ){
        System.out.println(n + ":" + res);
        if(n <= 1){
            return res;
        }
        return tailFactorial(n - 1,res * n);
    }
}
