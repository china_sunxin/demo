package com.sunxin.demo.zhaoyun.heap;

import java.util.Arrays;

public class HeapSort {

    public static void main(String[] args) {
        int []data = {8,4,20,7,3,1,25,14,17};

        //首先将数组堆化
        generateHeap(data);

        System.out.println(Arrays.toString(data));

    }

    private static void generateHeap(int[] data) {
        int len = data.length;
        for (int i = len / 2 -1; i >= 0; i--) {
            maxHeap(data,i,len);
        }
        for (int i = len - 1; i > 0; i--) {
            int tem = data[0];
            data[0] = data[i];
            data[i] = tem;
            maxHeap(data,0,i);
        }
    }

    private static void maxHeap(int[] data,int start,int end) {
        int parent = start;
        int son = parent * 2 + 1;
        while (son < end){
            int tem = son;
            if(son + 1 < end && data[son] < data[son + 1] ){
                tem = son + 1;
            }
            if(data[tem] < data[parent]){
                return;
            }else {
                int t = data[parent];
                data[parent] = data[tem];
                data[tem] = t;
                parent = tem;
                son = parent * 2 + 1;
            }
        }
    }
}
