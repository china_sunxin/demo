package com.sunxin.demo.zhaoyun.day03;

/**
 * @author sunxin02
 */
public class DoubleLinkedList {

    private DNode head;
    private DNode tail;


    public DoubleLinkedList() {
        this.head = null;
        this.tail = null;
    }

    public void insertHead(int data){
        DNode newNode = new DNode(data);
        if(head == null){
            tail = newNode;
        }else{
            newNode.next = this.head;
            head.pre = newNode;
        }
        head = newNode;
    }

    public void deleteHead(){
        if(this.head == null){

        }else{
            this.head = this.head.next;
            this.head.pre = null;
        }
    }

    public void insertN(int data,int position){
        DNode newNode = new DNode(data);
        DNode curr = head;
        for(int i = 1;i < position;i ++){
            curr = curr.next;
        }

        newNode.next = curr.next;
        newNode.pre = curr;
        curr.next = newNode;
        newNode.next.pre = newNode;

    }




}

class DNode{
    int value;

    DNode next;
    DNode pre;

    DNode(int value){
        this.value = value;
        this.next = null;
        this.pre = null;
    }
}
