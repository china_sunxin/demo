package com.sunxin.demo.zhaoyun.day03;

public class SunLinkedList {
    private ListNode head;
    private int size;
    public void insertHead(int data){
        ListNode newHead = new ListNode(data);
        newHead.next = head;
        this.head = newHead;
        size ++;
    }


    public void insertNth(int data,int position){
        if(position == 0){
            insertHead(data);
        }else{
            ListNode cur = head;
            for (int i = 1;i < position;i++){
                cur = head.next;
            }
            ListNode newNode = new ListNode(data);
            newNode.next = cur.next;
            cur.next = newNode;
        }

    }

    public void deleteHead(){
        this.head = this.head.next;
    }

    public void deleteNth(int position){
        if(position == 0){
            deleteHead();
        }else{
            ListNode cur = head;
            for (int i = 1; i < position; i++) {
                cur = cur.next;
            }
            cur.next = cur.next.next;
        }
    }

    public void print(){

    }



}

class ListNode{
    int value;
    ListNode next;
    ListNode(int value){
        this.value = value;
        this.next = null;
    }
}
