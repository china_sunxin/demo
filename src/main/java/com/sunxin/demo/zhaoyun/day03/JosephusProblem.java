package com.sunxin.demo.zhaoyun.day03;


/**
 * @author q
 * 约瑟夫问题
 */
public class JosephusProblem {
    public static void main(String[] args) {
        int m = 5;
        int  n =4;
        Node head = new Node(0);
        Node curr = head;
        for (int i = 1; i < m; i++) {
            Node node = new Node(i);
            curr.next = node;
            curr = node;
        }
        curr.next = head;

        while (head.next != head){
            for (int i = 1; i < n - 1; i++) {
                head = head.next;
            }
            head.next = head.next.next;
            head = head.next;
        }
        System.out.println(head.value);

    }
}

class Node{
    int value;
    Node next;

    public Node() {
    }

    public Node(int value) {
        this.next = null;
        this.value = value;
    }
}
