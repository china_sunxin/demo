package com.sunxin.demo.datastructure.advanced01.advanced03;

/**
 * @author sunxin
 * 求str字符串中能匹配到match的第一个字符的下标
 */
public class KMP {

    public static void main(String[] args) {
        String str = "sunxinhelloworld";
        String match = "hello";
        int value = getValue(str, match);
        System.out.println(value);

    }

    private static int getValue(String str, String match){
        int[] next = getArray(match.toCharArray());
        char[] s = str.toCharArray();
        char[] m = match.toCharArray();

        int sIndex = 0;
        int mIndex = 0;
        while (sIndex < s.length && mIndex < m.length){
            if (s[sIndex] == m[mIndex]){
                sIndex++;
                mIndex++;
            }else if(next[mIndex] != -1){
                mIndex = next[mIndex];
            }else {
                sIndex++;
            }
        }
        return mIndex == m.length ? sIndex - mIndex :- 1;
    }

    /**
     * 数组的值代表str字符串中该下标以前有多少个最长的前后相同的子字符串
     * abcdabclm  arr[9] = 3 因为abc=abc
     * @param match
     * @return
     */
    private static int[] getArray(char[] match) {
        if (match.length == 0){
            return new int[]{-1};
        }
        int[] next = new int[match.length];
        next[0] = -1;
        next[1] = 0;
        int i = 2;
        int index = 0;
        while (i < match.length){
            if (match[i - 1] == match[index]){
                next[i++] = ++index;
            }else if(index > 0){
                index = next[index];
            }else {
                next[i++] = 0;
            }
        }
        return next;
    }
}
