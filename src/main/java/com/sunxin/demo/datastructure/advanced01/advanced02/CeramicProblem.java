package com.sunxin.demo.datastructure.advanced01.advanced02;

/**
 * @author sunxin
 * 在长为N，宽为2的区域上铺设瓷砖，瓷砖的大小是 1x2 ，问将区域填满有多少种方案？
 * 定义函数f(n)，如果区域上还有n列没有填满，f(n)代表有多少种方案。
 * 枚举左上角的瓷砖的摆法：
 * 如果是竖着摆，后续方案为f（n - 1）种
 * 如果是横着摆，下面只有一种方案也是横着摆，后续方案为f（n - 2）种
 * 得f(n) = f(n - 1) + f(n - 2)
 */
public class CeramicProblem {



}
