package com.sunxin.demo.datastructure.advanced01.advanced01;

import java.util.LinkedList;

/**
 * @author sunxin
 *
 * 给定一个整型数组arr，和一个整数num
 * 某个arr中的子数组sub，如果想达标，必须满足：
 * sub中最大值 – sub中最小值 <= num，
 * 返回arr中达标子数组的数量
 */
public class AllLessNumSubArray {

    public static void main(String[] args) {
        int testNum = 100;
        int maxValue = 100;
        int maxSize = 100;
        System.out.println("test start");
        for (int i = 0; i < testNum; i++) {
            int[] arr = generateRandomArray(maxValue, maxSize);
            int num = (int)(Math.random() * maxValue);
            if (getNum(arr, num) != getNumBao(arr, num)){
                System.out.println("Oops!");
            }
        }
        System.out.println("test finish");
    }

    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * (maxValue + 1));
        }
        return arr;
    }

    private static int getNum(int[] arr, int num) {
        if (arr == null || arr.length == 0){
            return 0;
        }
        LinkedList<Integer> max = new LinkedList<>();
        LinkedList<Integer> min = new LinkedList<>();
        int l = 0;
        int r = 0;
        int res = 0;
        while (l < arr.length){
            while (r < arr.length){
                while (!max.isEmpty() && arr[max.peekLast()] <= arr[r]){
                    max.pollLast();
                }
                max.addLast(r);
                while (!min.isEmpty() && arr[min.peekLast()] >= arr[r]){
                    min.pollLast();
                }
                min.addLast(r);
                if (arr[max.peekFirst()] - arr[min.peekFirst()] > num){
                    break;
                }
                r++;
            }
            res += r - l;
            if (max.peekFirst() == l){
                max.pollFirst();
            }
            if (min.peekFirst() == l){
                min.pollFirst();
            }
            l++;
        }
        return res;
    }

    private static int getNumBao(int[] arr,int num){
        int res = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                int max = arr[i];
                int min = arr[i];
                for (int k = i; k <= j; k++) {
                    max = Math.max(max,arr[k]);
                    min = Math.min(min,arr[k]);
                }
                if (max - min <= num){
                    res++;
                }
            }
        }
        return res;
    }

}
