package com.sunxin.demo.datastructure.advanced01.advanced04;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author sunxin
 * @date 2021/1/16 0:38
 **/
public class FindMinKth {

    private static int process2(int[] arr,int left,int right,int index){
        if (left == right){
            return arr[left];
        }
        int pivot = arr[left + (int)Math.random() * (right - left + 1)];
        int[] range = partition(arr,left,right,pivot);
        if (index >= range[0] && index <= range[1]){
            return arr[index];
        }else if(index > range[1]) {
            return process2(arr,range[1] + 1,right,index);
        }else {
            return process2(arr,left,range[0] - 1,index);
        }
    }

    private static int[] partition(int[] arr, int left, int right, int pivot) {
        int lIndex = left - 1;
        int rIndex = right + 1;
        int curIndex = left;
        while (curIndex < rIndex){
            if (arr[curIndex] > pivot){
                swap(arr,curIndex,--rIndex);
            }else if(arr[curIndex] < pivot){
                swap(arr,++lIndex,curIndex++);
            }else {
                curIndex++;
            }
        }
        return new int[]{lIndex + 1,rIndex - 1};
    }

    private static void swap(int[] arr, int m, int n) {
        int tem = arr[m];
        arr[m] = arr[n];
        arr[n] = tem;
    }


    /**
     * 找到arr数组上[left,right]区间里面第index小的数
     * @param arr 数组
     * @param left 左边界
     * @param right 右边界
     * @param index index
     * @return 结果
     */
    private static int bfprt(int[] arr,int left,int right,int index){
        if (left == right){
            return arr[left];
        }
        //先找一个随机数
        int pivot = medianOfMedians(arr, left, right);
        int[] range = partition(arr, left, right, pivot);
        if (index >= range[0] && index <= range[1]) {
            return arr[index];
        } else if (index < range[0]) {
            return bfprt(arr, left, range[0] - 1, index);
        } else {
            return bfprt(arr, range[1] + 1, right, index);
        }
    }


    private static int medianOfMedians(int[] arr, int left, int right) {
        int size = right - left + 1;
        int offset = size % 5 == 0 ? 0 : 1;
        int[] res = new int[size / 5 + offset];
        for (int i = 0; i < res.length; i++) {
            int start = left + 5 * i;
            res[i] = getMedian(arr,start,Math.min(start + 4,right));
        }
        //找到新数组中 中间位置的数
        return bfprt(res,0,res.length - 1,res.length / 2);
    }

    public static int getMedian(int[] arr, int L, int R) {
        insertionSort(arr, L, R);
        return arr[(L + R) / 2];
    }

    public static void insertionSort(int[] arr, int L, int R) {
        for (int i = L + 1; i <= R; i++) {
            for (int j = i - 1; j >= L && arr[j] > arr[j + 1]; j--) {
                swap(arr, j, j + 1);
            }
        }
    }
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) (Math.random() * maxSize) + 1];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * (maxValue + 1));
        }
        return arr;
    }

    public static class MaxHeapComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return o2 - o1;
        }

    }

    // 利用大根堆，时间复杂度O(N*logK)
    public static int minKth1(int[] arr, int k) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(new MaxHeapComparator());
        for (int i = 0; i < k; i++) {
            maxHeap.add(arr[i]);
        }
        for (int i = k; i < arr.length; i++) {
            if (arr[i] < maxHeap.peek()) {
                maxHeap.poll();
                maxHeap.add(arr[i]);
            }
        }
        return maxHeap.peek();
    }

    // 改写快排，时间复杂度O(N)
    public static int minKth2(int[] array, int k) {
        int[] arr = copyArray(array);
        return process2(arr, 0, arr.length - 1, k - 1);
    }

    public static int[] copyArray(int[] arr) {
        int[] ans = new int[arr.length];
        for (int i = 0; i != ans.length; i++) {
            ans[i] = arr[i];
        }
        return ans;
    }

    // 利用bfprt算法，时间复杂度O(N)
    public static int minKth3(int[] array, int k) {
        int[] arr = copyArray(array);
        return bfprt(arr, 0, arr.length - 1, k - 1);
    }

    public static void main(String[] args) {
        int testTime = 1000000;
        int maxSize = 100;
        int maxValue = 100;
        System.out.println("test begin");
        for (int i = 0; i < testTime; i++) {
            int[] arr = generateRandomArray(maxSize, maxValue);
            int k = (int) (Math.random() * arr.length) + 1;
            int ans1 = minKth1(arr, k);
            int ans2 = minKth2(arr, k);
            int ans3 = minKth3(arr, k);
            if (ans1 != ans2 || ans2 != ans3) {
                System.out.println("Oops!");
            }
        }
        System.out.println("test finish");
    }

}
