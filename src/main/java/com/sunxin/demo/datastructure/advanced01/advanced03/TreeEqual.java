package com.sunxin.demo.datastructure.advanced01.advanced03;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sunxin
 * @date 2021/1/15 23:03
 **/
public class TreeEqual {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int v) {
            value = v;
        }
    }

    /**
     * 将两棵树都以先序方式序列化，然后使用KMP算法
     * @param big
     * @param small
     * @return
     */
    private static boolean containsTree2(Node big, Node small) {
        if (small == null){
            return true;
        }
        if (big == null){
            return false;
        }
        List<Integer> bigList = new ArrayList<>();
        List<Integer> smallList = new ArrayList<>();
        pres(big,bigList);
        pres(small,smallList);
        int[] next = getNextArray(smallList);

        int bigIndex = 0;
        int smallIndex = 0;

        if (bigList.size() < 1 || bigList.size() < smallList.size()) {
            return false;
        }
        while (bigIndex < bigList.size() && smallIndex < smallList.size()){
            if (isEquel(bigList.get(bigIndex),smallList.get(smallIndex))){
                bigIndex++;
                smallIndex++;
            }else if(smallIndex > 0){
                smallIndex = next[smallIndex];
            }else {
                bigIndex++;
            }
        }
        return smallIndex == smallList.size();
    }


    private static boolean isEquel(Integer node1, Integer node2){
        if (node1 == null && node2 == null){
            return true;
        }
        if (node1 == null){
            return false;
        }
        if (node2 == null){
            return false;
        }
        return node1.equals(node2);
    }

    private static int[] getNextArray(List<Integer> smallList) {
        if (smallList.size() == 1){
            return new int[]{-1};
        }
        int[] next = new int[smallList.size()];
        next[0] = -1;
        next[1] = 0;
        Integer[] match = new Integer[smallList.size()];
        for (int i = 0; i < smallList.size(); i++) {
            match[i] = smallList.get(i);
        }

        int index = 0;
        int i = 2;
        while (i < match.length){
            if (isEquel(match[i - 1] , match[index])){
                next[i++] = ++index;
            }else if(index > 0){
                index = next[index];
            }else {
                next[i++] = 0;
            }
        }
        return next;
    }

    private static void pres(Node head, List<Integer> list) {
        if (head == null){
            list.add(null);
        }else {
            list.add(head.value);
            pres(head.left, list);
            pres(head.right, list);
        }
    }

    /**
     * 暴力解法，时间复杂度为O(m*n)
     * @param big
     * @param small
     * @return
     */
    private static boolean containsTree1(Node big, Node small) {
        if (small == null){
            return true;
        }
        if (big == null){
            return false;
        }
        if (isSameStructure(big,small)){
            return true;
        }
        return containsTree1(big.left,small) || containsTree1(big.right,small);
    }

    private static boolean isSameStructure(Node big, Node small) {

        if (big == null && small != null){
            return false;
        }
        if (big != null && small == null){
            return false;
        }
        if (big == null && small == null){
            return true;
        }
        if (big.value != small.value){
            return false;
        }
        return isSameStructure(big.left,small.left) && isSameStructure(big.right,small.right);
    }

    public static Node generateRandomBST(int maxLevel, int maxValue) {
        return generate(1, maxLevel, maxValue);
    }

    // for test
    public static Node generate(int level, int maxLevel, int maxValue) {
        if (level > maxLevel || Math.random() < 0.5) {
            return null;
        }
        Node head = new Node((int) (Math.random() * maxValue));
        head.left = generate(level + 1, maxLevel, maxValue);
        head.right = generate(level + 1, maxLevel, maxValue);
        return head;
    }

    public static void main(String[] args) {
        int bigTreeLevel = 7;
        int smallTreeLevel = 4;
        int nodeMaxValue = 5;
        int testTimes = 100000;
        System.out.println("test begin");
        for (int i = 0; i < testTimes; i++) {
            Node big = generateRandomBST(bigTreeLevel, nodeMaxValue);
            Node small = generateRandomBST(smallTreeLevel, nodeMaxValue);
            boolean ans1 = containsTree1(big, small);
            boolean ans2 = containsTree2(big, small);
            if (ans1 != ans2) {
                System.out.println("Oops!");
            }
        }
        System.out.println("test finish!");

    }

}
