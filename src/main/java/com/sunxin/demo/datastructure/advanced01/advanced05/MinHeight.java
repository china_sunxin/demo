package com.sunxin.demo.datastructure.advanced01.advanced05;

/**
 * @author sunxin
 * @date 2021/1/17 15:12
 **/
public class MinHeight {
    public static class Node {
        public int val;
        public Node left;
        public Node right;

        public Node(int x) {
            val = x;
        }
    }

    public static int minHeight1(Node head) {
        if (head == null) {
            return 0;
        }
        return p(head);
    }

    private static int p(Node head) {
        if (head.left == null && head.right == null){
            return 1;
        }
        int leftH = Integer.MAX_VALUE;
        if (head.left != null){
            leftH = p(head.left);
        }
        int rightH = Integer.MAX_VALUE;
        if (head.right != null){
            rightH = p(head.right);
        }
        return Math.min(leftH,rightH) + 1;
    }

    /**
     * 求一颗二叉树的最小高度，根据morris遍历改写
     * @param head head
     * @return result
     */
    public static int minHeight2(Node head) {
        if (head == null) {
            return 0;
        }
        Node cur = head;
        Node mostRight;
        int curLevel = 0;
        int minHeight = Integer.MAX_VALUE;
        while (cur != null){
            mostRight = cur.left;
            if (mostRight != null){
                int rightBoardSize = 1;
                while (mostRight.right != null && mostRight.right != cur){
                    rightBoardSize++;
                    mostRight = mostRight.right;
                }
                //第一次来到该节点
                if (mostRight.right == null){
                    curLevel++;
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                }else {
                    //第二次到达
                    //mostRight.left == null说明该节点是叶节点
                    if (mostRight.left == null){
                        minHeight = Math.min(minHeight,curLevel);
                    }
                    curLevel -= rightBoardSize;;
                    mostRight.right = null;
                }
            }else {
                curLevel++;
            }
            cur = cur.right;
        }
        int finalRight = 1;
        cur = head;
        while (cur.right != null) {
            finalRight++;
            cur = cur.right;
        }
        //如果左右的节点是页节点，更新最小值。
        if (cur.left == null) {
            minHeight = Math.min(minHeight, finalRight);
        }
        return minHeight;
    }

    public static Node generateRandomBST(int maxLevel, int maxValue) {
        return generate(1, maxLevel, maxValue);
    }

    // for test
    public static Node generate(int level, int maxLevel, int maxValue) {
        if (level > maxLevel || Math.random() < 0.5) {
            return null;
        }
        Node head = new Node((int) (Math.random() * maxValue));
        head.left = generate(level + 1, maxLevel, maxValue);
        head.right = generate(level + 1, maxLevel, maxValue);
        return head;
    }

    public static void main(String[] args) {
        int treeLevel = 7;
        int nodeMaxValue = 5;
        int testTimes = 100000;
        System.out.println("test begin");
        for (int i = 0; i < testTimes; i++) {
            Node head = generateRandomBST(treeLevel, nodeMaxValue);
            int ans1 = minHeight1(head);
            int ans2 = minHeight2(head);
            if (ans1 != ans2) {
                System.out.println("Oops!");
            }
        }
        System.out.println("test finish!");

    }


}
