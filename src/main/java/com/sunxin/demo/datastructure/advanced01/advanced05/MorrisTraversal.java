package com.sunxin.demo.datastructure.advanced01.advanced05;

/**
 * @author sunxin
 * @date 2021/1/17 15:12
 **/
public class MorrisTraversal {

    public static class Node {
        public int value;
        Node left;
        Node right;

        public Node(int data) {
            this.value = data;
        }
    }
    public static void main(String[] args) {
        Node head = new Node(4);
        head.left = new Node(2);
        head.right = new Node(6);
        head.left.left = new Node(1);
        head.left.right = new Node(3);
        head.right.left = new Node(5);
        head.right.right = new Node(7);
//        printTree(head);
//        morrisIn(head);
//        morrisPre(head);
        printTree(head);
        morrisPos(head);

    }

    public static void morris(Node head) {
        if (head == null){
            return;
        }
        Node cur = head;
        Node mostRight;
        while (cur != null){
            mostRight = cur.left;
            if (mostRight != null){
                while (mostRight.right != null && mostRight.right != cur){
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null){
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                }else {
                    mostRight.right = null;

                }
            }
            cur = cur.right;
        }
    }

    /**
     * 利用Morris序进行中序遍历
     * morris序cur的顺序中，有的节点会走两遍，走两边的节点取第二次的位置，得到的序列就是中序遍历
     *
     * cur无左，cur=cur.right cur只遍历一次
     * cur有左，
     *      第一次,mosrRight.right=cur,cur=cur.left
     *      第二次,mosrRight.right=null,cur=cur.right
     * 所以只要在cur=cur.right的地方打印就行了
     * @param head head
     */
    public static void morrisIn(Node head) {
        if (head == null){
            return;
        }
        Node cur = head;
        Node mostRight;
        while (cur != null){
            mostRight = cur.left;
            if (mostRight != null){
                while (mostRight.right != null && mostRight.right != cur){
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null){
                    //第二次到达的节点
                    System.out.println(cur.value);
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                }else {
                    mostRight.right = null;
                }
            }
            System.out.println(cur.value);
            cur = cur.right;
        }
    }

    /**
     * 利用Morris序进行先序遍历
     * morris序cur的顺序中，有的节点会走两遍，走两遍的节点取第一次的位置，得到的序列就是先序遍历
     * @param head head
     */
    public static void morrisPre(Node head) {
        if (head == null){
            return;
        }
        Node cur = head;
        Node mostRight;
        while (cur != null){
            mostRight = cur.left;
            if (mostRight != null){
                while (mostRight.right != null && mostRight.right != cur){
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null){
                    //第二次来的时候
                    System.out.println(cur.value);
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                }else {
                    mostRight.right = null;
                }
            }else {
                //第一次来的时候
                System.out.println(cur.value);
            }
            cur = cur.right;
        }
    }


    public static void morrisPos(Node head) {
        if (head == null) {
            return;
        }
        Node cur = head;
        Node mostRight;
        while (cur != null) {
            mostRight = cur.left;
            if (mostRight != null) {
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null) {
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    mostRight.right = null;
                    printEdge(cur.left);
                }
            }
            cur = cur.right;
        }
        printEdge(head);
        System.out.println();
    }


    public static void printEdge(Node head) {
        Node tail = reverseEdge(head);
        Node cur = tail;
        while (cur != null) {
            System.out.print(cur.value + " ");
            cur = cur.right;
        }
        reverseEdge(tail);
    }

    public static Node reverseEdge(Node head) {
        Node pre = null;
        Node next;
        while (head != null){
            next = head.right;
            head.right = pre;
            pre = head;
            head = next;
        }
        return pre;
    }



    public static void printTree(Node head) {
        System.out.println("Binary Tree:");
        printInOrder(head, 0, "H", 17);
        System.out.println();
    }

    public static void printInOrder(Node head, int height, String to, int len) {
        if (head == null) {
            return;
        }
        printInOrder(head.right, height + 1, "v", len);
        String val = to + head.value + to;
        int lenM = val.length();
        int lenL = (len - lenM) / 2;
        int lenR = len - lenM - lenL;
        val = getSpace(lenL) + val + getSpace(lenR);
        System.out.println(getSpace(height * len) + val);
        printInOrder(head.left, height + 1, "^", len);
    }

    public static String getSpace(int num) {
        String space = " ";
        StringBuffer buf = new StringBuffer("");
        for (int i = 0; i < num; i++) {
            buf.append(space);
        }
        return buf.toString();
    }

    /**
     * 判断一棵树是不是搜索二叉树
     * @param head head
     * @return result
     */
    public static boolean isBST(Node head) {
        if (head == null){
            return true;
        }
        Node cur = head;
        Node mostRight;
        Integer pre = null;
        while (cur != null){
            mostRight = cur.left;
            if (mostRight != null){
                while (mostRight.right != null && mostRight.right != cur){
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null){
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                }else {
                    mostRight.right = null;
                }
            }
            if (pre != null && pre > cur.value){
                return false;
            }
            pre = cur.value;
            cur = cur.right;
        }
        return true;
    }
}
