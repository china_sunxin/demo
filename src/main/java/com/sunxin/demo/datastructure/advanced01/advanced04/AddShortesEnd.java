package com.sunxin.demo.datastructure.advanced01.advanced04;

/**
 * @author sunxin
 * @date 2021/1/17 13:21
 **/
public class AddShortesEnd {

    public static void main(String[] args) {
        String str1 = "abcd123321";
        System.out.println(shortestEnd(str1));
    }

    public static String shortestEnd(String s) {
        if (s == null || s.length() == 0){
            return null;
        }
        char[] chars = manacherString(s);
        int[] pArr = new int[chars.length];
        int R = -1;
        int C = -1;
        int maxContainsEnd = -1;
        for (int i = 0; i < chars.length; i++) {
            //需要跳过的字符
            pArr[i] = R <= i ? 1 : Math.min(R - i,pArr[2 * C - i]);
            while (i - pArr[i] >= 0 && i + pArr[i] < chars.length){
                if (chars[i + pArr[i]] == chars[i - pArr[i]]){
                    pArr[i]++;
                }else {
                    break;
                }

            }
            if (i + pArr[i] > R){
                R = i + pArr[i];
                C = i;
            }
            if (R == chars.length){
                maxContainsEnd = pArr[i];
                break;
            }
        }
        char[] res = new char[s.length() - maxContainsEnd + 1];
        for (int i = 0; i < res.length; i++) {
            res[res.length - 1 - i] = chars[i * 2 + 1];
        }
        return String.valueOf(res);
    }

    public static char[] manacherString(String str) {
        char[] charArr = str.toCharArray();
        char[] res = new char[str.length() * 2 + 1];
        int index = 0;
        for (int i = 0; i != res.length; i++) {
            res[i] = (i & 1) == 0 ? '#' : charArr[index++];
        }
        return res;
    }
}
