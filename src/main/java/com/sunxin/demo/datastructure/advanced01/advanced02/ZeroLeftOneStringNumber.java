package com.sunxin.demo.datastructure.advanced01.advanced02;

/**
 * @author sunxin
 * 给定一个数N，想象只由0和1两种字符，组成的所有长度为N的字符串
 * 如果某个字符串,任何0字符的左边都有1紧挨着或者没有0认为这个字符串达标
 * 返回有多少达标的字符串
 *
 * 定义f(i)函数，假设 在 长度为 i的字符串的左侧一定有一个 "1" ，f(i) 为i长度上随意变换的数量
 * i长度上第一个数为0时，第二个数不能是0，否则不达标，所以第二个数字是1 ，后面的可能性是f(i - 2)
 * i长度上第一个数为1时，后面所有的可能性是f(i - 1)
 * 所以f(i) = f(i - 1) + f(i - 2) 即斐波那契数列。
 *
 * 这种就是构造一个特殊的函数f来计算f(i)
 *
 */
public class ZeroLeftOneStringNumber {


    public static void main(String[] args) {

    }


}
