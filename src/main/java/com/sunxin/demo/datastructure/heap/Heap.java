package com.sunxin.demo.datastructure.heap;

import com.sunxin.demo.common.AlgorithmUtils;
import lombok.Data;

/**
 * @author sunxin02
 */
@Data
public class Heap {
    public int[] heap;
    private int limit;
    public int heapSize;

    public Heap(int limit) {
        this.heap = new int[limit];
        this.limit = limit;
        this.heapSize = 0;
    }

    public void push(int value){
        if (heapSize == limit){
            throw new RuntimeException("heap is full");
        }
        heap[heapSize] = value;
        heapInsert(heap, heapSize++);
    }

    /**
     * 生成大顶堆
     * @param heap
     * @param index
     */
    public static void heapInsert(int[] heap, int index) {
        while (heap[index] > heap[(index - 1 )/2]){
            AlgorithmUtils.swap(heap,index,(index - 1) / 2);
            index = (index - 1 )/2;
        }
    }


    // 用户此时，让你返回最大值，并且在大根堆中，把最大值删掉
    // 剩下的数，依然保持大根堆组织
    public int pop() {
        int ans = heap[0];
        AlgorithmUtils.swap(heap, 0, --heapSize);
        heapify(heap, 0, heapSize);
        return ans;
    }

    /**
     * 移除堆顶，并再次将堆化
     * 将堆顶原素与堆尾元素相交换，并将堆heapSize缩小
     * 将现在堆顶的元素从上向下进行堆化
     * @param heap
     * @param heapSize
     */
    private static void heapify(int[]heap,int index,int heapSize){
         int left = index * 2 + 1;
         while (left < heapSize){
             int large = left + 1 < heapSize && heap[left + 1] > heap[left] ? left + 1 : left;
             large = heap[large] > heap[index] ? large : index;
             if (large == index){
                 break;
             }
             AlgorithmUtils.swap(heap,large,index);
             index = large;
             left = index * 2 + 1;
         }
    }

    public static void main(String[] args) {
        int arr[] = {9,8,5,2,8,4,9,5,6,1,0,8,9,6,5,4,6,7,89};
        heapSort(arr);
    }


    public static void heapSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }

        for (int i = 0; i < arr.length; i++) {
			heapInsert(arr, i);
		}
        int heapSize = arr.length;
        AlgorithmUtils.swap(arr, 0, --heapSize);

        while (heapSize > 0){
            heapify(arr,0,heapSize);
            AlgorithmUtils.swap(arr, 0, --heapSize);
        }
    }

    /**
     * 当给定一个数组来生成大顶堆或小顶堆，可以使用下面的方法，它的的时间复杂度是O(N)
     * 从下向上heapify，最下层的元素的数量是n/2 它的次数是1
     * 当数字是一个一个加入堆中时，不能使用这个方法。
     */
    public static void genereateHeap(int [] arr){
        for (int i = arr.length - 1; i >= 0; i--) {
            heapify(arr,i,arr.length);
        }
    }
}
