package com.sunxin.demo.datastructure.linkedlist;

import com.sunxin.demo.common.ObjectNode;
import com.sunxin.demo.zuoshenyun.class06.Code01_LinkedListMid;

/**
 * @author q
 */
public class LinkedListOne {

    public static void main(String[] args) {
        ObjectNode<Integer> head = new ObjectNode<Integer>(1);

    }

    /**
     * 1、输入链表头节点，奇数长度返回中点，偶数长度返回上中点
     * @param head
     * @return
     */
    public static ObjectNode<Integer> midOrUpMidNode(ObjectNode<Integer> head){
        if (head == null){
            return head;
        }
        ObjectNode fast = head;
        ObjectNode slow = head;
        while (fast.next != null && fast.next.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    /**
     *  2、输入链表头节点，奇数长度返回中点，偶数长度返回下中点
     * @param head
     * @return
     */
    public static ObjectNode<Integer> midOrUpDownNode(ObjectNode<Integer> head){
        if (head == null || head.next == null){
            return head;
        }
        ObjectNode fast = head.next;;
        ObjectNode slow = head.next;
        while (fast.next != null && fast.next.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    /**
     * 3、输入链表头节点，奇数长度返回中点前一个，偶数长度返回上中点前一个
     * @param head
     * @return
     */
    public static ObjectNode midOrUpMidPreNode(ObjectNode head) {
        if (head == null || head.next == null || head.next.next == null){
            return head;
        }
        ObjectNode fast = head.next.next;
        ObjectNode slow = head;
        while (fast.next != null && fast.next.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    /**
     * 4、输入链表头节点，奇数长度返回中点前一个，偶数长度返回下中点前一个
     * @param head
     * @return
     */
    public static Code01_LinkedListMid.Node midOrDownMidPreNode(Code01_LinkedListMid.Node head) {
        if (head == null || head.next == null) {
            return null;
        }
        if (head.next.next == null) {
            return head;
        }
        Code01_LinkedListMid.Node slow = head;
        Code01_LinkedListMid.Node fast = head.next;
        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

}
