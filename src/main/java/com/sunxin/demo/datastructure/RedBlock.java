package com.sunxin.demo.datastructure;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sunxin
 * @date 2021/3/13 9:50
 **/
public class RedBlock {

    private static final int total = 9;

    private static int result = 0;



    public static void main(String[] args) {
        long a = System.currentTimeMillis();
        int[] arr = new int[9];

        getNumber(0,-1,-1,arr);
        System.out.println(result);
        System.out.println((System.currentTimeMillis() - a)/ 1000 );
    }


    /**
     * 剩余n行，上一行用的i,j在set中
     * @param n
     * @return
     */
    private static void getNumber(int n,int prei,int prej,int[] map) {
        for (int i = 0; i < total; i++) {
            if(map[i] >= 2){
                continue;
            }
            for (int j = i + 2; j < total; j++) {
                if(!invalid(n,i,j,prei,prej,map)){
                    continue;
                }
                map[i]++;
                map[j]++;
                test(++n,i,j,map);
                map[i]--;
                map[j]--;
                n--;
            }
        }
    }

    private static void test(int n,int i, int j,int[] arr) {
        if (n == total){
            result++;
            return;
        }
        getNumber(n,i,j,arr);
    }


    private static boolean invalid(int n,int i, int j,int prei,int prej,int[] arr) {
        if (arr[j] >= 2){
            return false;
        }
        if (prei == -1 && prej == -1){
            return true;
        }
        if (i == prei || j == prej || i == prej || j == prei){
            return false;
        }
        //一  正
        if (prei == n - 1 && (prei + 1 == i || prei + 1 == j) ){
            return false;
        }
        //一  副
        if (prei + n == total && (prei - 1 == i || prei - 1 == j)){
            return false;
        }
        //二 正

        if( prej == n - 1 && (prej + 1 == i || prej + 1 == j)){
            return false;
        }
        //二  副
        if (prej + n == total && (prej - 1 == i || prej - 1 == j)){
            return false;
        }
        return true;
    }

}
