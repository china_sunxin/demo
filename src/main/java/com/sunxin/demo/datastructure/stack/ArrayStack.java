package com.sunxin.demo.datastructure.stack;

/**
 * @author sunxin02
 */
public class ArrayStack<T> {

    private T [] data = null;

    private int index;

    public ArrayStack(int size) {
        data = (T[])new Object [size];
    }

    public void headPush(T ele){
        if (data.length <= index){
            System.out.println("stack已满，无法放置元素");
            return;
        }
        data[index ++] = ele;
    }


    public void headPop(T ele){
        if (data.length >= index){
            System.out.println("stack已空，无法弹出元素");
            return;
        }
        data[index --] = ele;
    }

    public boolean isEmpty(){
        return data.length == index;
    }

    public static void main(String[] args) {
        ArrayStack<Integer> stack = new ArrayStack<>(10);
        for (int i = 0; i < 12; i++) {
            stack.headPush(i);
        }
    }

}
