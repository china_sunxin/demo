package com.sunxin.demo.datastructure.stack;

import com.sunxin.demo.common.ObjectNode;

import java.util.NoSuchElementException;

/**
 * @author sunxin02
 */
public class ListStack<T> {

    private ObjectNode head;

    public ListStack() {
        head = new ObjectNode(null);
    }

    public void headPush(T t){
        ObjectNode<T> addObjectNode = new ObjectNode<>(t);
        if (head.next == null){
            head.next = addObjectNode;
        }else {
            addObjectNode.next = head.next;
            head.next = addObjectNode;
        }
    }

    public T headPop() {
        if (head.next == null){
            throw new NoSuchElementException("stack为空");
        }else {
            T t = (T)head.next.value;
            head.next = head.next.next;
            return t;
        }
    }

    public static void main(String[] args) {
        ListStack<Integer> stack = new ListStack<>();
        for (int i = 0; i < 10; i++) {
            stack.headPush(i);
        }
        for (int i = 0; i < 11; i++) {
            System.out.println(stack.headPop());
        }
    }
}
