package com.sunxin.demo.datastructure.sort;

/**
 * @author sunxin
 */
public class MergeNoRecursive {

    public static void main(String[] args) {
        int [] arr = {2,1,4,6,454,7,5,4,7,54,3,2};
        merge(arr);
    }

    /**
     * 非递归方式实现归并排序，迭代
     */
    public static void merge(int [] arr){
        int mergeSize = 1;
        int length = arr.length;
        while (mergeSize < length){
            int l = 0;
            while (l < length){
                int m = l + mergeSize - 1;
                if (m >= length){
                    //凑不齐左组
                    break;
                }
                //凑不齐右组，r = lenth - 1
                int r = Math.min(m + mergeSize,length - 1);
                merge(arr,l,m,r);
                l = r + 1;
            }
            if (mergeSize > length / 2){
                break;
            }
            mergeSize <<= 1;
        }

    }

    private static void merge(int[] arr, int l, int m, int r) {
    }

}
