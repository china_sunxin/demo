package com.sunxin.demo.datastructure.sort;

import java.util.Arrays;

/**
 * 稳定排序
 * @author sunxin02
 */
public class MergeSort {

    public static void main(String[] args) {
        int [] arr = {9,8,7,6,5,4,3,2,1};
        process(arr,0,arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    private static void process(int[] arr, int L, int R) {
        if(L == R){
            return;
        }
        int M = (L + R) >> 1;
        process(arr,L,M);
        process(arr,M + 1,R);
        merge(arr,L,M,R);
    }


    private static void merge(int[] arr, int l, int m, int r) {
        int[] assistant = new int[r - l + 1];
        int p = l;
        int q = m + 1;
        int i = 0;
        while(p <= m && q <= r){
            assistant[i++] = arr[p] <= arr[q] ? arr[p++] : arr[q++];
        }
        while (p <= m){
            assistant[i++] = arr[p++];
        }
        while (q <= r){
            assistant[i++] = arr[q++];
        }
        if (assistant.length >= 0) {
            System.arraycopy(assistant, 0, arr, l, assistant.length);
        }
    }

}
