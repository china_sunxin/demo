package com.sunxin.demo.datastructure.sort;

/**
 * @author sunxin02
 *  归并排序的精髓就是求左边或者右边有多少个数比自己大或者小
 */
public class Merge {

    public static void main(String[] args) {
        int [] data = {1,2,3,4};
        int i = smallSum(data, 0, data.length - 1);
        System.out.println(i);
    }


    /**
     *
     * 求小和
     * int [] data = {1,2,3,4,5,4,3}
     * 左边每一个比自己小的数加起来求和
     * 其实就是求右边有几个比自己大的数字 * 本身  最后再求和
     */
    public static int smallSum(int[] data,int l,int r){
        if (l == r){
            return 0;
        }
        int m = (l + r) / 2;
        return smallSum(data,l,m) + smallSum(data,m + 1,r) + merge(data,l,m,r);
    }

    private static int merge(int[] arr, int l, int m, int r) {
        int [] help = new int[r - l + 1];
        int i = 0;
        int p1 = l;
        int p2 = m + 1;
        int res = 0;
        while (p1 <= m && p2 <= r){
            res += arr[p1] < arr[p2] ? arr[p1] * (r - p2 + 1) : 0;
            help [i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        while (p1 <= m){
            help [i++] = arr[p1++];
        }
        while (p2 <= r){
            help [i++] = arr[p2++];
        }
        for (i = 0; i < help.length; i++) {
            arr[l + i] = help[i];
        }
        return res;
    }

}
