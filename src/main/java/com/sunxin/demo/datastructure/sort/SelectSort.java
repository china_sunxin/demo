package com.sunxin.demo.datastructure.sort;

import java.util.Arrays;

/**
 * 时间复杂度：n^2
 * 空间复杂度：O(n)
 * 交换次数：n
 * 稳定性：不稳定
 * 用的很少
 * @author sunxin02
 */
public class SelectSort {

    public static void main(String[] args) {
        selectSort1();
    }

    private static void selectSort1(){
        int[] arr = {9,8,7,6,5,4,3,2,1};
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            int min = arr[i];
            int cur = i;
            for (int j = i; j < length; j++) {
                if (arr[j] < min){
                    min = arr[j];
                    cur = j;
                }
            }
            arr[cur] = arr[i];
            arr[i] = min;
        }
        System.out.println(Arrays.toString(arr));
    }
}
