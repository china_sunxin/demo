package com.sunxin.demo.datastructure.sort;

import java.util.Arrays;

/**
 * @author sunxin
 */
public class  NetherLandsFlag {
    public static void main(String[] args) {
        int[] arr = {5,1,6,5,3,9,6,4,3,5,8,7,7,6,4,0,5,9,8,4,7,8,3,2,5};

        partition(arr,0,arr.length - 1);
        int[] ints1 = netherlandsFlag1(arr, 5, 0, arr.length - 1);
        int[] ints = netherlandsFlag(arr, 0, arr.length - 1);
    }

    public static int partition(int[] arr, int L, int R) {
        if (L > R) {
            return -1;
        }
        if (L == R) {
            return L;
        }
        int lessEqual = L - 1;
        int index = L;
        while (index < R) {
            if (arr[index] <= arr[R]) {
                swap(arr, index, ++lessEqual);
            }
            index++;
        }
        swap(arr, ++lessEqual, R);
        return lessEqual;
    }
    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }


    /**
     * 荷兰国旗问题，将arr中的数小于num的放在左边，等于num的放在中间，大于num的放在右边
     * 已数组r位置的数作为比较参数
     */
    public static int[] netherlandsFlag(int[]arr,int l,int r){
        if (l > r) {
            return new int[] { -1, -1 };
        }
        if (l == r) {
            return new int[] { l, r };
        }
        // < 区 右边界
        int left = l - 1;
        // > 区 左边界
        int right = r;
        int point = l;
        while (point < right){
            if (arr[point] < arr[r]){
                swap(arr,++left,point++);
            }else if(arr[point] > arr[r]){
                swap(arr,--right,point);
            }else {
                point++;
            }
        }
        //此时需要将r位置的数与大于区最左侧的数字进行交换
        swap(arr,r,right);
        System.out.println(Arrays.toString(arr));
        return new int[]{left + 1,right};
    }

    /**
     * 大于num的 当前数与大于区的前一个交换，大于区左移
     *
     * @param arr
     * @param num 给定的比较参数
     * @param l
     * @param r
     * @return
     */
    public static int[] netherlandsFlag1(int[]arr,int num,int l,int r){
        if (l > r) {
            return new int[] { -1, -1 };
        }
        if (l == r) {
            return new int[] { l, r };
        }
        // < 区 右边界
        int left = l - 1;
        // > 区 左边界
        int right = r + 1;
        int point = l;
        while (point < right){
            if (arr[point] < num){
                swap(arr,++left,point++);
            }else if(arr[point] > num){
                swap(arr,--right,point);
            }else {
                point++;
            }
        }
        System.out.println(Arrays.toString(arr));
        return new int[]{left + 1,right - 1};
    }

}
