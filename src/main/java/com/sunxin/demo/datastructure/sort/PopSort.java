package com.sunxin.demo.datastructure.sort;


import com.sunxin.demo.common.ArrayUtils;

import java.util.Arrays;

/**
 * 遍历n次，每次把最大的数字往后移动
 * 时间复杂度O(n^2)
 * 空间复杂度：使用临时变量为O(n)
 * 交换次数：等 差数列求和
 * 稳定性：稳定/判断条件有 = 时，不稳定
 * @author sunxin02
 */
public class PopSort {

    public static void main(String[] args) {
//        pop1();
        pop2();
    }

    public static void pop1(){
        int[] arr = {9,8,7,6,5,4,3,2,1};
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length - i -1; j++) {
                if (arr[j] > arr[j+1]){
                    ArrayUtils.swap1(arr,j,j + 1);
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    public static void pop2(){
        int[] arr = {9,8,7,6,5,4,3,2,1};
        int length = arr.length;
        for (int i = length - 1; i > 0; i--) {
            for (int j = length - 1; j > length - 1 - i; j--) {
                if (arr[j] < arr[j-1]){
                    ArrayUtils.swap1(arr,j,j - 1);
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }


    /**
     * 如果是排好序的时间复杂度会变成O(n)
     * 当排序进行到数字全部在自己位置的时候，后面就不会进行，直接break.
     */
    private static void pop3(){
        int[] data = {9,8,7,6,5,4,3,2,1};
        int n = data.length;
        boolean flag = false;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (data[j] > data[j + 1]){
                    ArrayUtils.swap1(data,j,j + 1);
                    flag = true;
                }
            }
            if (!flag){
                break;
            }
        }
        System.out.println(Arrays.toString(data));
    }
}
