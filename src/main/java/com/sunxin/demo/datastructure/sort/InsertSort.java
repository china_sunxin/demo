package com.sunxin.demo.datastructure.sort;

import com.sunxin.demo.common.ArrayUtils;

import java.util.Arrays;

/**
 * 1、将数组分成已排序和未排序段，初始化时已排序段只有一个元素
 * 2、到未排序段中去元素插入到已排序段，并保证插入后仍然有序
 * 3、重复执行上述操作，知道未排序段元素全部加载完
 * 4、插入排序是稳定排序
 * 插入排序是稳定的吗？稳定
 * 希尔排序呢？不稳定的
 * 归并排序呢？稳定
 * @author sunxin02
 */
public class InsertSort {

    public static void main(String[] args) {
        int[] arr = {9,8,7,6,5,4,3,2,11};
        int length = arr.length;
        for (int i = 1; i < length; i++) {
            int cur = arr[i];
            for (int j = i -1 ; j >= 0; j--) {
                //这里> 为正序， < 为倒序
                if (arr[j] > cur){
                    arr[j + 1] = arr[j];
                    arr[j] = cur;
                }else{
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
