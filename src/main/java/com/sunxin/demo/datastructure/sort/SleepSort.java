package com.sunxin.demo.datastructure.sort;

/**
 * @author sunxin02
 * 垃圾
 */
public class SleepSort {

    public static class sleepthread extends Thread{
        private int num;
        public sleepthread(int num){
            this.num=num;
        }
        @Override
        public void run() {
            try {
                //放大睡眠时间：为了减小误差，如果数据大小比较相近，睡眠时间较短就容易出现误差
                sleep(num*1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.print(num+" ");
        }
    }

    public static void main(String[] args) {
        int num[]= {5,22,10,7,59,3,1,2,3,4,5,6,7,8,9,97,98,99};
        System.out.print("  原始数据排列：");
        for(int i:num) {
            System.out.print(i+" ");
        }
        System.out.print("\n排序后数据排列：");
        sleepthread array[]=new sleepthread[num.length];
        for(int i=0;i<array.length;i++) {
            array[i]=new sleepthread(num[i]);
            array[i].start();
        }

    }



}
