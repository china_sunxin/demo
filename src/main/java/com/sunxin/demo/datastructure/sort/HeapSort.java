package com.sunxin.demo.datastructure.sort;


import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author sunxin02
 * 将数组堆化，然后将最后一个元素和堆顶交换，heapSize减一，然后再次heapify
 * 堆排序的额外空间复杂度是O(1)，时间复杂度是O(NlogN)
 * 最终数据有序
 */
public class HeapSort {
    public static void main(String[] args) {
//        int [] arr = {9,8,7,6,5,4,3,2,1,4,5,77};
//        Heap.heapSort(arr);
//        System.out.println(Arrays.toString(arr));
        test();

    }


    public static void test(){
        PriorityQueue<Student> heap = new PriorityQueue<>(new MyComparator());
        heap.add(new Student(1,1,"1"));
        heap.add(new Student(2,2,"1"));
        heap.add(new Student(3,3,"3"));
        heap.add(new Student(4,4,"4"));
        heap.add(new Student(5,5,"5"));
        heap.add(new Student(6,6,"6"));

        while (!heap.isEmpty()){
            System.out.println(heap.poll().age);
        }


    }

    static class MyComparator implements Comparator<Student> {

        @Override
        public int compare(Student s1, Student s2) {
            return s2.age - s1.age;
        }
    }

    static class Student {
        public int id;
        public int age;
        public String name;
        public Student(int id, int age, String name) {
            this.id = id;
            this.age = age;
            this.name = name;
        }
    }
}
