package com.sunxin.demo.datastructure.sort;

import java.util.Arrays;

/**
 * @author q
 * 快速排序就是荷兰国旗问题进行递归，每次搞定一个数，最终搞定所有的数字
 * 快排 1.0 和2.0 的时间复杂度都是O(N),3.0选值，进行快排，时间复杂度是 NlogN
 * 当数组的顺序为最坏的情况：9,8,7,6,5,4,3,2,1
 * 此时用最后一个最为比较参数，时间复杂度是O(N^2)   这种情况比命中最坏的情况
 * 如果比较参数是随机选取的一个数，此时最坏情况不一定命中，比较参数的值越接近中值，时间复杂度越小，
 * 每次的值占整体的 1/N,这时的最坏时间复杂度是根据数学中的概率计算出来的一个期望值：O(NlogN)
 *
 * 额外空间复杂度计算方式同时间复杂度的计算，也是概率累加，最差是O(N)   期望值O(logN)
 *
 */
public class QuickSort {

    public static void main(String[] args) {
        int[] arr = {5,1,6,5,3,9,6,4,3,5,8,7,7,6,4,0,5,9,8,4,7,8,3,2,5};
//        partition1(arr,0,arr.length - 1);
//        quickSort1(arr,0,arr.length - 1);
//        quickSort2(arr,0,arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }


    private static void quickSort1(int[] arr, int l, int r) {
        if (l >= r){
            return;
        }
        int m = partition1(arr,l,r);
        quickSort1(arr,l,m - 1);
        quickSort1(arr,m +1,r);

    }

    private static void quickSort2(int[] arr, int l, int r) {
        if (l >= r){
            return;
        }
        //swap(arr, L + (int) (Math.random() * (R - L + 1)), R);  3.0选取随机值
        int [] m = partition2(arr,l,r);
        quickSort2(arr,l,m[1] - 1);
        quickSort2(arr,m[1] + 1,r);
    }

    public static int[] partition2(int[]arr,int l,int r){
        if (l > r) {
            return new int[] { -1, -1 };
        }
        if (l == r) {
            return new int[] { l, r };
        }
        // < 区 右边界
        int left = l - 1;
        // > 区 左边界
        int right = r;
        int point = l;
        while (point < right){
            if (arr[point] < arr[r]){
                swap(arr,++left,point++);
            }else if(arr[point] > arr[r]){
                swap(arr,--right,point);
            }else {
                point++;
            }
        }
        //此时需要将r位置的数与大于区最左侧的数字进行交换
        swap(arr,r,right);
        return new int[]{left + 1,right};
    }

    /**
     * 只需要将小于等于num的放到左边就行了，没必要等于的非在中间。
     * @param arr
     * @param L
     * @param R
     * @return
     */
    public static int partition1(int[] arr, int L, int R) {
        if (L > R) {
            return -1;
        }
        if (L == R) {
            return L;
        }
        int lessEqual = L - 1;
        int index = L;
        while (index < R) {
            if (arr[index] <= arr[R]) {
                swap(arr, index, ++lessEqual);
            }
            index++;
        }
        swap(arr, ++lessEqual, R);
        return lessEqual;
    }

    private static void sort(int[] arr, int l, int r) {
        // < 区 右边界
        int left = l - 1;
        // > 区 左边界
        int right = r;
        int point = l;
        while (point < right){
            if (arr[point] < arr[r]){
//                swap(arr,++left,point++);
                ++left;
                point++;
            }else if(arr[point] > arr[r]){
                swap(arr,--right,point);
            }else {
                point++;
            }
        }
        //此时需要将r位置的数与大于区最左侧的数字进行交换
        swap(arr,r,right);
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

}
