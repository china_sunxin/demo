package com.sunxin.demo.datastructure;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sunxin
 * @date 2021/4/18 22:43
 **/
public class LRUCache {

    static class Node{
        Node next;
        Node pre;
        Integer key;
        Integer value;

        public Node(Integer key,Integer value) {
            this.key = key;
            this.value = value;
        }

        public Node() {
        }
    }

    Node head = new Node();
    Node tail = new Node();


    int capacity;
    private int size;
    Map<Integer,Node> cache = new HashMap<>();

    public LRUCache(int capacity) {
        this.size = 0;
        this.capacity = capacity;
        head.next = tail;
        tail.pre = head;
    }

    public int get(int key) {
        Node node = cache.get(key);
        if (node == null){
            return -1;
        }
        moveToHead(node);
        return node.value;
    }

    public void put(int key, int value) {
        Node node  = cache.get(key);
        if (node == null){
            Node newNode = new Node(key,value);
            cache.put(key,newNode);
            addToHead(newNode);
            ++size;
            if (capacity < size){
                Node node1 = removeTail();
                cache.remove(node1.key);
                --size;
            }
        }else {
            node.value = value;
            moveToHead(node);
        }


    }
    private void moveToHead(Node node) {
        removeNode(node);
        addToHead(node);
    }


    private Node removeTail() {
        Node res = tail.pre;
        removeNode(res);
        return res;
    }

    private void removeNode(Node node) {
        node.pre.next = node.next;
        node.next.pre = node.pre;
    }


    private void addToHead(Node node) {
        node.pre = head;
        node.next = head.next;
        head.next.pre = node;
        head.next = node;
    }

    public static void main(String[] args) {
        LRUCache cache = new LRUCache( 2);
        cache.put(1, 1);
        cache.put(2, 2);
        System.out.println(cache.get(1));

        cache.put(3, 3);
        System.out.println(cache.get(2));
        cache.put(4, 4);
        System.out.println(cache.get(1));
        System.out.println(cache.get(3));
        System.out.println(cache.get(4));
    }

}
