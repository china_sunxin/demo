package com.sunxin.demo.datastructure.queue;

import com.sunxin.demo.common.ObjectNode;

import java.util.NoSuchElementException;

/**
 * @author sunxin02
 */
public class ListQueue<T> {

    private ObjectNode head;
    private ObjectNode tail;

    public void headPush(T t){
        ObjectNode addObjectNode = new ObjectNode(t);
        if(head == null){
            head = addObjectNode;
            tail = addObjectNode;
        }else {
            addObjectNode.next = head;
            head.pre = addObjectNode;
            head = addObjectNode;
        }
    }

    public T tailPop() {
        if (tail == null){
            throw new NoSuchElementException("stack为空");
        }
        T t = (T)tail.value;
        ObjectNode last = tail;
        if(head == tail){
            head = tail = null;
        }else {
            tail = tail.pre;
            tail.next = null;
            last.next = null;
        }
        return t;
    }


    public static void main(String[] args) {
        ListQueue<Integer> queue = new ListQueue<>();
        for (int i = 0; i < 10; i++) {
            queue.headPush(i);
        }
        for (int i = 0; i < 11; i++) {
            System.out.println(queue.tailPop());
        }
    }

    public static void testBuffer() {
        StringBuffer s = new StringBuffer("hello");
        StringBuffer s2 = new StringBuffer("word");
        test(s, s2);
    }

    static void test(StringBuffer s, StringBuffer s2) {
        s2 = s;
        s = new StringBuffer("h1");
        s.append("welcome");
        s2.append("welcome");
    }
}
