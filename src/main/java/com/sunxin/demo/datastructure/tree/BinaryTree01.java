package com.sunxin.demo.datastructure.tree;

import com.sunxin.demo.common.TreeNode;

public class BinaryTree01 {

    public static void main(String[] args) {
        TreeNode head = new TreeNode(1);
        head.left = new TreeNode(2);
        head.right = new TreeNode(3);
        head.left.left = new TreeNode(4);
        head.left.right = new TreeNode(5);
        head.right.left = new TreeNode(6);
        head.right.right = new TreeNode(7);
        pos(head);

    }

    private static void pre(TreeNode<Integer> root){
        if (root == null){
            return;
        }
        System.out.println(root.value);
        pre(root.left);
        pre(root.right);
    }

    private static void mid(TreeNode<Integer> root){
        if (root == null){
            return;
        }
        mid(root.left);
        System.out.println(root.value);
        mid(root.right);
    }

    private static void pos(TreeNode<Integer> root){
        if (root == null){
            return;
        }
        pos(root.left);
        pos(root.right);
        System.out.println(root.value);
    }
}
