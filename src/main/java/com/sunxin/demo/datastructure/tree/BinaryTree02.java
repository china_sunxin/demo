package com.sunxin.demo.datastructure.tree;

import com.sunxin.demo.common.TreeNode;

import java.util.Stack;

public class BinaryTree02 {

    public static void main(String[] args) {
        TreeNode head = new TreeNode(1);
        head.left = new TreeNode(2);
        head.right = new TreeNode(3);
        head.left.left = new TreeNode(4);
        head.left.right = new TreeNode(5);
        head.right.left = new TreeNode(6);
        head.right.right = new TreeNode(7);

//        pre(head);
//        pre(head);
        pos1(head);
    }


    /**
     * 前序遍历 ，头左右
     * @param root
     */
    public static void pre(TreeNode<Integer> root){
        Stack<TreeNode<Integer>> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            root = stack.pop();
            System.out.println(root.value);
            if (root.right != null){
                stack.push(root.right);
            }if (root.left != null) {
                stack.push(root.left);
            }
        }
    }

    /**
     * 头右左，反过来就是左右头即后序遍历
     * @param root
     */
    public static void pos(TreeNode<Integer> root){
        Stack<TreeNode<Integer>> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            root = stack.pop();
            System.out.println(root.value);
            if (root.left != null){
                stack.push(root.left);
            }if (root.right != null) {
                stack.push(root.right);
            }
        }
    }

    public static void pos1(TreeNode<Integer> root){
        Stack<TreeNode<Integer>> stack = new Stack<>();
        Stack<TreeNode<Integer>> stack2 = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            root = stack.pop();
            stack2.push(root);
            if (root.left != null){
                stack.push(root.left);
            }if (root.right != null) {
                stack.push(root.right);
            }
        }

        while (!stack2.isEmpty()){
            System.out.println(stack2.pop().value);
        }
    }

}
