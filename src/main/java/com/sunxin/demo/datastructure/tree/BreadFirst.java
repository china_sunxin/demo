package com.sunxin.demo.datastructure.tree;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class BreadFirst {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    public static void main(String[] args) {
        Node head = new Node(1);
        head = new Node(1);
        head.left = new Node(2);
        head.right = new Node(3);
        head.left.left = new Node(4);
        head.right.left = new Node(5);
        head.right.right = new Node(6);
        head.left.left.right = new Node(7);
        breadFirst(head);
    }

    private static void breadFirst(Node root){
        Queue<Node> queue = new ArrayBlockingQueue<Node>(100);
        queue.add(root);
        while (!queue.isEmpty()){
            Node cur = queue.poll();
            Node left = cur.left;
            Node right = cur.right;
            System.out.println(cur.value);
            if (left != null){
                queue.add(left);
            }
            if (right != null){
                queue.add(right);
            }
        }

    }
}
