package com.sunxin.demo.datastructure.base.base02;

import com.sunxin.demo.common.DoubleNode;
import com.sunxin.demo.common.ListUtils;
import com.sunxin.demo.common.ObjectNode;
import com.sunxin.demo.common.NodeUtils;

/**
 * @author sunxin
 */
public class ReverseList {

    public static void main(String[] args) {
        ObjectNode<Integer> objectNode = NodeUtils.generateRandomLinkedList(10, 10);
        ListUtils.printList(objectNode);
        ObjectNode<Integer> reverseObjectNode = reverseList(objectNode);
        ListUtils.printList(reverseObjectNode);


        DoubleNode doubleNode = NodeUtils.generateRandomDoubleList(10, 10);
        ListUtils.printList(doubleNode);
        DoubleNode reverseDoubleNode = reverseDoubleList(doubleNode);
        ListUtils.printList(reverseDoubleNode);
    }

    /**
     * 单链表反转
     * @param head
     * @return
     */
    public static ObjectNode<Integer> reverseList(ObjectNode<Integer> head){
        ObjectNode<Integer> next = null;
        ObjectNode<Integer> pre = null;
        while (head != null){
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    public static DoubleNode reverseDoubleList(DoubleNode head){
        DoubleNode next = null;
        DoubleNode pre = null;
        while (head != null){
            next = head.next;
            head.next = pre;
            head.pre = next;
            pre = head;
            head = next;
        }
        return pre;
    }


}
