package com.sunxin.demo.datastructure.base.base03;

import com.sunxin.demo.common.ArrayUtils;

import java.util.Arrays;

/**
 * @author sunxin
 */
public class QuickSort {

    public static void main(String[] args) {
        int[] arr = {9,8,7,6,5,4,3,2,1};
//        process1(arr,0,arr.length - 1);
//        process2(arr,0,arr.length - 1);
        process3(arr,0,arr.length - 1);

        System.out.println(Arrays.toString(arr));
    }

    private static void process1(int[] arr,int l,int r){
        if (l >= r){
            return;
        }
        int mid = partition1(arr,l,r);
        process1(arr,l,mid - 1);
        process1(arr,mid + 1,r);
    }

    private static int partition1(int[] arr,int l,int r){
        if (l > r){
            return -1;
        }
        if(l == r){
            return l;
        }
        int lIndex = l - 1;
        int index = l;
        while (index < r){
            if (arr[index] <= arr[r]){
                ArrayUtils.swap(arr,index,++lIndex);
            }
            index++;
        }
        ArrayUtils.swap(arr,++lIndex,r);
        return lIndex;
    }


    private static void process2(int[] arr,int l,int r){
        if (l >= r){
            return;
        }
        int[] mid = partition2(arr,l,r);
        process1(arr,l,mid[0] - 1);
        process1(arr,mid[1] + 1,r);
    }

    private static int[] partition2(int[] arr,int l,int r){
        if (l > r) {
            return new int[] { -1, -1 };
        }
        if (l == r) {
            return new int[] {l,r};
        }
        int lIndex = l - 1;
        int rIndex = r;
        int index = l;
        while (index < rIndex){
            if (arr[index] < arr[r]){
                ArrayUtils.swap(arr,index++,++lIndex);
            }else if(arr[index] > arr[r]){
                ArrayUtils.swap(arr,index,--rIndex);
            }else {
                index++;
            }
        }
        ArrayUtils.swap(arr,rIndex,r);
        return new int[]{++lIndex,rIndex};
    }

    public static void process3(int[] arr, int L, int R) {
        if (L >= R) {
            return;
        }
        ArrayUtils.swap(arr, L + (int) (Math.random() * (R - L + 1)), R);
        int[] equalArea = partition2(arr, L, R);
        process1(arr, L, equalArea[0] - 1);
        process1(arr, equalArea[1] + 1, R);
    }



}
