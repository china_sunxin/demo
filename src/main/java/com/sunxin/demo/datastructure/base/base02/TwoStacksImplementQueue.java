package com.sunxin.demo.datastructure.base.base02;

import java.util.Stack;

/**
 * 只用栈结构实现队列
 * @author sunxin
 */
public class TwoStacksImplementQueue {

    public static void main(String[] args) {
        TwoStacksQueue queue = new TwoStacksQueue();
        for (int i = 0; i < 10; i++) {
            queue.push(i);
        }

        for (int i = 0; i < 8; i++) {
            System.out.println(queue.pop());
        }
        for (int i = 0; i < 7; i++) {
            queue.push(i);
        }

        for (int i = 0; i < 5; i++) {
            System.out.println(queue.pop());
        }
    }

    public static class TwoStacksQueue{
        private Stack<Integer> pushStack;
        private Stack<Integer> popStack;

        public TwoStacksQueue(){
            pushStack = new Stack<>();
            popStack = new Stack<>();
        }

        /**
         *
         * @param value
         */
        public void push(int value){
            pushStack.push(value);
        }


        public int pop(){
            if (popStack.isEmpty()) {
                while (!pushStack.isEmpty()) {
                    popStack.push(pushStack.pop());
                }
            }
            if (popStack.isEmpty()){
                throw new IllegalStateException("queue is enpty");
            }
            return popStack.pop();
        }



    }


}
