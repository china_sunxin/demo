package com.sunxin.demo.datastructure.base.base06;

/**
 * @author sunxin
 * 给定两个可能有环也可能无环的单链表，头节点head1和head2。请实现一个函数，如果两个链表相交，请返回相交的 第一个节点。如果不相交，返回null
 * 【要求】
 * 如果两个链表长度之和为N，时间复杂度请达到O(N)，额外空间复杂度 请达到O(1)。
 */
public class FindFirstIntersectNode {

    public static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }

    /**
     * 先判断两个链表的自身情况是不是有环
     * 1、两个都没有环
     * 2、一个有环、一个无环
     * 3、两个都有环
     * @param args
     */
    public static void main(String[] args) {



    }

    /**
     *
     * @param head1
     * @param head2
     * @return
     */
    public static Node getIntersectNode(Node head1, Node head2) {
        if (head1 == null || head2 == null){
            return null;
        }
        Node loopNode1 = getLoopNode(head1);
        Node loopNode2 = getLoopNode(head2);
        //两个链表都无环
        if (loopNode1 == null && loopNode2 == null){
            return noLoop(head1, head2);
        }
        //两个链表都有环
        if (loopNode1 != null && loopNode2 != null) {

        }
        //一个有环一个无环是不可能相交的
        return null;
    }


    //两个有环链表，返回第一个相交节点，如果不想交返回null
    public static Node bothLoop(Node head1, Node loop1, Node head2, Node loop2) {
        Node cur1;
        Node cur2;
        if (loop1 == loop2) {
            cur1 = head1;
            cur2 = head2;
            int n = 0;
            while (cur1 != loop1){
                n++;
                cur1 = cur1.next;
            }
            while (cur2 != loop2){
                n--;
                cur2 = cur2.next;
            }
            cur1 = n > 0 ? head1 : head2;
            cur2 = cur1 == head1 ? head2 : head1;
            while (n != 0){
                cur1 = cur1.next;
            }
            while (cur1 != cur2){
                cur1 = cur1.next;
                cur2 = cur2.next;
            }
            return cur1;
        }else {
            cur1 = loop1.next;
            while (cur1 != loop1){
                if (cur1 == loop2){
                    return loop1;
                }
                cur1 = cur1.next;
            }
            return null;
        }
    }

    public static Node noLoop(Node head1, Node head2){
        if (head1 == null || head2 == null) {
            return null;
        }
        Node cur1 = head1;
        Node cur2 = head2;

        int n = 0;
        while (cur1.next != null){
            cur1 = cur1.next;
            n++;
        }
        while (cur2.next != null){
            n--;
            cur2 = cur2.next;
        }

        //不相交
        if (cur1 == null){
            return null;
        }

        //长链表
        cur1 = n > 0 ? head1 : head2;
        //短链表
        cur2 = cur1 == head1 ? head2 : head1;

        n = Math.abs(n);
        while (n != 0){
            n--;
            cur1 = cur1.next;
        }
        while (cur1 != cur2) {
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        return cur1;
    }


    /**
     * 获取链表入环的第一个点，没有环则返回null
     * @param head
     * @return
     */
    public static Node getLoopNode(Node head) {
        if (head == null || head.next.next == null){
            return head;
        }
        Node slow= head.next;
        Node fast = head.next.next;

        while (fast != slow){
            if (fast.next == null || fast.next.next == null){
                return null;
            }
            fast = fast.next.next;
            slow = slow.next;
        }
        fast = head;
        while (fast != slow){
            fast = fast.next;
            slow = slow.next;
        }
        return fast;
    }

}
