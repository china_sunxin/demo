package com.sunxin.demo.datastructure.base.base07;


/**
 * @author sunxin02
 * 给你二叉树中的某个节点，返回该节点的后继节点
 * 后继节点 前序遍历中该节点的后一个节点。
 */
public class SuccessorNode {

    public static class Node {
        public int value;
        public Node left;
        public Node right;
        public Node parent;

        public Node(int data) {
            this.value = data;
        }
    }


    public static Node getSuccessorNode(Node node) {
        if (node == null){
            return node;
        }
        if (node.right == null){
            Node parent = node.parent;
            while (parent != null && parent.left != node){
                node = parent;
                parent = node.parent;
            }
        }else {
            while (node.left != null){
                node = node.left;
            }
            return node;
        }


        return null;
    }

}
