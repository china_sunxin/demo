package com.sunxin.demo.datastructure.base.base03;

import com.sunxin.demo.common.ArrayUtils;

import java.util.Arrays;

/**
 * @author sunxin
 */
public class MergeSort {

    public static void main(String[] args) {
        int[] arr = ArrayUtils.generateRandomArray(1000, 20);
        mergeSort(arr,0,arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    /**
     * 归并排序递归的实现
     * 时间复杂度为 NlogN
     * @param arr
     * @param l
     * @param r
     */
    private static void mergeSort(int[] arr, int l, int r) {
        if (l == r){
            return;
        }
        int mid = l + ((r - l) >> 1);
        mergeSort(arr,l,mid);
        mergeSort(arr,mid + 1,r);
        merge(arr,l,mid,r);
    }

    /**
     * mereg的时间复杂度是O(N) * 2 ，忽略常数项 即 O(N)
     * @param arr
     * @param l
     * @param mid
     * @param r
     */
    private static void merge(int[] arr, int l, int mid,int r) {
        int[] help = new int[r - l + 1];
        int lIndex = l;
        int rIndex = mid + 1;
        int start = 0;
        while (lIndex <= mid && rIndex <= r){
            help[start++] = arr[lIndex] <= arr[rIndex] ? arr[lIndex++] : arr[rIndex++];
        }
        while (lIndex <= mid){
            help[start++] = arr[lIndex++];
        }
        while (rIndex <= r){
            help[start++] = arr[rIndex++];
        }
        if (help.length >= 0) System.arraycopy(help, 0, arr, l, help.length);
    }

    /**
     * 归并排序的非递归方式
     * @param arr
     */
    private static void mergeSortWithOutRecursive(int[] arr){
        if (arr == null || arr.length < 2) {
            return;
        }
        int mergeSize = 1;
        int n = arr.length;
        while (mergeSize < n){
            int l = 0;
            while (l < n){
                int m = l + mergeSize - 1;
                int r = Math.min(m + mergeSize,n - 1);
                merge(arr,l,m,r);
                l = r + 1;
            }
            //防止下面的左移越界，提前退出
            if (mergeSize > arr.length / 2){
                break;
            }
            mergeSize <<= 1;
        }
    }
}
