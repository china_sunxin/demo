package com.sunxin.demo.datastructure.base.base02;

import java.util.Stack;

/**
 * @author sunxin
 */
public class GetMinStack {

    public static void main(String[] args) {

    }

    public static class MyMinStack{
        private Stack<Integer> stack;
        private Stack<Integer> minStack;

        public MyMinStack(){
            stack = new Stack<>();
            minStack = new Stack<>();
        }

        public void push(int value){
            if (minStack.isEmpty()){
                minStack.push(value);
            }else {
                minStack.push(minStack.peek() < value ? minStack.peek() : value);
            }
            stack.push(value);
        }

        public Integer pop(){
            if (stack.isEmpty()){
                throw new IllegalStateException("stack is empty");
            }
            minStack.pop();
            return stack.pop();
        }

        public Integer getMin(){
            if (minStack.isEmpty()){
                throw new IllegalStateException("stack is empty");
            }
            return minStack.peek();
        }

    }

}
