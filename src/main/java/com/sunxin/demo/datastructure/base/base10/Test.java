package com.sunxin.demo.datastructure.base.base10;

import java.util.HashSet;

public class Test {
    public static void main(String[] args) {
        String [] arr = {"a","b","c","d"};
        process(arr,new HashSet<>(),"");


    }

    public static void process(String [] arr, HashSet<Integer> use, String path){
        if (use.size() == arr.length){
            System.out.println(path);
        }
        for (int i = 0; i < arr.length; i++) {
            if (!use.contains(i)){
                use.add(i);
                process(arr,use,path + arr[i]);
                use.remove(i);
            }

        }
    }
}
