package com.sunxin.demo.datastructure.base.base03;

/**
 * @author sunxin
 * 在一个数组中，一个数左边比它小的数的总和，叫数的小和，所有数的小和累加起来，叫数组小和。求数组小和。
 */
public class SmallSum {


    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        int result = smallSum(arr, 0, arr.length - 1);
        System.out.println(result);
    }

    private static int smallSum(int[] arr,int l,int r){
        if (l == r){
            return 0;
        }
        int mid = l + ((r - l) >> 1);
        return smallSum(arr,l,mid) +
        smallSum(arr,mid + 1,r) +
        process(arr,l,mid,r);
    }

    private static int process(int[] arr, int l, int mid, int r) {
        int[] help = new int[r - l + 1];
        int lIndex = l;
        int rIndex = mid + 1;
        int start = 0;
        int result = 0;
        while (lIndex <= mid && rIndex <= r){
            result += arr[lIndex] < arr[rIndex] ? arr[lIndex] * (r - rIndex + 1) : 0;
            help[start++] = arr[lIndex] < arr[rIndex] ? arr[lIndex++] : arr[rIndex++];
        }
        while (lIndex <= mid){
            help[start++] = arr[lIndex++];
        }
        while (rIndex <= r){
            help[start++] = arr[rIndex++];
        }
        if (help.length >= 0) System.arraycopy(help, 0, arr, l, help.length);
        return result;
    }

}
