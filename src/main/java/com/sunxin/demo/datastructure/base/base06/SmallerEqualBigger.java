package com.sunxin.demo.datastructure.base.base06;

/**
 * @author sunxin
 */
public class SmallerEqualBigger {

    public static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }

    public static Node listPartition2(Node head, int pivot) {
        Node sH = null; // small head
        Node sT = null; // small tail
        Node eH = null; // equal head
        Node eT = null; // equal tail
        Node mH = null; // big head
        Node mT = null; // big tail
        Node next = null; // save next node
        while (head != null){
            next = head.next;
            head.next = null;
            if (head.value > pivot){
                if (mH == null){
                    mH = mT = head;
                }else {
                    mT.next = head;
                    mT = head;
                }
            }else if (head.value == pivot) {
                if (eH == null) {
                    eH = head;
                    eT = head;
                } else {
                    eT.next = head;
                    eT = head;
                }
            }else {
                if (sH == null){
                    sH = sT = head;
                }else {
                    sT.next = head;
                    sT = head;
                }
            }
            head = next;
        }

        if (sT != null) {
            sT.next = eH;
            eT = eT == null ? sT : eT;
        }
        if (eT != null) {
            eT.next = mH;
        }
        return sH != null ? sH : (eH != null ? eH : mH);


    }

}
