package com.sunxin.demo.datastructure.base.base05;

/**
 * @author sunxin
 */
public class RadixSort {
    public static void main(String[] args) {

    }

    public static void radixSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        radixSort(arr, 0, arr.length - 1, maxbits(arr));
    }


    public static int maxbits(int[] arr) {
        int max = 0;
        for (int value : arr) {
            max = Math.max(max, value);
        }
        int res = 0;
        while (max / 10 > 0){
            res++;
            max /= 10;
        }
        return res;
    }

    public static void radixSort(int[] arr, int L, int R, int digit) {
        int[] help = new int[R - L + 1];
        for (int i = 0; i < digit; i++) {
            int[] count = new int[10];
            for (int j = L; j <= R - L; j++) {
                int digit1 = getDigit(arr[j], i);
                count[digit1]++;
            }
            for (int j = 0; j < count.length; j++) {
                count[j] += count[j];
            }
            for (int j = R; j >= L; j--) {
                int digit1 = getDigit(arr[j], i);
                help[count[digit1]] = arr[i];
                count[digit1]--;
            }
            for (int t = L, j = 0; t <= R; t++, j++) {
                arr[t] = help[j];
            }
        }

    }

    /**
     * 获取数字指定位数的值
     * @param num 数字
     * @param d 位数
     * @return
     */
    public static int getDigit(int num,int d){
        return num / 10 / (d + 1) % 10;
    }



}
