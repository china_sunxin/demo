package com.sunxin.demo.datastructure.base.base02;

import com.sunxin.demo.common.ListUtils;
import com.sunxin.demo.common.Node;

/**
 * @author sunxin
 */
public class DeleteGivenValue {


    public static void main(String[] args) {
        Node cur = new Node(1);
        Node head = cur;

        for (int i = 2; i < 10; i++) {
            cur.next = new Node(i);
            cur = cur.next;
        }
        ListUtils.printList(head);
        Node node = deleteGivenValue(head, 5);
        ListUtils.printList(node);
    }

    /**
     * 将给定的值都删除
     * @param head
     * @param deleteNum
     */
    private static Node deleteGivenValue(Node head,int deleteNum){
        while (head != null){
            if (head.value != deleteNum){
                break;
            }
            head = head.next;
        }
        Node cur = head;
        Node pre = head;
        while (cur != null){
            if (cur.value == deleteNum){
                pre.next = cur.next;
            }else {
                pre = cur;
            }
            cur = cur.next;
        }
        return head;
    }

}
