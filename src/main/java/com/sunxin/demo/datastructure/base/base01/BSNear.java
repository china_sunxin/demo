package com.sunxin.demo.datastructure.base.base01;

/**
 * @author sunxin
 */
public class BSNear {

    public static void main(String[] args) {
        int[] arr = {1,2,3,5,6,7,8};
        int num = 3;
        int b = bsNear(arr, num);
        System.out.println(b);
    }

    /**
     * 在一个有序数组中，找>=某个数最左侧的位置
     * @param arr
     * @param num
     * @return
     */
    private static int bsNear(int[] arr, int num) {
        int l = 0;
        int r = arr.length - 1;
        int index = 0;
        while (l < r){
            int mid = l + ((r - l) >> 2);
            if (arr[mid] >= num){
                index = mid;
                r = mid - 1;
            }else if(arr[mid] < num){
                l = mid + 1;
            }
        }
        return arr[index];
    }

}
