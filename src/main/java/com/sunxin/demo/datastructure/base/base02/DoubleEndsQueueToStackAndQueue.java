package com.sunxin.demo.datastructure.base.base02;

import com.sunxin.demo.common.Node;

/**
 * @author sunxin
 */
public class DoubleEndsQueueToStackAndQueue {

    public static void main(String[] args) {

    }


    public static class DoubleEndsQueue{
        private static Node tail;
        private static Node head;

        public void addFromHead(int value){
            Node node = new Node(value);
            if (head == null){
                head = node;
                tail = head;
            }else {
                node.next = head;
                head.pre = node;
                head = node;
            }
        }


        public void addFromBottom(int value){
            Node node = new Node(value);
            if (head == null){
                head = node;
                tail = head;
            }else {
                node.pre = tail;
                tail.next = node;
                tail = node;
            }
        }

        public Integer popFromHead(){
            if (head == null){
                return null;
            }
            Node cur = head;
            if (head == tail){
                head = null;
                tail = null;
            }else {
                head = head.next;
                cur.next = null;
                head.pre = null;
            }
            return cur.value;
        }


        public Integer popFromBottom(){
            if (head == null){
                return null;
            }
            Node cur = tail;
            if (head == tail){
                head = null;
                tail = null;
            }else {
                tail = tail.pre;
                tail.next = null;
                cur.pre = null;
            }
            return cur.value;
        }

        public boolean isEmpty(){
            return head == null;
        }
    }

    public static class MyStack{
        private static DoubleEndsQueue stack;

        private void push(Integer value){
            stack.addFromHead(value);
        }

        private Integer pop(){
            return stack.popFromHead();
        }

        public boolean isEmpty(){
            return stack.isEmpty();
        }
    }

    public static class MyQueue{
        private static DoubleEndsQueue queue;

        private void push(Integer value){
            queue.addFromHead(value);
        }

        private Integer poll(){
            return queue.popFromBottom();
        }

        public boolean isEmpty(){
            return queue.isEmpty();
        }
    }

}
