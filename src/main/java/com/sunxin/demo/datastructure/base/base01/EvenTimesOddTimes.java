package com.sunxin.demo.datastructure.base.base01;

/**
 * @author sunxin
 */
public class EvenTimesOddTimes {

    public static void main(String[] args) {

        int i = bit1counts(4);
        System.out.println(i);


    }


    /**
     * 计算多少个二进制1
     * @param n
     * @return
     */
    private static int bit1counts(int n) {
        int count = 0;
        while (n != 0){
            count ++;
            n ^= (n & (~n + 1));
        }
        return count;
    }

    /**
     * 一个数组中有两种数出现了奇数次，其他数都出现了偶数次，找到这两种数
     * @param arr
     */
    public static void printOddTimesNum2(int[] arr) {
        int eor = 0;
        for (int i = 0; i < arr.length; i++) {
            eor ^= arr[i];
        }
        // eor = a ^ b
        // eor != 0
        // eor必然有一个位置上是1
        int rightOne = eor & (~eor + 1); // 提取出最右的1
        int onlyOne = 0; // eor'
        for (int i = 0 ; i < arr.length;i++) {
            if ((arr[i] & rightOne) != 0) {
                onlyOne ^= arr[i];
            }
        }
        System.out.println(onlyOne + " " + (eor ^ onlyOne));
    }

}
