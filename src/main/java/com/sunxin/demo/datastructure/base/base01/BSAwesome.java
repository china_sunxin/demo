package com.sunxin.demo.datastructure.base.base01;

/**
 * @author sunxin
 * 局部最小值问题
 */
public class BSAwesome {


    public static void main(String[] args) {
        int[] arr = {1,2,8,6,3,7,5,2,8,2,5};
    }

    public static int bsAwesome(int[] arr){
        if (arr == null || arr.length == 0){
            return -1;
        }
        if (arr.length == 1 || arr[0] < arr[1]){
            return 0;
        }
        if (arr[arr.length - 1] > arr[arr.length - 2]){
            return arr.length - 1;
        }
        int l = 1;
        int r = arr.length - 2;
        while (l < r){
            int mid = l + ((r - l) >> 1);
            if (arr[mid] > arr[mid - 1]){
                r = mid - 1;
            }else if(arr[mid] > arr[mid +1]){
                l = mid + 1;
            }else {
                return mid;
            }
        }
        return l;
    }


}
