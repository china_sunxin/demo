package com.sunxin.demo.datastructure.base.base02;

/**
 * @author sunxin
 */
public class GetArrayMax {

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8,9};
        int max = proces(arr, 0, arr.length - 1);
        System.out.println(max);
    }

    private static int proces(int[] arr, int l, int r) {
        if (l == r){
            return arr[l];
        }
        int mid = l + ((r - l) >> 1);
        int lMax = proces(arr,l,mid);
        int rMax = proces(arr,mid +1,r);
        return Math.max(lMax,rMax);
    }


}
