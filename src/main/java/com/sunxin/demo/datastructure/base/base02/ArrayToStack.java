package com.sunxin.demo.datastructure.base.base02;

/**
 * @author q
 */
public class ArrayToStack {

    public static void main(String[] args) {
        MyArrayStack stack = new MyArrayStack(8);

        for (int i = 0; i < 8; i++) {
            stack.push(i);
        }
        for (int i = 0; i < 9; i++) {
            System.out.println(stack.pop());
        }
    }

    public static class MyArrayStack{
        int[] arr;
        int index = -1;
        public MyArrayStack(int capacity){
            arr = new int[capacity];
        }

        public boolean isFull(){
            return arr.length - 1 <= index;
        }

        public void push(int value){
            if (isFull()){
                throw new IllegalStateException("stack is full");
            }
            arr[++index] = value;
        }

        public int pop(){
            if (index < 0){
                throw new IllegalStateException("stack is empty");
            }
            return arr[index--];
        }
    }

}
