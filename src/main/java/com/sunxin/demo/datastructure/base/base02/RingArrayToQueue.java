package com.sunxin.demo.datastructure.base.base02;

/**
 * @author sunxin
 */
public class RingArrayToQueue {

    public static void main(String[] args) {
        MyArrayQueue queue = new MyArrayQueue(8);

        for (int i = 0; i < 800; i++) {
            queue.push(i);
            System.out.println(queue.poll());
        }


    }


    public static class MyArrayQueue{
        private int[] arr;
        private int pushi;
        private int polli;
        private int limit;
        //通过添加size字段，免去了维护pushi和polli之间的追赶问题，将两者之间的关系解耦。
        private int size;

        public MyArrayQueue(int limit){
            this.limit = limit;
            arr = new int[limit];
        }

        private void push(int value){
            if (size >= limit){
                throw new IllegalStateException("queue is full");
            }
            size ++;
            arr[pushi] = value;
//            if (++pushi > limit - 1){
//                pushi = 0;
//            }
            pushi = nexIndex(pushi);
        }

        private int poll(){
            if (size <= 0){
                throw new IllegalStateException("queue is empty");
            }
            size --;
            int value = arr[polli];
//            if (++polli > limit - 1){
//                polli = 0;
//            }
            polli = nexIndex(polli);
            return value;
        }

        private int nexIndex(int index){
            return index < limit - 1 ? index++ : 0;
        }

    }
}
