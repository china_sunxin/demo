package com.sunxin.demo.datastructure.base.base03;

import com.sunxin.demo.common.ArrayUtils;

import java.util.Arrays;

/**
 * @author sunxin
 */
public class Partition {

    public static void main(String[] args) {
        int[] arr = {9,8,7,6,5,4,3,2,1};
        int[] partition = partition(arr, 5);
        System.out.println(Arrays.toString(partition));
    }


    /**
     * 给定一个数组arr，和一个整数num。请把小于num的数放在数组的左边，等于num的数放在中间，大于num的数放在数组的右边。
     * 要求额外空间复杂度O(1)，时间复杂度O(N)
     */
    private static int[] partition(int[] arr,int num){
        int l = -1;
        int r = arr.length;
        int index = 0;
        while (index < r){
            if (arr[index] > num){
                ArrayUtils.swap(arr,index,--r);
            }else if(arr[index] < num){
                ArrayUtils.swap(arr,++l,index++);
            }else {
                index++;
            }
        }
        ArrayUtils.swap(arr,index,arr.length - 1);
        return arr;
    }


}
