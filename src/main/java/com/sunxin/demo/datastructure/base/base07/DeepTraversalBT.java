package com.sunxin.demo.datastructure.base.base07;

import java.util.Stack;

/**
 * @author sunxin
 * @date 2021/4/17 23:17
 * 深度优先遍历
 **/
public class DeepTraversalBT {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int v) {
            value = v;
        }
    }

    public static void main(String[] args) {
        Node head = new Node(1);
        head.left = new Node(2);
        head.right = new Node(3);
        head.left.left = new Node(4);
        head.left.right = new Node(5);
        head.right.left = new Node(6);
        head.right.right = new Node(7);
        deepTraversalBt(head);
    }

    private static void deepTraversalBt(Node head) {
        if (head == null){
            return;
        }
        Stack<Node> stack = new Stack<>();
        stack.push(head);
        Node cur ;
        while (!stack.isEmpty()){
            cur = stack.pop();
            System.out.println(cur.value);
            if (cur.right != null) {
                stack.push(cur.right);
            }
            if(cur.left != null) {
                stack.push(cur.left);
            }
        }
    }

}
