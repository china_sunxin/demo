package com.sunxin.demo.datastructure.base.base01;

/**
 * @author sunxin
 */
public class BSExist {

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8};
        int num = 1;
        boolean b = bsExist(arr, num);
        System.out.println(b);
    }

    /**
     * 在一个有序数组中，找某个数是否存在
     * @param arr
     */
    private static boolean bsExist(int[] arr,int num) {
        if (arr == null || arr.length == 0) {
            return false;
        }
        int l = 0;
        int r = arr.length - 1;
        while (l < r){
            int mid = l + ((r - l) >> 1);
            if (arr[mid] > num){
                r = mid - 1;
            }else if(arr[mid] < num){
                l = mid + 1;
            }else {
                return true;
            }
        }
        return arr[l] == num;
    }
}
