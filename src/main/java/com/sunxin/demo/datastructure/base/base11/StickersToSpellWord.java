package com.sunxin.demo.datastructure.base.base11;

import java.util.HashMap;

/**
 * @author sunxin02
 *
 * 题目二
 * 给定一个字符串str，给定一个字符串类型的数组arr。
 * arr里的每一个字符串，代表一张贴纸，你可以把单个字符剪开使用，目的是拼出str来。
 * 返回需要至少多少张贴纸可以完成这个任务。
 * 例子：str= "babac"，arr = {"ba","c","abcd"}
 * 至少需要两张贴纸"ba"和"abcd"，因为使用这两张贴纸，把每一个字符单独剪开，含有2个a、2个b、1个c。是可以拼出str的。所以返回2。
 */
public class StickersToSpellWord {

    public static void main(String[] args) {
        String[] arr = {"abc","bc"};
        String str = "asdfasdfasdfadgh4eyus";
        int[][]map = new int[arr.length][26];
        HashMap<String, Integer> dp = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            char[] chars = arr[i].toCharArray();
            for (int j = 0; j < chars.length; j++) {
                map[i][chars[i] - 'a']++;
            }
        }
        dp.put("",0);
    }


    // dp 傻缓存，如果t已经算过了，直接返回dp中的值
    // t 剩余的目标
    // 0..N每一个字符串所含字符的词频统计
    public static int process1(HashMap<String, Integer> dp, int[][] map, String t) {
        if (dp.containsKey(t)){
            return dp.get(t);
        }
        int ans = Integer.MAX_VALUE;
        int []tmap =  new int[26];
        char[] target = t.toCharArray();
        for (char c : target) {
            tmap[c - 'a']++;
        }
        for (int[] ints : map) {
            if (ints[target[0] - 'a'] == 0) {
                continue;
            }
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < 26; j++) {
                if (tmap[j] > 0) {
                    for (int k = 0; k < Math.max(tmap[j] - ints[j], 0); k++) {
                        sb.append((char) ('a' + j));
                    }
                }
            }
            int tmp = process1(dp, map, sb.toString());
            if (tmp != -1) {
                ans = Math.min(ans, 1 + tmp);
            }
        }
        dp.put(t,ans == Integer.MAX_VALUE ? -1 : ans);
        return dp.get(t);
    }
}
