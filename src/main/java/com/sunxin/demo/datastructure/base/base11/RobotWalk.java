package com.sunxin.demo.datastructure.base.base11;

/**
 * @author sunxin
 * 假设有排成一行的N个位置，记为1~N，N 一定大于或等于 2
 * 开始时机器人在其中的M位置上(M 一定是 1~N 中的一个)
 * 如果机器人来到1位置，那么下一步只能往右来到2位置；
 * 如果机器人来到N位置，那么下一步只能往左来到 N-1 位置；
 * 如果机器人来到中间位置，那么下一步可以往左走或者往右走；
 * 规定机器人必须走 K 步，最终能来到P位置(P也是1~N中的一个)的方法有多少种
 * 给定四个参数 N、M、K、P，返回方法数。
 */
public class RobotWalk {

    public static void main(String[] args) {
        process(1,2,3,4);
    }

    private static int process(int N, int M, int K, int P) {
        if (K == 0){
            return M == P ? 1 : 0;
        }
        if (M == 1){
            process(N,2,K - 1,P);
        }
        if (M == N){
            process(N,N - 1,K - 1,P);
        }
        return process(N,M + 1,K - 1,P) + process(N,M - 1,K - 1,P);
    }



    private static int process2(int N, int M, int K, int P) {
        int [][]dp = new int[K + 1][N + 1];
        dp[0][P] = 1;
        for (int i = 1; i <= K; i++) {
            for (int j = 1; j <= N; j++) {
                if (j == 1){
                    dp[i][j] = dp[i - 1][2];
                }else if(j == N){
                    dp[i][j] = dp[i - 1][N - 1];
                }else {
                    dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j + 1];
                }
            }
        }
        return dp[K][M];
    }

}
