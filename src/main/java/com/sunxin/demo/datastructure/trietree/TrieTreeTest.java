package com.sunxin.demo.datastructure.trietree;

public class TrieTreeTest {
    public static void main(String[] args) {
        TrieTree trieTree = new TrieTree();
        trieTree.insert("hello");
        trieTree.insert("hello");
        trieTree.insert("helloww");
        trieTree.insert("hellosdf");
        System.out.println(trieTree.prefixNumber("he"));
        trieTree.delete("hellosdf");
        System.out.println(trieTree.prefixNumber("he"));
    }
}
