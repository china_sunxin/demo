package com.sunxin.demo.datastructure.trietree;

/**
 * @author sunxin02
 */
public class TrieTreeNode {
    //通过该节点的数量
    public int pass;
    //到该节点结束的数量
    public int end;
    //该节点下一个可以到达的节点
    public TrieTreeNode[] next = new TrieTreeNode[26];
}
