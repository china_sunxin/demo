package com.sunxin.demo.datastructure.trietree;

/**
 * @author sunxin02
 */
public class TrieTree {
    private TrieTreeNode root;

    public TrieTree() {
        root = new TrieTreeNode();
    }

    public void insert(String word){
        char[] chars = word.toCharArray();
        TrieTreeNode cur = root;
        root.pass++;
        for (int i = 0; i < chars.length; i++) {
            int index = chars[i] - 'a';
            if (cur.next[index] == null){
                cur.next[index] = new TrieTreeNode();
            }
            cur = cur.next[index];
            cur.pass++;
        }
        cur.end++;
    }

    public void delete(String word) {
        if (search(word) != 0){
            root.pass--;
            TrieTreeNode cur = root;
            char[] chars = word.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                int index = chars[i] - 'a';
                if (--cur.next[index].pass==0){
                    cur.next[index] = null;
                    return;
                }
                cur = cur.next[index];
            }
            cur.end--;
        }


    }

    /**
     * word这个单词之前加入过几次
     * @param word
     * @return
     */
    public int search(String word) {
        char[] chars = word.toCharArray();
        TrieTreeNode cur = root;
        for (int i = 0; i < chars.length; i++) {
            int index = chars[i] - 'a';
            if (cur.next[index] == null){
                return 0;
            }
            cur = cur.next[index];
        }
        return cur.end;
    }

    /**
     * 所有加入的字符串中，有几个是以pre这个字符串作为前缀的
     * @param pre
     * @return
     */
    public int prefixNumber(String pre) {
        if (pre == null) {
            return 0;
        }
        char[] chars = pre.toCharArray();
        TrieTreeNode cur = root;
        for (int i = 0; i < chars.length; i++) {
            int index = chars[i] - 'a';
            if (cur.next[index] ==null){
                return 0;
            }
            cur = cur.next[index];
        }
        return cur.pass;
    }

}
