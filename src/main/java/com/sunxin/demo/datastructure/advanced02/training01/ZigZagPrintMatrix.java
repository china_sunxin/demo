package com.sunxin.demo.datastructure.advanced02.training01;

/**
 * @author sunxin
 * 矩阵的锯齿打印
 */
public class ZigZagPrintMatrix {

    public static void main(String[] args) {
        int[][] arr = new int[3][5];
        int index = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                arr[i][j] = index++;
            }
        }
        print(arr);
    }

    private static void print(int[][] arr) {
        int row = arr.length - 1;
        int col = arr[0].length - 1;
        int ax = 0;
        int ay = 0;
        int bx = 0;
        int by = 0;
        boolean fromUp = false;
        while (ay <= row){
            printLevel(arr, ax, ay, bx, by, fromUp);
            ay = ax == col ? ay + 1 : ay;
            ax = ax == col ? ax : ax + 1;

            bx = by == row ? bx + 1 : bx;
            by = by == row ? by : by + 1;
            fromUp = !fromUp;
        }
    }
    
    private static void printLevel(int[][] arr, int ax, int ay, int bx, int by, boolean fromUp) {
        if (fromUp){
            while (ax != bx - 1) {
                System.out.println(arr[ax][ay]);
                ax--;
                ay++;
            }
        }else {
            while (bx != ax + 1) {
                System.out.println(arr[bx][by]);
                bx++;
                by--;
            }
        }
    }

}
