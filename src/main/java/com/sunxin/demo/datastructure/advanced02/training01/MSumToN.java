package com.sunxin.demo.datastructure.advanced02.training01;

/**
 * @author sunxin02
 * 定义一种数：可以表示成若干（数量>1）连续正数和的数
 * 比如:
 * 5 = 2+3，5就是这样的数
 * 12 = 3+4+5，12就是这样的数
 * 1不是这样的数，因为要求数量大于1个、连续正数和
 * 2 = 1 + 1，2也不是，因为等号右边不是连续正数
 * 给定一个参数N，返回是不是可以表示成若干连续正数和的数
 */
public class MSumToN {

    public static void main(String[] args) {
        boolean res = isMSum1(12);
        System.out.println(res);
    }

    public static boolean isMSum1(int num) {
        if (num == 1 || num == 2){
            return false;
        }
        for (int i = 1; i <= num; i++) {
            int sum = i;
            for (int j = i + 1; j <= num; j++) {
                sum += j;
                if (sum > num){
                    break;
                }
                if (sum == num){
                    return true;
                }
            }
        }
        return false;
    }
}
