package com.sunxin.demo.datastructure.advanced02.training02;

import java.util.HashMap;

/**
 * @author sunxin02
 * 求和为K的最长子数组，数组元素可以为正数、负数、零
 */
public class LongestSumSubArrayLength {


    public static void main(String[] args) {

    }


    public static int maxLength(int[] arr, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0,-1);
        int sum = 0;
        int len = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if (map.containsKey(sum - k)){
                len = Math.max(len,i - map.get(sum - k));
            }else {
                map.put(sum,i);
            }
        }
        return len;
    }
}
