package com.sunxin.demo.datastructure;


public class Color {


    public static void main(String[] args) {

        int n = 9;

        long start = System.currentTimeMillis();
        System.out.println(ways(n));
        System.out.println("cost : " + (System.currentTimeMillis() - start));

    }


    public static int ways(int n) {
        if (n < 5) {
            return 0;
        }
        int plimit = (1 << n) - 1;
        int preStatus = 0;
        int colRest1 = 0;
        int colRest2 = 0;
        return process(plimit, n, preStatus, colRest1, colRest2, 0);
    }


    // plimit 所有可以染色的位置  例如 N 等于 5时，00000011111
    // size 总共几行
    // preStatus 前一行的染色状态  例如 01001
    // colRest1 某一列上是否染了第1个格子
    // colRest2 某一列上是否染了第2个格子
    public static int process(int plimit, int size, int preStatus, int colRest1, int colRest2, int row) {
        if (row == size) {
            return 1;
        }
        // 副对角线上的位置是否有染色
        int a = row == 0 ? 0 : (((1 << (size - row)) & preStatus) >> 1);
        // 主对角线上是否有染色
        int b = row == 0 ? 0 : (((1 << (row - 1)) & preStatus) << 1);
        int c = colRest1 & colRest2;
        // 当前行可以染色的位置
        //  (preStatus | a | b | c) 是所有染色了的位置 ，取反 和 plimit 与一下就是当前行可以染色的位置
        int can = (~(preStatus | a | b | c)) & plimit;
        int ways = 0;
        while (can != 0) {
            // 第一个可以染色的位置
            int first = can & (-can);
            // 染完第1个格子后，剩下可以让第2个格子染色的位置
            int rest = can ^ first;
            while (rest != 0) {
                int second = rest & (-rest);
                // 如果2个染色的位置不挨着
                if (second != (first << 1)) {
                    int nextColRest1 = colRest1;
                    int nextColRest2 = colRest2;
                    // 如果这一列上一个格子都没染 染第 1 个格子
                    if ((colRest1 & first) == 0) {
                        nextColRest1 = colRest1 | first;
                    }
                    else {
                        nextColRest2 = colRest2 | first;
                    }
                    if ((colRest1 & second) == 0) {
                        nextColRest1 = nextColRest1 | second;
                    } else {
                        nextColRest2 = nextColRest2 | second;
                    }
                    ways += process(plimit, size, first | second, nextColRest1, nextColRest2, row + 1);
                }
                // 之前选中的第 2个格子，已经走完了它的递归，把这个位置占上，去下一个位置尝试
                rest ^= second;
            }
            // 之前选中的第1个格子，已经走完了它的递归，把这个位置占上，去下一个位置尝试
            can ^= first;
        }
        return ways;
    }

}

