package com.sunxin.demo.datastructure.list;

import com.sunxin.demo.common.ListUtils;
import com.sunxin.demo.common.ObjectNode;

/**
 * @author sunxin02
 */
public class ReverseList {
    public static void main(String[] args) {
        ListUtils.printList(reverse());
    }

    /**
     * 单链表反转
     */
    public static ObjectNode reverse(){
        ObjectNode head = ListUtils.generateList();
        ObjectNode cur = null;
        ObjectNode pre = null;
        while (head.next != null){
            cur = head.next;
            head.next = pre;
            pre = head;
            head = cur;
        }
        head.next = pre;
        return head;
    }
}
