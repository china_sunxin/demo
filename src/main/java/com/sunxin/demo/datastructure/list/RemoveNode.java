package com.sunxin.demo.datastructure.list;

import com.sunxin.demo.common.ListUtils;
import com.sunxin.demo.common.ObjectNode;

/**
 * @author sunxin02
 */
public class RemoveNode {

    public static void main(String[] args) {
        ObjectNode head = ListUtils.generateList();
        while ((int)head.value == 3){
            head = head.next;
        }
        ObjectNode pre = head;
        ObjectNode cur = head;
        while (cur.next != null){
            cur = cur.next;
            if((int)cur.value == 3){
                pre.next = cur.next;
            }else {
                pre = cur;
            }
        }
        ListUtils.printList(head);
    }






}
