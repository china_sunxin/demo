
package com.sunxin.demo.utils;

import java.sql.*;

public class JdbcUtils {


    /**
     *         Connection connection = JdbcUtils.getConnection();
     *         Statement st=connection.createStatement();
     *         ResultSet rs=st.executeQuery("select * from  batch_bairongLimit where id = '" + id + "'");
     *         while (rs.next()){
     *             rs.getString("id");
     *             rs.getString("name");
     *         }
     *         JdbcUtils.closeResultSet(rs);
     *         JdbcUtils.closeStatement(st);
     *         JdbcUtils.closeConnection(connection);
     * @return
     */

    public static Connection getConnection() {
        try {
            String Url="jdbc:mysql://hadoop-2:3306/crawlers?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            String User="dp_test";
            String Password="dp_test123456";
            //1.加载驱动程序
            Class.forName("com.mysql.jdbc.Driver");
            //2.获得数据库链接
            Connection con= DriverManager.getConnection(Url,User,Password);
            return con;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static void closeConnection(Connection connection){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeResultSet(ResultSet rs){
        try {
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeStatement(Statement statement){
        try {
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
