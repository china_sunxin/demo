package com.sunxin.demo.utils;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

/**
 * @author sunxin02
 */
public class HttpUtils {
    private static final PoolingHttpClientConnectionManager CM = new PoolingHttpClientConnectionManager(registry());


    private static final int DEFAULT_SO_TIMEOUT = 15000;
    /**
     * 单路由最大连接数
     */
    private static final int DEFAULT_MAX_PER_ROUTE = 500;

    /**
     * 连接池最大连接数
     */
    private static final int CONN_POOL_MAX_TOTAL = 2000;
    /**
     * 连接池空闲连接最大存活时间(3分钟)
     */
    private static final int CONN_POOL_IDLE_TIMEOUT = 3 * 60 * 1000;


    static {
        initConnectionManager(CM);
    }

    private static void initConnectionManager(PoolingHttpClientConnectionManager cm) {
        cm.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(DEFAULT_SO_TIMEOUT).build());
        // 单路由最大连接数
        cm.setDefaultMaxPerRoute(DEFAULT_MAX_PER_ROUTE);
        cm.setMaxTotal(CONN_POOL_MAX_TOTAL);
        // 关闭超过keep-alive的连接（服务端有可能关闭了）
        cm.closeExpiredConnections();
        // 空闲连接关闭阈值
        cm.closeIdleConnections(CONN_POOL_IDLE_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    private static Registry<ConnectionSocketFactory> registry() {
        return RegistryBuilder.<ConnectionSocketFactory>create()
                .register("https", new SSLConnectionSocketFactory(sslContext(), new NopHostnameVerifier()))
                .register("http", PlainConnectionSocketFactory.getSocketFactory()) // thread-safe
                .build();
    }

    static class NopHostnameVerifier implements X509HostnameVerifier {
        @Override
        public boolean verify(String s, SSLSession sslSession) {
            return true;
        }

        @Override
        public void verify(String host, SSLSocket ssl) throws IOException {

        }

        @Override
        public void verify(String host, X509Certificate cert) throws SSLException {

        }

        @Override
        public void verify(String host, String[] cns, String[] subjectAlts) throws SSLException {

        }
    }


    static class TrustAllStrategy implements TrustStrategy {
        @Override
        public boolean isTrusted(X509Certificate[] chain, String authType) {
            return true;
        }
    }

    private static SSLContext sslContext() {
        try {
            return SSLContexts.custom().loadTrustMaterial(null, new TrustAllStrategy() {
            }).build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



    /**
     * sockettimeout
     */
    private static final int DEFAULT_SOCKET_TIMEOUT = 15000;
    /**
     * 从连接池获取连接超时
     */
    private static final int DEFAULT_GET_CONNECTION_MAX_WAIT_TIMEOUT = 3000;
    /**
     * 创建连接超时
     */
    private static final int DEFAULT_CONNECTION_TIMEOUT = 3000;

    private static CloseableHttpClient getHttpClient() {
//        HttpClients.createDefault();
        return HttpClients.custom().setConnectionManager(CM).setDefaultRequestConfig(requestConfig()).build();
    }
    private static RequestConfig requestConfig() {
        return RequestConfig.custom().setStaleConnectionCheckEnabled(true)
                // 分配的 socket 的 soTimeout, 后续处理过程使用这个超时时间
                .setSocketTimeout(DEFAULT_SOCKET_TIMEOUT)
                // socket 建立网络连接, 超时时间
                .setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT)
                // 从连接池获取连接, 最长的等待时间
                .setConnectionRequestTimeout(DEFAULT_GET_CONNECTION_MAX_WAIT_TIMEOUT).build();
    }

    public static void main(String[] args) {
        RegistryBuilder<ConnectionSocketFactory> registryBuilder = RegistryBuilder.<ConnectionSocketFactory>create();
        ConnectionSocketFactory plainSF = new PlainConnectionSocketFactory();
        registryBuilder.register("http", plainSF);
        //指定信任密钥存储对象和连接套接字工厂
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            SSLContext sslContext = SSLContexts.custom().useTLS().loadTrustMaterial(trustStore, new TrustAllStrategy()).build();
            LayeredConnectionSocketFactory sslSF = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            registryBuilder.register("https", sslSF);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Registry<ConnectionSocketFactory> registry = registryBuilder.build();
    }

}
