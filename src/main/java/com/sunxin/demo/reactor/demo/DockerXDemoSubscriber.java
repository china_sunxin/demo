package com.sunxin.demo.reactor.demo;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public class DockerXDemoSubscriber<T> implements Subscriber<T> {

    private String name;

    private Subscription subscription;

    final long bufferSize;

    long count;

    public String getName(){
        return name;
    }

    public Subscription getSubscription() {
        return subscription;
    }
    public DockerXDemoSubscriber(long bufferSize,String name){
        this.bufferSize = bufferSize;
        this.name = name;
    }

    @Override
    public void onSubscribe(Subscription s) {
        this.subscription = s;
        subscription.request(bufferSize);
        System.out.println("开始订阅，onSubscribe");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNext(T t) {

    }

    @Override
    public void onError(Throwable t) {

    }

    @Override
    public void onComplete() {

    }
}
