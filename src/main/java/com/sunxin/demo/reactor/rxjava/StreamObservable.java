package com.sunxin.demo.reactor.rxjava;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class StreamObservable {
    private static Disposable disposable1;
    public static void main(String[] args) {

        Observable.create(new ObservableOnSubscribe<Integer>() {
            // 1. 创建被观察者 & 生产事件
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                System.out.println(5);
                emitter.onNext(1);
                emitter.onNext(2);
                emitter.onNext(3);
                emitter.onComplete();
                System.out.println(6);
            }
        }).subscribe(new Observer<Integer>() {
            @Override public void onSubscribe(Disposable disposable) {
                disposable1 = disposable;
                System.out.println(1);
            }

            @Override public void onNext(Integer integer) {
                System.out.println(2);
//                Disposable.dispose();
            }

            @Override public void onError(Throwable throwable) {
                System.out.println(3);
            }

            @Override public void onComplete() {
                System.out.println(4);
            }
        });
    }

}
