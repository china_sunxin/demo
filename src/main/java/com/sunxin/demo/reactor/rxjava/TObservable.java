package com.sunxin.demo.reactor.rxjava;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public class TObservable {
    public static void main(String[] args) {
        //创建被观察者对象
        // create() 是 RxJava 最基本的创造事件序列的方法
        // 此处传入了一个 OnSubscribe 对象参数
        // 当 Observable 被订阅时，OnSubscribe 的 call() 方法会自动被调用，即事件序列就会依照设定依次被触发
        // 即观察者会依次调用对应事件的复写方法从而响应事件
        // 从而实现被观察者调用了观察者的回调方法 & 由被观察者向观察者的事件传递，即观察者模式
        Observable<Integer> observable = Observable
                .create(new ObservableOnSubscribe<Integer>() {
                    @Override
                    public void subscribe(ObservableEmitter<Integer> observableEmitter) {
                        observableEmitter.onNext(1);
                        observableEmitter.onNext(2);
                        observableEmitter.onNext(3);
                        observableEmitter.onComplete();
                    }
                });

        //        Observable<Integer> just1 = Observable.just(1, 2, 3, 4, 5);
        //        Observable.fromArray(new Integer [] {1,2,3,4});


        //步骤二：创建观察者并定义响应时间的行为

        /*************************************方式一***************************************/

        // 1. 创建观察者 （Observer ）对象
        // 2. 创建对象时通过对应复写对应事件方法 从而 响应对应事件
        Observer<Integer> observer = new Observer<Integer>() {

            // 观察者接收事件前，默认最先调用复写 onSubscribe（）
            @Override
            public void onSubscribe(Disposable disposable) {

            }

            // 当被观察者生产Next事件 & 观察者接收到时，会调用该复写方法 进行响应
            @Override
            public void onNext(Integer o) {
                System.out.println("观察者" + o);
            }

            // 当被观察者生产Error事件& 观察者接收到时，会调用该复写方法 进行响应
            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            // 当被观察者生产Complete事件& 观察者接收到时，会调用该复写方法 进行响应
            @Override
            public void onComplete() {
                System.out.println("结束事件===========");
            }
        };


        /*************************************方式二***************************************/
        Subscriber<Integer> subscriber = new Subscriber<Integer>() {
            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onNext(Integer integer) {

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        };


        //特别注意：2种方法的区别，即Subscriber 抽象类与Observer 接口的区别
        // 相同点：二者基本使用方式完全一致（实质上，在RxJava的 subscribe 过程中，Observer总是会先被转换成Subscriber再使用）
        // 不同点：Subscriber抽象类对 Observer 接口进行了扩展，新增了两个方法：
        // 1. onStart()：在还未响应事件前调用，用于做一些初始化工作
        // 2. unsubscribe()：用于取消订阅。在该方法被调用后，观察者将不再接收 & 响应事件
        // 调用该方法前，先使用 isUnsubscribed() 判断状态，确定被观察者Observable是否还持有观察者Subscriber的引用，如果引用不能及时释放，就会出现内存泄露


        // 步骤三：通过订阅（Subscribe）连接观察者和被观察

        observable.subscribe(observer);
//        observable.subscribe(subscriber);


    }

}
