package com.sunxin.demo.leetcode.interview;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class IsUnique {


    @Test
    public void test1(){
        isUnique1("adsf");
    }



    public  boolean isUnique1(String str) {
        Set<Object> set = new HashSet<>();
        for(int i = 0;i <str.length(); i++){
            set.add(str.charAt(i));
        }
        return set.size() == str.length();
    }


    public boolean isUnique2(String astr) {
        long low64 = 0;
        long high64 = 0;

        for (char c : astr.toCharArray()) {
            if (c >= 64) {
                long bitIndex = 1L << c - 64;
                if ((high64 & bitIndex) != 0) {
                    return false;
                }

                high64 |= bitIndex;
            } else {
                long bitIndex = 1L << c;
                if ((low64 & bitIndex) != 0) {
                    return false;
                }

                low64 |= bitIndex;
            }

        }
        return true;
    }

    @Test
    public void test2(){
        for(long i = 64; i <= 128 ; i++){
            System.out.println(1L << i -64);
        }


    }
}
