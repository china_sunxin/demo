package com.sunxin.demo.lamda;

public class PredicateInterface<T> {

    public void test(MyInterface myInterface) {
        myInterface.printSomething();
    }

    public static String Up(String a) {
        return a.toUpperCase();
    }
}
