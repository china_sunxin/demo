package com.sunxin.demo.lamda;

import org.junit.jupiter.api.Test;

import java.util.function.Function;
import java.util.function.Predicate;

public class TestLamda {

    @Test
    public void test1(){
        Runnable r = () -> System.out.println("测试");
    }

    @Test
    public void test2(){
        MyInterface m = () -> System.out.println("打印");
//        m.printSomething();
        m.printSomething1(1);
    }

    @Test
    public void test3(){
        Predicate<Integer> p = (Integer x) -> x > 5;
        boolean test = p.test(6);
        System.out.println(test);
    }

    @Test
    public void test4(){
        PredicateInterface<Integer> p = new PredicateInterface<>();
        p.test(() -> System.out.println("自定义的函数接口"));
    }

    @Test
    public void test5(){
        Function<String,String> function = PredicateInterface::Up;
        String a = function.apply("t");
        System.out.println(a);
    }

}
