package com.sunxin.demo.lamda;

@FunctionalInterface
public interface MyInterface {

    void printSomething();
    default void printSomething1(Integer value){
        printSomething();
        printSomething();
    }

}
