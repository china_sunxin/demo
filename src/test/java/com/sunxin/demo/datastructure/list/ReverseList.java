package com.sunxin.demo.datastructure.list;

import com.sunxin.demo.common.ListUtils;
import com.sunxin.demo.common.ObjectNode;

/**
 * @author sunxin02
 */
public class ReverseList {

    /**
     * 单链表反转
     */
    public void reverseSingleList(){
        ObjectNode head = ListUtils.generateList();
        ObjectNode cur = null;
        ObjectNode pre = null;
        while (head.next != null){
            cur = head.next;
            head.next = pre;
            pre = head;
            head = cur;
        }
        head.next = pre;
        ListUtils.printList(head);
    }
}
