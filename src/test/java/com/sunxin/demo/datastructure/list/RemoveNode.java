package com.sunxin.demo.datastructure.list;

import com.sunxin.demo.common.ListUtils;
import com.sunxin.demo.common.ObjectNode;
import org.junit.Test;

/**
 * @author sunxin02
 */
public class RemoveNode {

    @Test
    public void removeNode(){
        ObjectNode<Integer> head = ListUtils.generateList();
        while (head.value == 3){
            head = head.next;
        }
        ObjectNode<Integer> pre = head;
        ObjectNode<Integer> cur = head;
        while (cur.next != null){
            cur = cur.next;
            if(cur.value == 3){
                pre.next = cur.next;
            }else {
                pre = cur;
            }
        }
        ListUtils.printList(head);
    }






}
