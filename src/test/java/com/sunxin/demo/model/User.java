package com.sunxin.demo.model;

import lombok.Data;

@Data
public class User {

    private int age;

    private String name;

    private Integer weight;
}
