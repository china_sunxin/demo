package com.sunxin.demo.reactor;

import com.mysql.cj.util.LogUtils;
import io.reactivex.*;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.util.concurrent.TimeUnit;

@Slf4j
public class TestObservable {

    @Test
    public void test1(){
        Observable.interval(3000, TimeUnit.MILLISECONDS)//每隔3s发一个事件
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<Long>() {
                @Override
                public void onSubscribe(Disposable d) {
                    log.info("tag", "start");
                }

                @Override
                public void onNext(Long aLong) {
                    log.info("tag", "onNext：" + aLong);
                }

                @Override
                public void onError(Throwable e) {
                    log.info("tag", "error：" + e.getMessage());
                }

                @Override
                public void onComplete() {
                    log.info("tag", "onComplete");
                }
            });

        new Thread(() -> {});

    }

    @Test
    public void testMap(){
        Observable.create(new ObservableOnSubscribe<File>() {
            @Override
            public void subscribe(ObservableEmitter<File> emitter) throws Exception {
                File file = new File("" + File.separator + "blacklist");
                emitter.onNext(file);
            }
        }).concatMap( (File file) -> {
                if (!file.isDirectory()) {
                    return Observable.empty();
                }
                return Observable.fromArray(file.listFiles());
        }).subscribe((File file) -> {
                    log.info("getPackageNames", "删除文件夹中已存在的文件");
                    file.delete();
                }
        );
    }





}
