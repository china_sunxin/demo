package com.sunxin.demo.reactor.flux;

import org.junit.Test;
import reactor.core.publisher.Flux;


public class FluxDemo {

    @Test
    public void demo1() {
        Flux<String> flux = Flux.just("hello world");
        flux.subscribe(System.out::println);
    }

    @Test
    public void demo2() {
        Flux<String> stringFlux = Flux.fromArray(new String[] { "sun", "xin" });
        stringFlux.subscribe(System.out::println);
    }

    @Test
    public void demo3() {
        Flux<String> stringFlux = Flux.fromArray(new String[] { "sun", "xin" });
        stringFlux.subscribe(System.out::println);
    }

    @Test
    public void demo4() {

    }
}
