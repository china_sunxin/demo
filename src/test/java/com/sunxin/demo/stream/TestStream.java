package com.sunxin.demo.stream;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestStream {

    @Test
    public void test1(){
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");

        List<String> list1 = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("b");

//        boolean r1 = list.stream().allMatch(a -> a.equals("a"));
        boolean r2 = list1.stream().allMatch(a -> a.equals("bjhf"));

        System.out.println(r2);
    }

    @Test
    public void test2(){
        List<String> strs = Arrays.asList("a", "a", "a", "a", "b");
        boolean aa = strs.stream().anyMatch(str -> str.equals("a"));
        boolean bb = strs.stream().allMatch(str -> str.equals("a"));
        boolean cc = strs.stream().noneMatch(str -> str.equals("a"));
        long count = strs.stream().filter(str -> str.equals("a")).count();
        System.out.println(aa);// TRUE
        System.out.println(bb);// FALSE
        System.out.println(cc);// FALSE
        System.out.println(count);
    }
}
